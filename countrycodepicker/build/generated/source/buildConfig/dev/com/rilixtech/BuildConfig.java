/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.rilixtech;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.rilixtech";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "com.rilixtech";
  public static final String BUILD_TYPE = "dev";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 233;
  public static final String VERSION_NAME = "2.3.3-Dev";
  // Fields from build type: dev
  public static final String BASEURL = "http://indialandexchange.com/indialand/";
  public static final String DIRECTION_API_KEY = "AIzaSyBVzAC-M59fAKFDKadq44YsPPdmYn4sEzA";
}
