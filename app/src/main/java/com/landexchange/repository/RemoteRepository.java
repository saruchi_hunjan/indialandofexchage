package com.landexchange.repository;

import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationAPIResponse;
import com.landexchange.projectmodules.bookagent.models.AgentsDetailAPIResponse;
import com.landexchange.projectmodules.createaccount.models.CreateAcRequestBodyMO;
import com.landexchange.projectmodules.dashboardfragments.home.models.CollectionsAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedLocalitiesAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.PromotersAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.TopAgentsAPIResponse;
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse;
import com.landexchange.projectmodules.login.models.LoginAPIResponse;
import com.landexchange.projectmodules.login.models.LoginRequestBodyMO;
import com.landexchange.projectmodules.login.models.ResendApiMO;
import com.landexchange.projectmodules.popularlocalities.models.CityAreasAPIResponse;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.Response;

public interface RemoteRepository {

    //    Observable<LoginAPIResponse> loginAPICall(String mobileNo);
    Observable<LoginAPIResponse> loginAPICall(LoginRequestBodyMO requestBodyMO); // Modified in API ver 2.0

    Observable<ResendApiMO> resendOtpAPICall(); //Added in API ver 2.0

    Observable<CitiesAPIResponse> citiesAPICall();

    Observable<PromotersAPIResponse> promotersAPICall();

    Observable<CityAreasAPIResponse> cityAreaAPICall(String cityCode);

    Observable<FeaturedProjectAPIResponse> featuredProjectAPICall(HashMap<String, Integer> queryMap);

    Observable<FeaturedProjectAPIResponse> ileRecommendationAPICall(HashMap<String, Integer> queryMap);

    Observable<FeaturedProjectAPIResponse> trendingProjectAPICall(HashMap<String, Integer> queryMap);

    Observable<CollectionsAPIResponse> collectionsAPICall(HashMap<String, Integer> queryMap);

    Observable<TopAgentsAPIResponse> topAgentsAPICall(HashMap<String, Integer> queryMap);

    Observable<FeaturedLocalitiesAPIResponse> featuredLocalitiesAPICall(HashMap<String, Integer> queryMap);

    Observable<AgentsDetailAPIResponse> agentsDetailsAPICall(HashMap<String, Integer> queryMap);

    Observable<Response<LoginAPIResponse>> createAccountAPICall(CreateAcRequestBodyMO userDetailsBody);

    Observable<NotificationAPIResponse> notificationsAPICall(String userId);
}
