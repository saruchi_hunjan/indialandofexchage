package com.landexchange.repository;

import com.landexchange.networking.NetworkService;
import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationAPIResponse;
import com.landexchange.projectmodules.bookagent.models.AgentsDetailAPIResponse;
import com.landexchange.projectmodules.createaccount.models.CreateAcRequestBodyMO;
import com.landexchange.projectmodules.createaccount.models.CreateAccountAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.CollectionsAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedLocalitiesAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.PromotersAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.TopAgentsAPIResponse;
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse;
import com.landexchange.projectmodules.login.models.LoginAPIResponse;
import com.landexchange.projectmodules.login.models.LoginRequestBodyMO;
import com.landexchange.projectmodules.login.models.ResendApiMO;
import com.landexchange.projectmodules.popularlocalities.models.CityAreasAPIResponse;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;

public class RemoteRepositoryImpl implements RemoteRepository {

    private NetworkService networkService;

    @Inject
    public RemoteRepositoryImpl(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public Observable<LoginAPIResponse> loginAPICall(LoginRequestBodyMO loginRequestBodyMO) {
        return networkService.loginAPICallV2(loginRequestBodyMO);
    }

    @Override
    public Observable<ResendApiMO> resendOtpAPICall() {
        return networkService.resendOtpAPICallV2();
    }

    @Override
    public Observable<CitiesAPIResponse> citiesAPICall() {
        return networkService.citiesAPICall();
    }

    @Override
    public Observable<CityAreasAPIResponse> cityAreaAPICall(String cityCode) {
        return networkService.cityAreaAPICall(cityCode);
    }

    @Override
    public Observable<FeaturedProjectAPIResponse> featuredProjectAPICall(HashMap<String, Integer> queryMap) {
        return networkService.featuredProjectAPICall(queryMap);
    }

    @Override
    public Observable<FeaturedProjectAPIResponse> ileRecommendationAPICall(HashMap<String, Integer> queryMap) {
        return networkService.ileRecommendationAPICall(queryMap);
    }

    @Override
    public Observable<FeaturedProjectAPIResponse> trendingProjectAPICall(HashMap<String, Integer> queryMap) {
        return networkService.trendingProjectAPICall(queryMap);
    }

    @Override
    public Observable<CollectionsAPIResponse> collectionsAPICall(HashMap<String, Integer> queryMap) {
        return networkService.collectionsAPICall(queryMap);
    }

    @Override
    public Observable<TopAgentsAPIResponse> topAgentsAPICall(HashMap<String, Integer> queryMap) {
        return networkService.topAgentsAPICall(queryMap);
    }

    @Override
    public Observable<FeaturedLocalitiesAPIResponse> featuredLocalitiesAPICall(HashMap<String, Integer> queryMap) {
        return networkService.featuredLocalitiesAPICall(queryMap);
    }

    @Override
    public Observable<AgentsDetailAPIResponse> agentsDetailsAPICall(HashMap<String, Integer> queryMap) {
        return networkService.agentsDetailsAPICall(queryMap);
    }

    @Override
    public Observable<NotificationAPIResponse> notificationsAPICall(String userId) {
        return networkService.notificationsAPICall(userId);
    }

    @Override
    public Observable<PromotersAPIResponse> promotersAPICall() {
        return networkService.promotersAPICall();
    }

    @Override
    public Observable<Response<LoginAPIResponse>> createAccountAPICall(CreateAcRequestBodyMO userDetailsBody) {
        return networkService.createAccountAPICall(userDetailsBody);
    }
}
