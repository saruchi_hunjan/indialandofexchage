package com.landexchange.util.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.landexchange.R
import com.landexchange.projectmodules.dedicatedscreens.picturetour.TourPictureActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_pager_adapter.view.*

/**
 * Created by Ashu Rajput on 6/17/2019.
 */
class ILEPagerAdapter(val context: Context, private val isPicturePreviewEnabled: Boolean) : PagerAdapter() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private val headerImagesShowCount = 5
    private val pictureList = arrayOf(R.drawable.temp_home_remove_later,
        R.drawable.temp_banner, R.drawable.temp_image3_remove_it, R.drawable.temp_home_remove_later,
        R.drawable.temp_banner, R.drawable.temp_image3_remove_it, R.drawable.temp_home_remove_later,
        R.drawable.temp_banner, R.drawable.temp_image3_remove_it)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return `object` == view
    }

    override fun getCount(): Int {
        return if (isPicturePreviewEnabled) pictureList.size else headerImagesShowCount
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View = if (isPicturePreviewEnabled)
            layoutInflater.inflate(R.layout.row_picture_preview_pager, container, false)
        else
            layoutInflater.inflate(R.layout.row_pager_adapter, container, false)

        try {
            Picasso.get().load(pictureList[position]).into(view.propertiesImageView)
            container.addView(view)
            if (!isPicturePreviewEnabled)
                view.setOnClickListener { context.startActivity(Intent(context, TourPictureActivity::class.java)) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}