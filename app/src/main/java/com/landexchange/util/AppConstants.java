package com.landexchange.util;

/**
 * Created by Saruchi on 22,April,2019
 */
public interface AppConstants {

    String DATABASE_NAME = "ile.db";

    //----------------API Status Message------------//
    String SUCCESS_STATUS_MESSAGE = "success";
    String FAILURE_STATUS_MESSAGE = "failure";

    //------------CHAT------------------------------//
    String FB_CHAT_USERS_KEY = "ChatUsers";
    String FB_CHAT_DETAILS_KEY = "ChatDetails";
    String FB_CHAT_USER_LIST_KEY = "MyChatUserList";
    String RIGHT_MSG_VIEW = "RIGHT_VIEW";
    String LEFT_MSG_VIEW = "LEFT_VIEW";
    String CHILD_MSG_DETAILS = "msgDetails";

    //------------ Network Connection -------------------//
    String NOINTERNET = "NOINTERNET";
    String INTERNETENABLED = "INTERNET ENABLED";

    //------------CREATE ACCOUNT API PARAMS KEYS-----------//
    String CREATE_ACCOUNT_FIRST_NAME = "fname";
    String CREATE_ACCOUNT_LAST_NAME = "lname";
    String CREATE_ACCOUNT_MOBILE = "mobile";

    //------------DASHBOARD-HOME SCREEN APIS KEYS-----------//
    String CITY_ID = "city_id";
    String AREA_ID = "area_id";

    //------------ACCOUNT OPTIONS KEYS-----------//
    String ACCOUNT_MODULE_TYPE = "AccountModuleType";
    String ACCOUNT_MODULE_NOTIFICATION = "NOTIFICATIONS";
    String ACCOUNT_MODULE_FEEDBACK = "FEEDBACK";
    String ACCOUNT_MODULE_SETTINGS = "SETTINGS";

    //------------BUNDLE KEYS--------------------//
    String CITY_LIST_KEY = "CitiesKeys";
    String SELECTED_CITY_NAME_KEY = "SELECTED_CITY_NAME";
    String SELECTED_CITY_CODE_KEY = "SELECTED_CITY_CODE";
    String COMING_FROM_AGENT_KEY = "COMING_FROM_AGENT_SCREEN";
    String COMING_FROM_FILTER_KEY = "COMING_FROM_AGENT_FILTER_SCREEN";
    String DIALOG_TYPE = "DIALOG_TYPE";
    String DIALOG_TYPE_LOGOUT = "LOGOUT";
    String DIALOG_TYPE_FILTER = "FILTER";
    String DIALOG_TYPE_GPS = "GPS";
    String DIALOG_TYPE_EXPORT_PDF = "EXPORT_PDF";
    String SEE_ALL_AGENT_KEY = "SEE_ALL_AGENTS";
    //Using below key for 'Trending Projects','ILE Recommendation Projects', 'Featured Projects'
    String SEE_ALL_PROJECTS_KEY = "SEE_ALL_PROJECTS";
    String SORTING_TYPE_KEY = "SORTING_TYPE";
    String SORTING_ATOZ = "A_TO_Z";
    String SORTING_ZTOA = "Z_TO_A";
    String SORTING_RATING_HIGH_TO_LOW = "RATING_HIGH_TO_LOW";
    String SORTING_RATING_LOW_TO_HIGH = "RATING_LOW_TO_HIGH";
    String WEB_VIEW_KEY = "WEB_VIEW_KEY";
    String WEB_VIEW_TOU = "TOU";
    String WEB_VIEW_PRIVACY_POLICY = "PRIVACY_POLICY";
    String WEB_VIEW_ABOUT_US = "ABOUT_US";
    String WEB_VIEW_CONTACT_US = "CONTACT_US";

    //------------BUNDLE KEYS(Filters fragment)-------------//
    String FILTER_TYPE_KEY = "FILTER_TYPE";
    String AGENT_FILTER = "AGENT_FILTER";
    String SEE_ALL_PROP_FILTER = "SEE_ALL_PROP";
    String PROPERTY_FILTER = "PROPERTY_FILTER";
    String NAV_SEARCH_FILTER = "NAV_SEARCH_FILTER";

    String FEEDBACK_FILTER = "FEEDBACK";
    String FILTER_BHK_TYPE = "BHK_TYPE";
    String FILTER_PROP_STATUS = "PROP_STATUS";
    String FILTER_PROP_LISTED = "PROP_LISTED";
    String FILTER_DISTANCE = "DISTANCE";
    String FILTER_POSSESSION = "POSSESSION";
    String FILTER_AGE = "AGE_OF_PROP";
    String FILTER_AMENITIES = "AMENITIES";
    String FILTER_BATHROOMS = "BATHROOMS";
    String FILTER_FACING = "FACING";

    String ACTIVE_PROP_FILTER = "ACTIVE_PROP";
    String AGENT_DETAILS_KEY = "AGENT_DETAILS";

}
