package com.landexchange.util.seekbar;

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
public interface OnRangeSeekbarChangeListener {
    void valueChanged(Number minValue, Number maxValue);
}
