package com.landexchange.util.seekbar;

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
public interface OnRangeSeekbarFinalValueListener {
    void finalValue(Number minValue, Number maxValue);
}
