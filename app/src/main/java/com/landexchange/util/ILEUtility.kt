package com.landexchange.util

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.landexchange.R
import javax.inject.Inject

class ILEUtility @Inject constructor() {

    fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    /*private lateinit var displayMetrics: DisplayMetrics
    private fun getDeviceResolution(context: Context): String {
        var screenResolution = ""
        try {
            val density = context.resources.displayMetrics.densityDpi
            displayMetrics = context.resources.displayMetrics
            val width = displayMetrics.widthPixels

            when (density) {
                DisplayMetrics.DENSITY_MEDIUM -> screenResolution = "mdpi"
                DisplayMetrics.DENSITY_HIGH -> screenResolution = "hdpi"
                DisplayMetrics.DENSITY_XHIGH -> screenResolution = "xhdpi"
                DisplayMetrics.DENSITY_XXHIGH -> screenResolution = "xxhdpi"
                DisplayMetrics.DENSITY_XXXHIGH -> screenResolution = "xxxhdpi"
                else -> screenResolution = "hdpi"
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Crashlytics.logException(e)
        }

        return screenResolution
    }*/

    /*private lateinit var deviceWidthHeightArray: IntArray
    fun getDeviceWidthAndHeightForTopAgentTiles(): IntArray {
        deviceWidthHeightArray = IntArray(2)
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        deviceWidthHeightArray[0] = (getDeviceWidth / 3) + (getDeviceWidth / 15)
        deviceWidthHeightArray[1] = (getDeviceWidth / 3) + (getDeviceWidth / 7)
        return deviceWidthHeightArray
    }*/

    private lateinit var collectionDeviceWidthHeightArray: IntArray
    fun getDeviceWidthAndHeightForCollectionsTiles(): IntArray {
        collectionDeviceWidthHeightArray = IntArray(2)
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        collectionDeviceWidthHeightArray[0] = (getDeviceWidth / 3) + (getDeviceWidth / 15)
        collectionDeviceWidthHeightArray[1] = (getDeviceWidth / 3) + (getDeviceWidth / 5)
        return collectionDeviceWidthHeightArray
    }

    fun getDeviceWidthForFeaturedLocalitiesTiles(): Int {
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        return (getDeviceWidth - (getDeviceWidth / 4))
    }

    fun getDeviceWidthAndHeightForCardViews(): Int {
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        return getDeviceWidth - (getDeviceWidth / 6)
    }

    fun getDeviceHeightForPropertyCardViews(): Int {
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        return getDeviceWidth - 30
    }

    fun getDeviceHeightForActivePropCardView(): Int {
        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
        return (getDeviceWidth - (getDeviceWidth / 8))
    }

    fun getDeviceHeightForPicturePreview(): Int {
//        val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
//        return (getDeviceWidth + ((getDeviceWidth*5)/100))
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun getDeviceWidthForCompareCardView(): Int {
        return (Resources.getSystem().displayMetrics.widthPixels) / 2
    }

    fun getDiameterFromDeviceWidth(): Double {
        var mapCircleDiameter = 100.0
        try {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            mapCircleDiameter = (getDeviceWidth / 9).toDouble()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return mapCircleDiameter
    }

    /*@Suppress("DEPRECATION")
    @Synchronized
    fun getMarkerBitmapFromView(context: Context, imageURL: String): Bitmap {
        var returnedBitmap: Bitmap? = null
        try {
            val customMarkerView =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.view_agent_marker, null
                )
            val markerImageView = customMarkerView.findViewById(R.id.profile_image) as ImageView
            Picasso.get().load(imageURL).into(markerImageView, picassoCallBack)

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
            customMarkerView.buildDrawingCache()
            returnedBitmap = Bitmap.createBitmap(
                customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(returnedBitmap)
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
            val drawable = customMarkerView.background
            if (drawable != null)
                drawable!!.draw(canvas)
            customMarkerView.draw(canvas)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return returnedBitmap!!
    }*/

    @Suppress("DEPRECATION")
    @Synchronized
    fun getPicOnMarkerFromCustomView(context: Context, bitmap: Bitmap): Bitmap {
        var returnedBitmap: Bitmap? = null
        try {
            val customMarkerView =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_agent_marker,
                    null)

            Log.e("Marker", "Coming to set Marker and Bitmap together")

            val markerImageView = customMarkerView.findViewById(R.id.profile_image) as ImageView
            markerImageView.setImageBitmap(bitmap)
            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
            customMarkerView.buildDrawingCache()
            returnedBitmap = Bitmap.createBitmap(customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888)
            val canvas = Canvas(returnedBitmap!!)
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
            val drawable = customMarkerView.background
            if (drawable != null)
                drawable.draw(canvas)
            customMarkerView.draw(canvas)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return returnedBitmap!!
    }

}