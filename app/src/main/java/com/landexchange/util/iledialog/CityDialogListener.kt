package com.landexchange.util.iledialog

/**
 * Created by Ashu Rajput on 6/3/2019.
 */
interface CityDialogListener {
    fun onCitySelected(cityName: String, cityCode: Int)
}