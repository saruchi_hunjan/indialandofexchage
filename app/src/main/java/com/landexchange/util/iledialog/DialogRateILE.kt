package com.landexchange.util.iledialog

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.landexchange.R
import com.landexchange.projectmodules.accountoptions.AccountOptionsActivity
import com.landexchange.util.AppConstants
import kotlinx.android.synthetic.main.dialog_rate_ile.view.*

/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class DialogRateILE : DialogFragment() {

    /*@Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IleApplication.getRoomComponent().inject(this)
    }*/

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_rate_ile, container, false)
        try {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            /*compositeDisposable.add(view.rateUsButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                dismiss()
            })
            compositeDisposable.add(view.sendUsFeedbackButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                startActivity(Intent(context, AccountOptionsActivity::class.java)
                    .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_FEEDBACK))
                dismiss()
            })
            compositeDisposable.add(view.rateILECrossIcon.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })*/

            view.rateUsButton.setOnClickListener(onClickListener)
            view.sendUsFeedbackButton.setOnClickListener(onClickListener)
            view.rateILECrossIcon.setOnClickListener(onClickListener)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.rateUsButton -> dismiss()
            R.id.sendUsFeedbackButton -> {
                startActivity(Intent(context, AccountOptionsActivity::class.java)
                    .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_FEEDBACK))
                dismiss()
            }
            R.id.rateILECrossIcon -> dismiss()
        }
    }

    /*override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }*/
}