package com.landexchange.util.iledialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.jakewharton.rxbinding3.view.clicks
import com.landexchange.IleApplication
import com.landexchange.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_choose_city.view.*
import kotlinx.android.synthetic.main.dialog_select_priority.view.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/3/2019.
 */
class DialogSelectPriority : DialogFragment() {

    @Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IleApplication.getRoomComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_select_priority, container, false)
        try {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            compositeDisposable.add(view.selectPriorityCrossButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}