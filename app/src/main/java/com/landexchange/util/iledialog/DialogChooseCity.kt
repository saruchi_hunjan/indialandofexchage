package com.landexchange.util.iledialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.jakewharton.rxbinding3.view.clicks
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.projectmodules.iamlookingto.adapters.CitiesAdapter
import com.landexchange.projectmodules.iamlookingto.models.CitiesMO
import com.landexchange.util.AppConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_choose_city.*
import kotlinx.android.synthetic.main.dialog_choose_city.view.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/3/2019.
 */
class DialogChooseCity : DialogFragment(), CityDialogListener {

    @Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    private lateinit var citiesList: ArrayList<CitiesMO>

    companion object {
        @JvmStatic
        fun newInstance(arrayList: ArrayList<CitiesMO>): DialogChooseCity {
            val dialogChooseCity = DialogChooseCity()
            val bundle = Bundle()
            bundle.putSerializable(AppConstants.CITY_LIST_KEY, arrayList)
            dialogChooseCity.arguments = bundle
            return dialogChooseCity
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IleApplication.getRoomComponent().inject(this)
        citiesList = arguments!!.getSerializable(AppConstants.CITY_LIST_KEY) as ArrayList<CitiesMO>
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_choose_city, container, false)
        try {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            view.dialogCitiesRecyclerView.layoutManager = GridLayoutManager(activity, 3)
            view.dialogCitiesRecyclerView.adapter = CitiesAdapter(activity!!, citiesList, true, this)

            compositeDisposable.add(view.chooseCityCrossButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    override fun onCitySelected(cityName: String, cityCode: Int) {
        dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}