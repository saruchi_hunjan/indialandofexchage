package com.landexchange.util.iledialog

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.landexchange.R
import com.landexchange.projectmodules.accountoptions.AccountOptionsActivity
import com.landexchange.util.AppConstants
import kotlinx.android.synthetic.main.dialog_rate_ile.view.sendUsFeedbackButton
import kotlinx.android.synthetic.main.dialog_user_rating.view.*

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class DialogUserRating : DialogFragment() {

    /*@Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IleApplication.getRoomComponent().inject(this)
    }*/

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_user_rating, container, false)
        try {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            /*compositeDisposable.add(view.submitAgentRating.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                dismiss()
            })
            compositeDisposable.add(view.sendUsFeedbackButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                startActivity(Intent(context, AccountOptionsActivity::class.java)
                    .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_FEEDBACK))
                dismiss()
            })
            compositeDisposable.add(view.ratingCrossIcon.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })*/

            view.submitAgentRating.setOnClickListener(onClickListener)
            view.sendUsFeedbackButton.setOnClickListener(onClickListener)
            view.ratingCrossIcon.setOnClickListener(onClickListener)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.submitAgentRating -> dismiss()
            R.id.sendUsFeedbackButton -> {
                startActivity(Intent(context, AccountOptionsActivity::class.java)
                    .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_FEEDBACK))
                dismiss()
            }
            R.id.ratingCrossIcon -> dismiss()
        }
    }

    /*override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }*/
}