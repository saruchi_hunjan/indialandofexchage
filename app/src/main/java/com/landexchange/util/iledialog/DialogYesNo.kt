package com.landexchange.util.iledialog

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.dialog_yes_no.view.*
import javax.inject.Inject


/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class DialogYesNo : DialogFragment() {

    /*@Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable*/

    @Inject
    lateinit var ileUtility: ILEUtility
    private lateinit var dialogType: String

    companion object {
        @JvmStatic
        fun newInstance(dialogType: String): DialogYesNo {
            val dialog = DialogYesNo()
            val bundle = Bundle()
            bundle.putString(AppConstants.DIALOG_TYPE, dialogType)
            dialog.arguments = bundle
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IleApplication.getRoomComponent().inject(this)
        //        isComingFromFilter = arguments!!.getBoolean(AppConstants.COMING_FROM_FILTER_KEY)
        dialogType = arguments!!.getString(AppConstants.DIALOG_TYPE, "")
    }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_yes_no, container, false)
        try {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            when (dialogType) {
                AppConstants.DIALOG_TYPE_FILTER -> view.dialogTitleMessage.text = activity!!.getString(R.string.resetAllFilters)
                AppConstants.DIALOG_TYPE_GPS -> view.dialogTitleMessage.text = activity!!.getString(R.string.dialog_gps_msg)
                AppConstants.DIALOG_TYPE_EXPORT_PDF -> {
                    view.dialogTitleMessage.text = ileUtility.fromHtml("<b>" +
                            resources.getString(R.string.exportPDFDialogMsg1) + "</b><br><br>" +
                            resources.getString(R.string.exportPDFDialogMsg2))
                }
                /*compositeDisposable.add(view.logoutYesButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                    when (dialogType) {
                        AppConstants.DIALOG_TYPE_LOGOUT -> cleanStorageAndLogout()
                        AppConstants.DIALOG_TYPE_FILTER -> {
                        }
                        AppConstants.DIALOG_TYPE_EXPORT_PDF -> {
                        }
                    }
                    dismiss()
                })
                compositeDisposable.add(view.logoutNoButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })*/
            }
            /*compositeDisposable.add(view.logoutYesButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                when (dialogType) {
                    AppConstants.DIALOG_TYPE_LOGOUT -> cleanStorageAndLogout()
                    AppConstants.DIALOG_TYPE_FILTER -> {
                    }
                    AppConstants.DIALOG_TYPE_EXPORT_PDF -> {
                    }
                }
                dismiss()
            })
            compositeDisposable.add(view.logoutNoButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { dismiss() })*/

            view.logoutYesButton.setOnClickListener(onClickListener)
            view.logoutNoButton.setOnClickListener(onClickListener)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.logoutYesButton -> {
                when (dialogType) {
                    AppConstants.DIALOG_TYPE_LOGOUT -> cleanStorageAndLogout()
                    AppConstants.DIALOG_TYPE_GPS -> {
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }
                    AppConstants.DIALOG_TYPE_FILTER -> {
                    }
                    AppConstants.DIALOG_TYPE_EXPORT_PDF -> {
                    }
                }
                dismiss()
            }
            R.id.logoutNoButton -> dismiss()
        }
    }

    private fun cleanStorageAndLogout() {

    }

    /*override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }*/
}