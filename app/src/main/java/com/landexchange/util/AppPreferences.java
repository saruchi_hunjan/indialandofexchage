package com.landexchange.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO;
import com.landexchange.projectmodules.login.models.UserInfoMO;

public class AppPreferences {

    private static final String KEY_LOGIN_STATUS = "LOGIN_STATUS";
    private static final String KEY_SIGN_UP_STATUS = "SIGN_UP_STATUS";
    private static final String KEY_CITY_LOCALITIES_JSON = "KEY_CITY_LOCALITIES_JSON";
    private static final String KEY_USER_INFO_JSON = "KEY_USER_INFO_JSON";
    private static final String KEY_FIREBASE_USER_ID = "KEY_FIREBASE_USER_ID";
    private static final String KEY_FIREBASE_USER_NAME = "KEY_FIREBASE_USER_NAME";
    private static final String KEY_FCM_ID = "KEY_FCM_ID";
    public static final String KEY_RECEIVER_CHAT_ID = "KEY_RECEIVER_CHAT_ID";
    private static final String KEY_NOTIFICATION_ID = "KEY_NOTIFICATION_ID";
    private static final String KEY_API_TOKEN_ID = "KEY_API_TOKEN_ID";

    private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();
    private final Context mcontext;
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private Gson gson = null;

    @SuppressLint("CommitPrefEdits")
    public AppPreferences(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
        this.mcontext = context;
    }

    public void clearAll() {
        prefsEditor.clear().apply();
    }

    public void saveLoginStatus(boolean isLogin) {
        prefsEditor.putBoolean(KEY_LOGIN_STATUS, isLogin).apply();
    }

    public boolean getLoginStatus() {
        return sharedPrefs.getBoolean(KEY_LOGIN_STATUS, false);
    }

    public void saveSignUpStatus(boolean isSignedUp) {
        prefsEditor.putBoolean(KEY_SIGN_UP_STATUS, isSignedUp).apply();
    }

    public boolean getSignUpStatus() {
        return sharedPrefs.getBoolean(KEY_SIGN_UP_STATUS, false);
    }

    public void saveCityLocalities(CityLocalitiesMO cityLocalitiesMO) {
        if (gson == null)
            gson = new Gson();
        String json = gson.toJson(cityLocalitiesMO);
        prefsEditor.putString(KEY_CITY_LOCALITIES_JSON, json).apply();
    }

    public CityLocalitiesMO getCityLocalitiesJson() {
        if (gson == null)
            gson = new Gson();
        return gson.fromJson(sharedPrefs.getString(KEY_CITY_LOCALITIES_JSON, ""), CityLocalitiesMO.class);
    }

    public void saveUserInformation(UserInfoMO userInfoMO) {
        if (gson == null)
            gson = new Gson();
        String json = gson.toJson(userInfoMO);
        prefsEditor.putString(KEY_USER_INFO_JSON, json).apply();
    }

    public UserInfoMO getUserInfoJson() {
        if (gson == null)
            gson = new Gson();
        return gson.fromJson(sharedPrefs.getString(KEY_USER_INFO_JSON, ""), UserInfoMO.class);
    }

    public void setFireBaseUniqueUserId(String userId) {
        prefsEditor.putString(KEY_FIREBASE_USER_ID, userId).apply();
    }

    public String getFireBaseUniqueUserId() {
        return sharedPrefs.getString(KEY_FIREBASE_USER_ID, "");
    }

    public void setFireBaseUserName(String userName) {
        prefsEditor.putString(KEY_FIREBASE_USER_NAME, userName).apply();
    }

    public String getFireBaseUniqueUserName() {
        return sharedPrefs.getString(KEY_FIREBASE_USER_NAME, "");
    }

    public void setFCMId(String fcmID) {
        prefsEditor.putString(KEY_FCM_ID, fcmID).apply();
    }

    public String getFCMId() {
        return sharedPrefs.getString(KEY_FCM_ID, "");
    }

    public void setReceiverChatId(String receiverUniqueChatId) {
        prefsEditor.putString(KEY_RECEIVER_CHAT_ID, receiverUniqueChatId).apply();
    }

    public String getReceiverChatId() {
        return sharedPrefs.getString(KEY_RECEIVER_CHAT_ID, "");
    }

    public void setUniqueNotificationId(int notificationId) {
        prefsEditor.putInt(KEY_NOTIFICATION_ID, notificationId).apply();
    }

    public int getUniqueNotificationId() {
        return sharedPrefs.getInt(KEY_NOTIFICATION_ID, 0);
    }

    public void setAPITokenId(String appTokenId) {
        prefsEditor.putString(KEY_API_TOKEN_ID, appTokenId).apply();
    }

    public String getAPITokenId() {
        return sharedPrefs.getString(KEY_API_TOKEN_ID, "");
    }

    public void removeKeyValueFromPref(String key) {
        prefsEditor.remove(key).apply();
    }
}
