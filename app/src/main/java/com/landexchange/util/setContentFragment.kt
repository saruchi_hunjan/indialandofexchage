package com.sisindia.csat.util

import android.app.Activity
import android.content.res.Configuration
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager

fun FragmentActivity.setContentFragment(containerViewId: Int, fragment: Fragment?, fragmentTagName: String): Fragment? {
    val f: Fragment? = supportFragmentManager.findFragmentById(containerViewId)
    f?.let { return@setContentFragment f }
    supportFragmentManager?.beginTransaction().add(containerViewId, fragment!!, fragmentTagName)?.commit()
    return fragment
}

//Changes made by AR
fun FragmentActivity.replaceContentFragment(containerViewId: Int, fragment: Fragment?,
    fragmentTagName: String): Fragment? {
    val f: Fragment? = supportFragmentManager.findFragmentById(containerViewId)
    f?.let { return@replaceContentFragment f }
    supportFragmentManager?.beginTransaction().replace(containerViewId, fragment!!, fragmentTagName)
        ?.addToBackStack(null).commit()
    return fragment
}

//Changes made by AR
fun AppCompatActivity.replaceFragment(fragment: Fragment,
    allowStateLoss: Boolean = false, @IdRes containerViewId: Int,
    fragmentTagName: String) {
    val ft = supportFragmentManager.beginTransaction().replace(containerViewId, fragment, fragmentTagName)
    if (!supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

fun replaceFragmentWithBackStack(fragmentManager: FragmentManager, fragment: Fragment,
    allowStateLoss: Boolean = false, @IdRes containerViewId: Int,
    fragmentTagName: String) {
    val ft = fragmentManager.beginTransaction().replace(containerViewId, fragment, fragmentTagName).addToBackStack(null)
    if (!fragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

fun FragmentActivity.addFragment(fragment: Fragment, @IdRes containerViewId: Int, fragmentTagName: String) {
    supportFragmentManager.beginTransaction().add(containerViewId, fragment, fragmentTagName).commit()
}

fun Activity.isLandscape(): Boolean = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE