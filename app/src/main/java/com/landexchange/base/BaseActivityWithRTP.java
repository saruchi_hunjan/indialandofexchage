package com.landexchange.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.snackbar.Snackbar;
import com.landexchange.R;
import com.landexchange.base.listener.ActionBarView;
import com.landexchange.networking.NetworkChangeReceiver;
import com.landexchange.util.AppConstants;
import com.landexchange.util.AppPreferences;
import com.landexchange.util.ObjectUtil;
import com.landexchange.util.dialog.SpotsDialog;
import com.landexchange.util.iledialog.DialogYesNo;

import java.util.Objects;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.text.TextUtils.isEmpty;
import static com.landexchange.util.ObjectUtil.isNull;

/**
 * Created by Ashu Rajput on 3/9/2020.
 */
abstract public class BaseActivityWithRTP extends AppCompatActivity implements ActionBarView {
    public Toolbar toolbar;
    public NetworkChangeReceiver networkCheck;
    public BaseViewModel baseViewModel;
    protected Resources mResources;
    protected AppPreferences appPreferences;
    private AlertDialog spotsDialog;
    private final int locationPermissionCode = 2;

    public abstract int getLayoutId();

    protected abstract void initializeDagger();

    protected abstract void initializeViewModel();

    protected abstract void setUpUi();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        toolbar = findViewById(R.id.toolbar);
        mResources = getResources();
        spotsDialog = new SpotsDialog.Builder().setContext(this).build();
        appPreferences = new AppPreferences(this);
        initializeDagger();
        initializeViewModel();
        if (baseViewModel != null)
            baseViewModel.initialize(Objects.requireNonNull(getIntent().getExtras()));
        initializeToolbar();
        setUpUi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (baseViewModel != null) {
            baseViewModel.start();
        }
    }

    protected void initializeToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            Objects.requireNonNull(getSupportActionBar()).setTitle("");
        }
    }

    @Override
    public void setUpIconVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(visible);
        }
    }

    @Override
    public void setToolbarTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (!isNull(actionBar)) {
            if (!isEmpty(titleKey)) {
                actionBar.setTitle(titleKey);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        setNetworkCheck();
        checkLocationRTP();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (!isNull(networkCheck))
                unregisterReceiver(networkCheck);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (baseViewModel != null) {
            baseViewModel.finalizeView();
        }
    }

    /**
     * for network connection detection
     */
    private void setNetworkCheck() {
        try {
            networkCheck = new NetworkChangeReceiver(this);
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkCheck, filter);
        } catch (Exception ignored) {
        }
    }

    public void showProgressDialog(String message) {
        hideProgressDialog();
        spotsDialog.setMessage(message);
        spotsDialog.setCancelable(false);
        spotsDialog.show();
    }

    public void hideProgressDialog() {
        if (!ObjectUtil.isNull(spotsDialog)) {
            spotsDialog.dismiss();
        }
    }

    public void showSnakeBarMessages(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public void setFullModeScreenForStatusBar() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    public void gradientStatusBar() {
        Window w = getWindow();
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            w.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
        w.setBackgroundDrawableResource(R.drawable.blue_gradient);
    }

    private String[] locations = {android.Manifest.permission.ACCESS_FINE_LOCATION};

    public void checkLocationRTP() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, locations, locationPermissionCode);
        else
            fetchCurrentLocationFromGPS();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == locationPermissionCode) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, locations, locationPermissionCode);
                Toast.makeText(this, "Location Permission is required...", Toast.LENGTH_LONG).show();
            } else {
                fetchCurrentLocationFromGPS();
            }
        }
    }

    public MutableLiveData<Boolean> isGPSEnabled = new MutableLiveData<>();

    private void fetchCurrentLocationFromGPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isGPSEnabled.setValue(false);
            DialogYesNo customDialog = DialogYesNo.newInstance(AppConstants.DIALOG_TYPE_GPS);
            customDialog.show(getSupportFragmentManager().beginTransaction(),
                    mResources.getString(R.string.fragTagNameLogoutDialog));
            customDialog.setCancelable(false);
        } else {
            isGPSEnabled.setValue(true);
        }
    }
}
