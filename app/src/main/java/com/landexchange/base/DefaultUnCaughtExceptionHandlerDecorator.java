package com.landexchange.base;

public class DefaultUnCaughtExceptionHandlerDecorator implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler; //we will decorate the default handler to add functionality to it

    public DefaultUnCaughtExceptionHandlerDecorator(Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler) {
        this.mDefaultUncaughtExceptionHandler = mDefaultUncaughtExceptionHandler;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

        logToCrashLytics(); //add our crashlytics logging and keys here
        //  we have added our crashlytics extra data, now invoke Crashlytics
        mDefaultUncaughtExceptionHandler.uncaughtException(t, e);
    }

    private void logToCrashLytics() {

//        Crashlytics.setUserIdentifier("CSAT");//or whatever. this will show directly on crashlytics dashboard a pretty element

//        Crashlytics.setString("com.sisindia.csat", "5784"); // or whatever you want, such as printing out singleton managers references or model info
        //dont use Crashlytics.Log(..) here. it wont work consistent.
    }
}