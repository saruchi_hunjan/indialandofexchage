package com.landexchange.base;

import android.app.AlertDialog;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import com.google.android.material.snackbar.Snackbar;
import com.landexchange.R;
import com.landexchange.base.listener.ActionBarView;
import com.landexchange.networking.NetworkChangeReceiver;
import com.landexchange.util.AppPreferences;
import com.landexchange.util.ObjectUtil;
import com.landexchange.util.dialog.SpotsDialog;

import java.util.Objects;

import static android.text.TextUtils.isEmpty;
import static com.landexchange.util.ObjectUtil.isNull;

/**
 * saruchi
 */
public abstract class BaseActivity extends AppCompatActivity implements ActionBarView {
    public Toolbar toolbar;
    public NetworkChangeReceiver networkCheck;
    public BaseViewModel baseViewModel;
    // private Unbinder unbinder;
    protected Resources mResources;
    protected AppPreferences appPreferences;
    private AlertDialog spotsDialog;

    public abstract int getLayoutId();

    protected abstract void initializeDagger();

    protected abstract void initializeViewModel();

    protected abstract void setUpUi();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        toolbar = findViewById(R.id.toolbar);
        mResources = getResources();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                // .setTheme(R.style.CustomProgressDialog)
                .build();
        appPreferences = new AppPreferences(this);
        initializeDagger();
        initializeViewModel();
        if (baseViewModel != null) {
            baseViewModel.initialize(Objects.requireNonNull(getIntent().getExtras()));
        }
        initializeToolbar();
        setUpUi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (baseViewModel != null) {
            baseViewModel.start();
        }
    }

    protected void initializeToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            Objects.requireNonNull(getSupportActionBar()).setTitle("");
        }
    }

    @Override
    public void setUpIconVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(visible);
        }
    }

    @Override
    public void setToolbarTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (!isNull(actionBar)) {
            if (!isEmpty(titleKey)) {
                actionBar.setTitle(titleKey);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        setNetworkCheck();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (!isNull(networkCheck))
                unregisterReceiver(networkCheck);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (baseViewModel != null) {
            baseViewModel.finalizeView();
        }
    }

    /**
     * \
     * for network connection detection
     */
    private void setNetworkCheck() {
        try {
            networkCheck = new NetworkChangeReceiver(this);

            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkCheck, filter);

        } catch (Exception e) {
            // finish();
        }
    }

    public void showProgressDialog(String message) {
        hideProgressDialog();
        spotsDialog.setMessage(message);
        spotsDialog.setCancelable(false);
        spotsDialog.show();
    }

    public void hideProgressDialog() {
        if (!ObjectUtil.isNull(spotsDialog)) {
            spotsDialog.dismiss();
        }
    }

    public void showSnakeBarMessages(View view, String message) {
        Snackbar snackBar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
//        snackBar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite));
        snackBar.show();
    }

    public void setFullModeScreenForStatusBar() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    public void gradientStatusBar() {
        Window w = getWindow();
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            w.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
        w.setBackgroundDrawableResource(R.drawable.blue_gradient);
    }

}
