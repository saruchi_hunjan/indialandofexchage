package com.landexchange.base

import android.os.Bundle
import androidx.lifecycle.ViewModel

import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by saruchiarora on 28,November,2018
 */

abstract class BaseViewModel<T> : ViewModel() {

    private var view: WeakReference<T>? = null

    protected var isViewAlive = AtomicBoolean()

    fun getView(): T? {
        return view!!.get()
    }

    fun setView(view: T) {
        this.view = WeakReference(view)
    }

    open fun initialize(extras: Bundle) {}

    fun start() {
        isViewAlive.set(true)
    }

    fun finalizeView() {
        isViewAlive.set(false)
    }
}

