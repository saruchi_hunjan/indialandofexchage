package com.landexchange.base.listener;


import com.landexchange.networking.NetworkError;
import org.jetbrains.annotations.Nullable;
import retrofit2.Response;

/**
 * Created by saruchiarora on 1/20/18.
 */
public interface BaseCallback {
    void onSuccess(Response response);

    void onFail(NetworkError networkError);

    void onSuccess(@Nullable Object commonResponse);
}