package com.sisindia.csat.base.listener

import android.view.View

  interface OnRecyclerViewItemClickListener {

    fun onRecyclerViewItemClick(v: View, position: Int)
}