package com.landexchange.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.landexchange.util.AppPreferences;
import com.landexchange.util.Logger;
import com.landexchange.util.ObjectUtil;
import com.landexchange.util.dialog.SpotsDialog;

import java.util.Objects;

/**
 * Created by saruchiarora on 14,December,2018
 */
public abstract class BaseFragment extends Fragment {
    protected AppPreferences appPreferences;

    private FragmentManager fragmentManager;

    private BaseViewModel baseViewModel;

    public abstract int getLayoutId();

    protected abstract void initializeDagger();

    protected abstract void initializeViewModel();

    protected abstract void fragmentOnCreate();

    protected abstract void setUpUi();

    protected View view;

    public Resources mResources;
    private Context mContext;
    private AlertDialog spotsDialog;

    // private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferences = new AppPreferences(Objects.requireNonNull(getActivity()));
        fragmentManager = getActivity().getSupportFragmentManager();
        mResources = getActivity().getResources();
        spotsDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                // .setTheme(R.style.CustomProgressDialog)
                .build();
        initializeDagger();
        initializeViewModel();
        if (baseViewModel != null) {
            baseViewModel.initialize(Objects.requireNonNull(getArguments()));
        }
        fragmentOnCreate();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (this.view == null)
            view = inflater.inflate(getLayoutId(), container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (baseViewModel != null) {
            baseViewModel.start();
        }
        setUpUi();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (baseViewModel != null) {
            baseViewModel.finalizeView();
        }
        Logger.d("DashboardFragment", "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbinder.unbind();
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    public void setTitle(String title) {
        final ActionBar actionBar = ((BaseActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void showProgressDialog(String message) {
        try {
            hideProgressDialog();
            spotsDialog.setMessage(message);
            //spotsDialog.setCancelable(false);
            spotsDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
//            Crashlytics.logException(e);
        }
    }

    public void hideProgressDialog() {
        try {
            if (!ObjectUtil.isNull(spotsDialog))
                spotsDialog.dismiss();

        } catch (Exception e) {
            e.printStackTrace();
//            Crashlytics.logException(e);
        }
    }
}