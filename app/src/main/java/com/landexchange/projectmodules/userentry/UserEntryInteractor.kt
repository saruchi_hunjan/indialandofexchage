package com.landexchange.projectmodules.userentry

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 4/23/2019.
 */
class UserEntryInteractor {

    interface UserEntryView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }

}