package com.landexchange.projectmodules.userentry

import android.content.Intent
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.createaccount.CreateAccountActivity
import com.landexchange.projectmodules.login.LoginActivity
import kotlinx.android.synthetic.main.user_entry_activity.*

/**
 * Created by Ashu Rajput on 4/23/2019.
 */

class UserEntryActivity : BaseActivity() {

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.user_entry_activity
    }

    override fun setUpUi() {
        createSpanLoginString()
        userEntryCreateAccountButton.setOnClickListener {
            startActivity(Intent(this@UserEntryActivity, CreateAccountActivity::class.java))
        }
    }

    private fun createSpanLoginString() {

        val loginSpanString = SpannableString(resources.getString(R.string.alreadyHaveAccount))
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@UserEntryActivity, LoginActivity::class.java))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }

        loginSpanString.setSpan(clickableSpan, 25, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginSpanString.setSpan(StyleSpan(Typeface.BOLD), 25, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginSpanString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorBlueLinks)),
            25,
            31,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        loginButton.text = loginSpanString
        loginButton.movementMethod = LinkMovementMethod.getInstance()
        loginButton.linksClickable = true
    }

}