package com.landexchange.projectmodules.userentry

import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by Ashu Rajput on 4/24/2019.
 */
class UserEntryViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<UserEntryInteractor>() {


}