package com.landexchange.projectmodules.filters

import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.filters.fragments.AgentsFiltersFragment
import com.landexchange.projectmodules.filters.fragments.NavSearchFilterFragment
import com.landexchange.projectmodules.filters.fragments.PropertyFiltersFragment
import com.landexchange.util.AppConstants
import com.sisindia.csat.util.addFragment

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
class FiltersMainActivity : BaseActivity() {

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_filters_main
    }

    override fun setUpUi() {
        gradientStatusBar()
        val receivedBundle = intent.extras
        receivedBundle?.let {
            if (receivedBundle.containsKey(AppConstants.FILTER_TYPE_KEY)) {
                when (receivedBundle.get(AppConstants.FILTER_TYPE_KEY)) {
                    AppConstants.AGENT_FILTER -> addFragment(AgentsFiltersFragment.newInstance(),
                        R.id.filterFrameLayout, mResources.getString(R.string.fragTagNameAgentFilter))
                    AppConstants.PROPERTY_FILTER -> addFragment(PropertyFiltersFragment.newInstance(),
                        R.id.filterFrameLayout, mResources.getString(R.string.fragTagNamePropFilter))
                    AppConstants.NAV_SEARCH_FILTER -> addFragment(NavSearchFilterFragment.newInstance(),
                        R.id.filterFrameLayout, mResources.getString(R.string.fragTagNameNavSearchFilter))
                    /*AppConstants.SEE_ALL_PROP_FILTER -> addFragment(SeeAllPropFilterFragment.newInstance(),
                        R.id.filterFrameLayout, mResources.getString(R.string.fragTagNameSeeAllFragment))*/
                }
            }
        }
    }
}