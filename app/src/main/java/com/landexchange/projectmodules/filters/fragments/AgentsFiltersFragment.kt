package com.landexchange.projectmodules.filters.fragments

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.jakewharton.rxbinding3.view.clicks
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.uploadlistings.basicinfo.TextViewSupplier
import com.landexchange.util.AppConstants
import com.landexchange.util.iledialog.DialogYesNo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_agents_filters.*
import kotlinx.android.synthetic.main.include_filter_header.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/6/2019.
 */
class AgentsFiltersFragment : BaseFragment() {

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    companion object {
        @JvmStatic
        fun newInstance() = AgentsFiltersFragment()
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_agents_filters
    }

    override fun setUpUi() {
        compositeDisposable.add(filterHeaderBackButton.clicks().observeOn(AndroidSchedulers.mainThread())
            .subscribe { activity!!.onBackPressed() })
        feedback5Star.setOnClickListener(onClickListener)
        feedback4Star.setOnClickListener(onClickListener)
        feedback3Star.setOnClickListener(onClickListener)
        feedback2Star.setOnClickListener(onClickListener)
        greaterActivePro.setOnClickListener(onClickListener)
        lesserActivePro.setOnClickListener(onClickListener)
        resetAllFilterButton.setOnClickListener(onClickListener)
    }

    private val onClickListener = View.OnClickListener {

        when (it.id) {
            R.id.feedback5Star -> updateTagViewsSelector(AppConstants.FEEDBACK_FILTER, R.id.feedback5Star)
            R.id.feedback4Star -> updateTagViewsSelector(AppConstants.FEEDBACK_FILTER, R.id.feedback4Star)
            R.id.feedback3Star -> updateTagViewsSelector(AppConstants.FEEDBACK_FILTER, R.id.feedback3Star)
            R.id.feedback2Star -> updateTagViewsSelector(AppConstants.FEEDBACK_FILTER, R.id.feedback2Star)
            R.id.greaterActivePro -> updateTagViewsSelector(AppConstants.ACTIVE_PROP_FILTER, R.id.greaterActivePro)
            R.id.lesserActivePro -> updateTagViewsSelector(AppConstants.ACTIVE_PROP_FILTER, R.id.lesserActivePro)
            R.id.resetAllFilterButton -> {
                val customDialog = DialogYesNo.newInstance(AppConstants.DIALOG_TYPE_FILTER)
                val fragTxn = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                customDialog.show(fragTxn, context!!.resources.getString(R.string.fragTagNameLogoutDialog))
                customDialog.isCancelable = false
            }
        }
    }

    private fun updateTagViewsSelector(filterType: String, textViewId: Int) {
        var filtersList = ArrayList<Int>()

        if (filterType == AppConstants.FEEDBACK_FILTER)
            filtersList = TextViewSupplier.agentFeedbackTVList
        else if (filterType == AppConstants.ACTIVE_PROP_FILTER)
            filtersList = TextViewSupplier.agentActivePropTVList

        compositeDisposable.add(
            Observable.fromIterable(filtersList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (textViewId == it) {
                        val textView = view.findViewById(it) as TextView
                        textView.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_pressed)
                        textView.setTextColor(ContextCompat.getColor(activity!!, R.color.colorWhite))
                    } else {
                        val textView = view.findViewById(it) as TextView
                        textView.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_unpressed)
                        textView.setTextColor(ContextCompat.getColor(activity!!, R.color.colorBlueLinks))
                    }
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}