package com.landexchange.projectmodules.filters.fragments

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.uploadlistings.basicinfo.TextViewSupplier
import com.landexchange.util.AppConstants
import com.landexchange.util.iledialog.DialogYesNo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.include_filter_header.*
import kotlinx.android.synthetic.main.include_filter_options_layout.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/12/2019.
 */
class NavSearchFilterFragment : BaseFragment() {

    private lateinit var bhkFilterSelected: String

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    override fun getLayoutId(): Int {
        return R.layout.fragment_nav_search_filter
    }

    companion object {
        @JvmStatic
        fun newInstance() = NavSearchFilterFragment()
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {

        viewFilteredProperties.text = resources.getString(R.string.dynamicPropertiesCount, "200")

        resetAllFilterButton.setOnClickListener(onClickListener)
        bhkType1.setOnClickListener(onClickListener)
        bhkType2.setOnClickListener(onClickListener)
        bhkType3.setOnClickListener(onClickListener)
        bhkType4.setOnClickListener(onClickListener)

        propStatusNew.setOnClickListener(onClickListener)
        propStatusResale.setOnClickListener(onClickListener)

        propListedByAgent.setOnClickListener(onClickListener)
        propListedByOwner.setOnClickListener(onClickListener)
        propListedByDev.setOnClickListener(onClickListener)

        distance1KM.setOnClickListener(onClickListener)
        distance5KM.setOnClickListener(onClickListener)
        distance30KM.setOnClickListener(onClickListener)
        distance45KM.setOnClickListener(onClickListener)

        possessionReady.setOnClickListener(onClickListener)
        possessionIn1Year.setOnClickListener(onClickListener)
        possessionIn2Year.setOnClickListener(onClickListener)
        possessionIn3Year.setOnClickListener(onClickListener)
        possessionBeyond3Year.setOnClickListener(onClickListener)

        ageOfPropLess1year.setOnClickListener(onClickListener)
        ageOfPropLess2year.setOnClickListener(onClickListener)
        ageOfPropLess5year.setOnClickListener(onClickListener)
        ageOfPropLess10year.setOnClickListener(onClickListener)
        ageOfPropMore10year.setOnClickListener(onClickListener)

        amenitiesGasPipeline.setOnClickListener(onClickListener)
        amenitiesSwimmingPool.setOnClickListener(onClickListener)
        amenitiesGym.setOnClickListener(onClickListener)
        amenitiesLift.setOnClickListener(onClickListener)
        amenitiesGatedCommunity.setOnClickListener(onClickListener)
        amenitiesParking.setOnClickListener(onClickListener)

        bathroom1Plus.setOnClickListener(onClickListener)
        bathroom2Plus.setOnClickListener(onClickListener)
        bathroom3Plus.setOnClickListener(onClickListener)
        bathroom4Plus.setOnClickListener(onClickListener)

        facingNorth.setOnClickListener(onClickListener)
        facingEast.setOnClickListener(onClickListener)
        facingWest.setOnClickListener(onClickListener)
        facingSouth.setOnClickListener(onClickListener)
        facingNorthEast.setOnClickListener(onClickListener)
        facingNorthWest.setOnClickListener(onClickListener)
        facingSouthEast.setOnClickListener(onClickListener)
        facingSouthWest.setOnClickListener(onClickListener)
    }

    private val onClickListener = View.OnClickListener {

        when (it.id) {
            R.id.bhkType1 -> {
                updateTagViewsSelector(AppConstants.FILTER_BHK_TYPE, R.id.bhkType1)
                bhkFilterSelected = "1 BHK"
            }
            R.id.bhkType2 -> {
                updateTagViewsSelector(AppConstants.FILTER_BHK_TYPE, R.id.bhkType2)
                bhkFilterSelected = "2 BHK" // similar, can do for other filter
            }
            R.id.bhkType3 -> updateTagViewsSelector(AppConstants.FILTER_BHK_TYPE, R.id.bhkType3)
            R.id.bhkType4 -> updateTagViewsSelector(AppConstants.FILTER_BHK_TYPE, R.id.bhkType4)

            R.id.propStatusNew -> updateTagViewsSelector(AppConstants.FILTER_PROP_STATUS, R.id.propStatusNew)
            R.id.propStatusResale -> updateTagViewsSelector(AppConstants.FILTER_PROP_STATUS, R.id.propStatusResale)

            R.id.propListedByAgent -> updateTagViewsSelector(AppConstants.FILTER_PROP_LISTED, R.id.propListedByAgent)
            R.id.propListedByOwner -> updateTagViewsSelector(AppConstants.FILTER_PROP_LISTED, R.id.propListedByOwner)
            R.id.propListedByDev -> updateTagViewsSelector(AppConstants.FILTER_PROP_LISTED, R.id.propListedByDev)

            R.id.distance1KM -> updateTagViewsSelector(AppConstants.FILTER_DISTANCE, R.id.distance1KM)
            R.id.distance5KM -> updateTagViewsSelector(AppConstants.FILTER_DISTANCE, R.id.distance5KM)
            R.id.distance30KM -> updateTagViewsSelector(AppConstants.FILTER_DISTANCE, R.id.distance30KM)
            R.id.distance45KM -> updateTagViewsSelector(AppConstants.FILTER_DISTANCE, R.id.distance45KM)

            R.id.possessionReady -> updateTagViewsSelector(AppConstants.FILTER_POSSESSION, R.id.possessionReady)
            R.id.possessionIn1Year -> updateTagViewsSelector(AppConstants.FILTER_POSSESSION, R.id.possessionIn1Year)
            R.id.possessionIn2Year -> updateTagViewsSelector(AppConstants.FILTER_POSSESSION, R.id.possessionIn2Year)
            R.id.possessionIn3Year -> updateTagViewsSelector(AppConstants.FILTER_POSSESSION, R.id.possessionIn3Year)
            R.id.possessionBeyond3Year -> updateTagViewsSelector(AppConstants.FILTER_POSSESSION,
                R.id.possessionBeyond3Year)

            R.id.ageOfPropLess1year -> updateTagViewsSelector(AppConstants.FILTER_AGE, R.id.ageOfPropLess1year)
            R.id.ageOfPropLess2year -> updateTagViewsSelector(AppConstants.FILTER_AGE, R.id.ageOfPropLess2year)
            R.id.ageOfPropLess5year -> updateTagViewsSelector(AppConstants.FILTER_AGE, R.id.ageOfPropLess5year)
            R.id.ageOfPropLess10year -> updateTagViewsSelector(AppConstants.FILTER_AGE, R.id.ageOfPropLess10year)
            R.id.ageOfPropMore10year -> updateTagViewsSelector(AppConstants.FILTER_AGE, R.id.ageOfPropMore10year)

            R.id.amenitiesGasPipeline -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES,
                R.id.amenitiesGasPipeline)
            R.id.amenitiesSwimmingPool -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES,
                R.id.amenitiesSwimmingPool)
            R.id.amenitiesGym -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES, R.id.amenitiesGym)
            R.id.amenitiesLift -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES, R.id.amenitiesLift)
            R.id.amenitiesParking -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES, R.id.amenitiesParking)
            R.id.amenitiesGatedCommunity -> updateTagViewsSelector(AppConstants.FILTER_AMENITIES,
                R.id.amenitiesGatedCommunity)

            R.id.bathroom1Plus -> updateTagViewsSelector(AppConstants.FILTER_BATHROOMS, R.id.bathroom1Plus)
            R.id.bathroom2Plus -> updateTagViewsSelector(AppConstants.FILTER_BATHROOMS, R.id.bathroom2Plus)
            R.id.bathroom3Plus -> updateTagViewsSelector(AppConstants.FILTER_BATHROOMS, R.id.bathroom3Plus)
            R.id.bathroom4Plus -> updateTagViewsSelector(AppConstants.FILTER_BATHROOMS, R.id.bathroom4Plus)

            R.id.facingNorth -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingNorth)
            R.id.facingEast -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingEast)
            R.id.facingWest -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingWest)
            R.id.facingSouth -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingSouth)

            R.id.facingNorthEast -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingNorthEast)
            R.id.facingNorthWest -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingNorthWest)
            R.id.facingSouthEast -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingSouthEast)
            R.id.facingSouthWest -> updateTagViewsSelector(AppConstants.FILTER_FACING, R.id.facingSouthWest)

            R.id.resetAllFilterButton -> {
                val customDialog = DialogYesNo.newInstance(AppConstants.DIALOG_TYPE_FILTER)
                val fragTxn = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                customDialog.show(fragTxn, context!!.resources.getString(R.string.fragTagNameLogoutDialog))
                customDialog.isCancelable = false
            }
        }
    }

    private fun updateTagViewsSelector(filterType: String, textViewId: Int) {
        var filtersList = ArrayList<Int>()

        when (filterType) {
            AppConstants.FILTER_BHK_TYPE -> filtersList = TextViewSupplier.bhkTVList
            AppConstants.FILTER_PROP_STATUS -> filtersList = TextViewSupplier.propStatusTVList
            AppConstants.FILTER_PROP_LISTED -> filtersList = TextViewSupplier.propListedByTVList
            AppConstants.FILTER_DISTANCE -> filtersList = TextViewSupplier.distanceTVList
            AppConstants.FILTER_POSSESSION -> filtersList = TextViewSupplier.possessionTVList
            AppConstants.FILTER_AGE -> filtersList = TextViewSupplier.ageTVList
            AppConstants.FILTER_AMENITIES -> filtersList = TextViewSupplier.amenitiesTVList
            AppConstants.FILTER_BATHROOMS -> filtersList = TextViewSupplier.bathroomTVList
            AppConstants.FILTER_FACING -> filtersList = TextViewSupplier.facingTVList
        }

        compositeDisposable.add(
            Observable.fromIterable(filtersList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (textViewId == it) {
                        val textView = view.findViewById(it) as TextView
                        textView.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_pressed)
                        textView.setTextColor(ContextCompat.getColor(activity!!, R.color.colorWhite))
                    } else {
                        val textView = view.findViewById(it) as TextView
                        textView.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_unpressed)
                        textView.setTextColor(ContextCompat.getColor(activity!!, R.color.colorBlueLinks))
                    }
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}