package com.landexchange.projectmodules.popularlocalities.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/4/2019.
 */
data class AreaLocalityMO(
    @field:SerializedName("area_code")
    var areaId: Int,

    @field:SerializedName("AreaName")
    var areaName: String
)