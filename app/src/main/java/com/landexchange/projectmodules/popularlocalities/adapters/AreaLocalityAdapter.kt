package com.landexchange.projectmodules.popularlocalities.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.popularlocalities.models.AreaLocalityMO
import kotlinx.android.synthetic.main.row_area_locality.view.*

/**
 * Created by Ashu Rajput on 5/4/2019.
 */
class AreaLocalityAdapter(val context: Context,
    private var areaLocalityList: List<AreaLocalityMO>,
    val localityListener: LocalityListener) :
    RecyclerView.Adapter<AreaLocalityAdapter.AreaLocalityViewHolder>(), Filterable {

    private var layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var areaLocalityFilterableList: List<AreaLocalityMO>

    init {
        areaLocalityFilterableList = areaLocalityList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaLocalityViewHolder {
        val view = layoutInflater.inflate(R.layout.row_area_locality, parent, false)
        return AreaLocalityViewHolder(view)
    }

    override fun getItemCount(): Int {
        return areaLocalityFilterableList.size
    }

    override fun onBindViewHolder(holder: AreaLocalityViewHolder, position: Int) {
        holder.bindDataViews()
    }

    inner class AreaLocalityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindDataViews() {
            itemView.areaLocalityValueTV.text = areaLocalityFilterableList[layoutPosition].areaName
            itemView.setOnClickListener {
                localityListener.onLocalitySelected(areaLocalityFilterableList[layoutPosition].areaName,
                    areaLocalityFilterableList[layoutPosition].areaId)
            }
        }
    }

    interface LocalityListener {
        fun onLocalitySelected(localityName: String, localityCode: Int)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                if (charSequence.isNullOrEmpty())
                    areaLocalityFilterableList = areaLocalityList
                else {
                    var loopedSearchingList = ArrayList<AreaLocalityMO>()
                    for (areaLocalityMO in areaLocalityList) {
                        if (areaLocalityMO.areaName.toLowerCase().contains(charSequence.toString().toLowerCase()))
                            loopedSearchingList.add(areaLocalityMO)
                    }
                    areaLocalityFilterableList = loopedSearchingList
                }

                val filterResults = FilterResults()
                filterResults.values = areaLocalityFilterableList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                areaLocalityFilterableList = results!!.values as List<AreaLocalityMO>
                notifyDataSetChanged()
            }
        }
    }
}