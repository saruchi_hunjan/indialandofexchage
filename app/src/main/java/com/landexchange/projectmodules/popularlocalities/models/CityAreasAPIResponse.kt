package com.landexchange.projectmodules.popularlocalities.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/8/2019.
 */
data class CityAreasAPIResponse(
    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: ArrayList<AreaLocalityMO>? = null
)