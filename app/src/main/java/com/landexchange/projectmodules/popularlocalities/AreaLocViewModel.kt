package com.landexchange.projectmodules.popularlocalities

import com.landexchange.base.BaseViewModel
import com.landexchange.projectmodules.popularlocalities.models.CityAreasAPIResponse
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 5/8/2019.
 */
class AreaLocViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<AreaLocContractor.AreaLocalityView>() {

    fun callingGetAreasFromCityAPI(cityCode: String) {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            io.reactivex.Observable.just(1)
                .subscribeOn(Schedulers.computation())
                .flatMap { remoteRepository.cityAreaAPICall(cityCode) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer<CityAreasAPIResponse> { commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, Consumer<Throwable> { throwable ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )

    }

}