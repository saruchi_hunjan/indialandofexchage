package com.landexchange.projectmodules.popularlocalities

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.widget.TextViewTextChangeEvent
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboard.DashboardActivity
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO
import com.landexchange.projectmodules.popularlocalities.AreaLocContractor.AreaLocalityView
import com.landexchange.projectmodules.popularlocalities.adapters.AreaLocalityAdapter
import com.landexchange.projectmodules.popularlocalities.adapters.AreaLocalityAdapter.LocalityListener
import com.landexchange.projectmodules.popularlocalities.models.AreaLocalityMO
import com.landexchange.projectmodules.popularlocalities.models.CityAreasAPIResponse
import com.landexchange.util.AppConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search_area_codes.*
import kotlinx.android.synthetic.main.toolbar_header_dark.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 5/4/2019.
 */
class AreaLocalitiesActivity : BaseActivity(), AreaLocalityView, LocalityListener {

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var areaLocViewModelFactory: AreaLocViewModelFactory

    private lateinit var areaLocViewModel: AreaLocViewModel
    private lateinit var areaLocalityAdapter: AreaLocalityAdapter
    private var isComingFromBookAgent = false

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        areaLocViewModel =
            ViewModelProvider(this, areaLocViewModelFactory).get(AreaLocViewModel::class.java)
        areaLocViewModel.setView(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_search_area_codes
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun setUpUi() {
        val receivedBundle = intent.extras
        if (receivedBundle != null) {
            if (receivedBundle.containsKey(AppConstants.SELECTED_CITY_NAME_KEY))
                headerMainTitle.text = resources.getString(R.string.selectedAreaString,
                    receivedBundle.getString(AppConstants.SELECTED_CITY_NAME_KEY))
            if (receivedBundle.containsKey(AppConstants.SELECTED_CITY_CODE_KEY))
                areaLocViewModel.callingGetAreasFromCityAPI(receivedBundle.getInt(AppConstants.SELECTED_CITY_CODE_KEY).toString())
            if (receivedBundle.containsKey(AppConstants.COMING_FROM_AGENT_KEY))
                isComingFromBookAgent =
                    receivedBundle.getBoolean(AppConstants.COMING_FROM_AGENT_KEY)

            val cityLocalitiesMO = CityLocalitiesMO(
                receivedBundle.getString(AppConstants.SELECTED_CITY_NAME_KEY)!!,
                receivedBundle.getInt(AppConstants.SELECTED_CITY_CODE_KEY),
                "", 0)
            appPreferences.saveCityLocalities(cityLocalitiesMO)
        }
        headerStepperMessageTV.text = resources.getString(R.string.select_locality_info_msg)
        headerBackArrowButton.setOnClickListener(clickListener)
        headerSkipButton.setOnClickListener(clickListener)
        executeLocalitySearchProcess()
    }

    override fun onSuccess(response: Any) {
        val cityAreaApiResponse = response as CityAreasAPIResponse
        setUpAreaLocalityRecyclerView(cityAreaApiResponse.data!!)
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    override fun onLocalitySelected(localityName: String, localityCode: Int) {
        val cityLocalitiesMO = appPreferences.cityLocalitiesJson
        cityLocalitiesMO.localityName = localityName
        cityLocalitiesMO.localityCode = localityCode
        appPreferences.saveCityLocalities(cityLocalitiesMO)
        if (isComingFromBookAgent) {
            setResult(Activity.RESULT_OK, Intent())
            finish()
        } else navigateToILEDashboard()
    }

    private val clickListener = View.OnClickListener { view ->
        if (view.id == R.id.headerBackArrowButton) finish()
        else if (view.id == R.id.headerSkipButton) {
            if (isComingFromBookAgent) finish()
            else navigateToILEDashboard()
        }
    }

    private fun setUpAreaLocalityRecyclerView(cityNameList: ArrayList<AreaLocalityMO>) {
        searchAreaLocalitiesRecyclerView.layoutManager = LinearLayoutManager(this)
        areaLocalityAdapter = AreaLocalityAdapter(this, cityNameList, this)
        searchAreaLocalitiesRecyclerView.adapter = areaLocalityAdapter
    }

    private fun navigateToILEDashboard() {
        startActivity(Intent(this@AreaLocalitiesActivity, DashboardActivity::class.java)
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        finish()
    }

    private fun executeLocalitySearchProcess() {
        compositeDisposable.add(
            countryCodeSearchBar.textChangeEvents()
                .skipInitialValue()
                .debounce(250, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<TextViewTextChangeEvent>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: TextViewTextChangeEvent) {
                        areaLocalityAdapter.filter.filter(t.text)
                    }

                    override fun onError(e: Throwable) {
                    }
                })
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}