package com.landexchange.projectmodules.popularlocalities

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 5/4/2019.
 */
class AreaLocContractor {
    interface AreaLocalityView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}