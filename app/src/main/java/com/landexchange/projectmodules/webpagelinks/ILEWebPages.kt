package com.landexchange.projectmodules.webpagelinks

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.landexchange.BuildConfig
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.util.AppConstants
import kotlinx.android.synthetic.main.activity_webpages_links.*
import kotlinx.android.synthetic.main.toolbar_header_dark.*

/**
 * Created by Ashu Rajput on 4/29/2019.
 */
class ILEWebPages : BaseActivity() {

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_webpages_links
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun setUpUi() {

        headerSkipButton.visibility = View.GONE
        headerStepperMessageTV.visibility = View.GONE

        val receivedBundle = intent.extras
        if (receivedBundle != null) {
            if (receivedBundle.containsKey(AppConstants.WEB_VIEW_KEY)) {
                when (receivedBundle.getString(AppConstants.WEB_VIEW_KEY)) {
                    AppConstants.WEB_VIEW_TOU -> headerMainTitle.text = resources.getString(R.string.tou_title)
                    AppConstants.WEB_VIEW_PRIVACY_POLICY -> headerMainTitle.text =
                        resources.getString(R.string.privacy_policy_title)
                    AppConstants.WEB_VIEW_ABOUT_US -> headerMainTitle.text =
                        resources.getString(R.string.about_us_title)
                    AppConstants.WEB_VIEW_CONTACT_US -> headerMainTitle.text =
                        resources.getString(R.string.contact_us_title)
                }
            }
        }
        headerBackArrowButton.setOnClickListener { finish() }

        webPagesWebView.clearCache(true)
        webPagesWebView.clearHistory()
        webPagesWebView.settings.javaScriptEnabled = true
        webPagesWebView.settings.domStorageEnabled = true
        webPagesWebView.isLongClickable = false
        webPagesWebView.webViewClient = mWebViewClient
        webPagesWebView.loadUrl(BuildConfig.BASEURL + "api/getCmsPage")
    }

    private var mWebViewClient: WebViewClient = object : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            showProgressDialog(mResources.getString(R.string.message_page_loading))
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            hideProgressDialog()
        }

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return true
        }
    }

}