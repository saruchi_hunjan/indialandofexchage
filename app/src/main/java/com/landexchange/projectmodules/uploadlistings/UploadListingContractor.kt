package com.landexchange.projectmodules.uploadlistings

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 5/7/2019.
 */
class UploadListingContractor {
    interface UploadListingView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
        fun onListingsUploadedSuccessfully()
    }

}