package com.landexchange.projectmodules.uploadlistings

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.landexchange.projectmodules.uploadlistings.address.AddressFragment
import com.landexchange.projectmodules.uploadlistings.basicinfo.BasicInformationFragment
import com.landexchange.projectmodules.uploadlistings.photos.PhotosFragment

/**
 * Created by Ashu Rajput on 5/6/2019.
 */
class UploadListingPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var listingFragments: Fragment? = null
        when (position) {
            0 -> listingFragments = BasicInformationFragment()
            1 -> listingFragments = AddressFragment()
            2 -> listingFragments = PhotosFragment()
        }
        return listingFragments!!
    }

    override fun getCount(): Int {
        return 3
    }

}