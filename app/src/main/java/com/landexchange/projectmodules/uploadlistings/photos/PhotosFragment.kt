package com.landexchange.projectmodules.uploadlistings.photos

import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.uploadlistings.UploadListingActivity
import com.landexchange.projectmodules.uploadlistings.photos.adapters.ListingPhotosAdapter
import com.landexchange.projectmodules.uploadlistings.photos.models.PhotosInformationMO
import kotlinx.android.synthetic.main.fragment_photos.*
import kotlinx.android.synthetic.main.view_no_photos_placeholder.*

/**
 * Created by Ashu Rajput on 5/6/2019.
 */
class PhotosFragment : BaseFragment() {

    lateinit var uploadListingListener: UploadListingListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        uploadListingListener = context as UploadListingActivity
    }

    //    DECLARING interface here AS PER 'PRINCIPLE OF LEAST PRIVILEGE'
    interface UploadListingListener {
        fun onListUploadedSuccessfully()
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_photos
    }

    override fun setUpUi() {
        uploadListingButton.visibility = View.VISIBLE
        uploadListingButton.setOnClickListener(onClickListener)
        addPhotosButton.setOnClickListener(onClickListener)
    }

    private fun setUpPhotoRecyclerView() {
        listingPhotosRecyclerView.visibility = View.VISIBLE
        var photoLists = ArrayList<PhotosInformationMO>()
        photoLists.add(PhotosInformationMO(0))
        photoLists.add(PhotosInformationMO(1))
        photoLists.add(PhotosInformationMO(1))
        photoLists.add(PhotosInformationMO(1))
        photoLists.add(PhotosInformationMO(1))
        photoLists.add(PhotosInformationMO(1))
        photoLists.add(PhotosInformationMO(1))
        listingPhotosRecyclerView.layoutManager = GridLayoutManager(activity, 2)
        listingPhotosRecyclerView.adapter = ListingPhotosAdapter(activity!!, photoLists)
    }

    private val onClickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.uploadListingButton -> {
                uploadListingListener.onListUploadedSuccessfully()
            }
            R.id.addPhotosButton -> {
                setUpPhotoRecyclerView()
                noPhotosPlaceHolderLayout.visibility = View.GONE
            }
        }
    }

   /* private fun getDynamicWidthHeightOfScreen() {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
    }*/

}