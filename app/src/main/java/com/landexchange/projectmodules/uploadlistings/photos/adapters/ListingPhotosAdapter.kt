package com.landexchange.projectmodules.uploadlistings.photos.adapters

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.uploadlistings.photos.models.PhotosInformationMO
import kotlinx.android.synthetic.main.row_photo_button.view.*
import kotlinx.android.synthetic.main.row_photo_items.view.*

/**
 * Created by Ashu Rajput on 5/7/2019.
 */
class ListingPhotosAdapter(val context: Context, var photoLists: ArrayList<PhotosInformationMO>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var layoutManager: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0

    companion object {
        const val TYPE_PHOTO_ITEM = 0
        const val TYPE_ITEM = 1
    }

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PHOTO_ITEM -> {
                val itemWithPhotoButton = layoutManager.inflate(R.layout.row_photo_button, parent, false)
                PhotosButtonViewHolder(itemWithPhotoButton)
            }
            TYPE_ITEM -> {
                val capturedPhotos = layoutManager.inflate(R.layout.row_photo_items, parent, false)
                PhotosItemViewHolder(capturedPhotos)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (photoLists[position].itemType == TYPE_ITEM)
            TYPE_ITEM
        else
            TYPE_PHOTO_ITEM
    }

    override fun getItemCount(): Int {
        return photoLists.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is PhotosButtonViewHolder -> holder.bindPhotoButtonView()
            is PhotosItemViewHolder -> holder.bindPhotosItemsViews()
            else -> throw IllegalArgumentException()
        }
    }

    inner class PhotosItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindPhotosItemsViews() {
            itemView.photoItemsRelativeLayout.layoutParams.width = deviceWidth
            itemView.photoItemsRelativeLayout.layoutParams.height = deviceHeight
        }
    }

    inner class PhotosButtonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPhotoButtonView() {
            itemView.photoButtonItemLinearLayout.layoutParams.width = deviceWidth
            itemView.photoButtonItemLinearLayout.layoutParams.height = deviceHeight
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = (getDeviceWidth / 3) + (getDeviceWidth / 11)
            deviceHeight = (getDeviceWidth / 3) + (getDeviceWidth / 9) + 10
        }
    }

}