package com.landexchange.projectmodules.uploadlistings.basicinfo

import com.landexchange.R

/**
 * Created by Ashu Rajput on 5/7/2019.
 */
object TextViewSupplier {
    //Row '0'
    val iAmTextViewList = listOf(R.id.agentTV, R.id.ownerTV, R.id.developerTV)
    //Row '1'
    val propertyForTextViewList = listOf(R.id.propForRent, R.id.propForSell, R.id.propForMortgage)
    //Row '2'
    val propertyTypeTextViewList = listOf(
        R.id.propTypeApartment,
        R.id.propTypeIndependentFloor, R.id.propTypeIndependentHouse, R.id.propTypeVilla
    )
    //Row '3'
    val txnTypeTextViewList = listOf(R.id.txnTypeNewBooking, R.id.txnTypeResale)
    //Row '4'
    val constructionStatusTextViewList = listOf(R.id.consStatusReady, R.id.consStatusUnder)
    //Row '5'
    val bhkTypeTextViewList =
        listOf(R.id.bhkType1, R.id.bhkType2, R.id.bhkType3, R.id.bhkType4, R.id.bhkType5, R.id.bhkType6)
    //Row '6'
    val propFurnishedTextViewList = listOf(R.id.propFullyFurnished, R.id.propSemiFurnished, R.id.propUnFurnished)


    /*TEXT VIEWS OF AGENT FILTERS FRAGMENT
    * 1-Agent's Feedback
    * 2-Active Properties
    * */
    val agentFeedbackTVList =
        arrayListOf(R.id.feedback5Star, R.id.feedback4Star, R.id.feedback3Star, R.id.feedback2Star)
    val agentActivePropTVList = arrayListOf(R.id.greaterActivePro, R.id.lesserActivePro)

    /*TEXT VIEWS OF NAVIGATION SEARCH FILTERS FRAGMENT
    * 1-BHK
    * 2-PROPERTY STATUS
    * 3-PROPERTY LISTED BY
    * 4-DISTANCE
    * 5-POSSESSION
    * 6-AGE OF PROPERTY
    * 7-AMENITIES
    * 8-BATHROOMS
    * 9-FACING
    * */
    val bhkTVList = arrayListOf(R.id.bhkType1, R.id.bhkType2, R.id.bhkType3, R.id.bhkType4)
    val propStatusTVList = arrayListOf(R.id.propStatusNew, R.id.propStatusResale)
    val propListedByTVList = arrayListOf(R.id.propListedByAgent, R.id.propListedByOwner, R.id.propListedByDev)
    val distanceTVList = arrayListOf(R.id.distance1KM, R.id.distance5KM, R.id.distance30KM, R.id.distance45KM)
    val possessionTVList = arrayListOf(R.id.possessionReady, R.id.possessionIn1Year, R.id.possessionIn2Year,
        R.id.possessionIn3Year, R.id.possessionBeyond3Year)
    val ageTVList = arrayListOf(R.id.ageOfPropLess1year, R.id.ageOfPropLess2year, R.id.ageOfPropLess5year,
        R.id.ageOfPropLess10year, R.id.ageOfPropMore10year)
    val amenitiesTVList = arrayListOf(R.id.amenitiesGasPipeline, R.id.amenitiesSwimmingPool, R.id.amenitiesGym,
        R.id.amenitiesLift, R.id.amenitiesGatedCommunity, R.id.amenitiesParking)
    val bathroomTVList = arrayListOf(R.id.bathroom1Plus, R.id.bathroom2Plus, R.id.bathroom3Plus, R.id.bathroom4Plus)
    val facingTVList = arrayListOf(R.id.facingNorth, R.id.facingEast, R.id.facingWest, R.id.facingSouth,
        R.id.facingNorthEast, R.id.facingNorthWest, R.id.facingSouthEast, R.id.facingSouthWest)
}