package com.landexchange.projectmodules.uploadlistings.address

import com.landexchange.R
import com.landexchange.base.BaseFragment


/**
 * Created by Ashu Rajput on 5/6/2019.
 */
class AddressFragment :BaseFragment(){

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_address
    }

    override fun setUpUi() {
    }

}