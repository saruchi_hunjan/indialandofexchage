package com.landexchange.projectmodules.uploadlistings.basicinfo

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.landexchange.R
import com.landexchange.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_basic_information.*

/**
 * Created by Ashu Rajput on 5/6/2019.
 */
class BasicInformationFragment : BaseFragment() {

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_basic_information
    }

    override fun setUpUi() {
        agentTV.setOnClickListener(clickListener)
        ownerTV.setOnClickListener(clickListener)
        developerTV.setOnClickListener(clickListener)
        propForRent.setOnClickListener(clickListener)
        propForSell.setOnClickListener(clickListener)
        propForMortgage.setOnClickListener(clickListener)
        propTypeApartment.setOnClickListener(clickListener)
        propTypeIndependentFloor.setOnClickListener(clickListener)
        propTypeIndependentHouse.setOnClickListener(clickListener)
        propTypeVilla.setOnClickListener(clickListener)
        txnTypeNewBooking.setOnClickListener(clickListener)
        txnTypeResale.setOnClickListener(clickListener)
        consStatusReady.setOnClickListener(clickListener)
        consStatusUnder.setOnClickListener(clickListener)
        txnTypeResale.setOnClickListener(clickListener)
        bhkType1.setOnClickListener(clickListener)
        bhkType2.setOnClickListener(clickListener)
        bhkType3.setOnClickListener(clickListener)
        bhkType4.setOnClickListener(clickListener)
        bhkType5.setOnClickListener(clickListener)
        bhkType6.setOnClickListener(clickListener)
        propFullyFurnished.setOnClickListener(clickListener)
        propSemiFurnished.setOnClickListener(clickListener)
        propUnFurnished.setOnClickListener(clickListener)
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            // TextViews under 'I am Layout : Row'
            R.id.agentTV -> updateSelectorsOfBasicInfo(0, R.id.agentTV)
            R.id.ownerTV -> updateSelectorsOfBasicInfo(0, R.id.ownerTV)
            R.id.developerTV -> updateSelectorsOfBasicInfo(0, R.id.developerTV)
            // TextViews under 'Property For Layout: Row'
            R.id.propForRent -> updateSelectorsOfBasicInfo(1, R.id.propForRent)
            R.id.propForSell -> updateSelectorsOfBasicInfo(1, R.id.propForSell)
            R.id.propForMortgage -> updateSelectorsOfBasicInfo(1, R.id.propForMortgage)
            // TextViews under 'Property Type Layout : Row'
            R.id.propTypeApartment -> updateSelectorsOfBasicInfo(2, R.id.propTypeApartment)
            R.id.propTypeIndependentFloor -> updateSelectorsOfBasicInfo(2, R.id.propTypeIndependentFloor)
            R.id.propTypeIndependentHouse -> updateSelectorsOfBasicInfo(2, R.id.propTypeIndependentHouse)
            R.id.propTypeVilla -> updateSelectorsOfBasicInfo(2, R.id.propTypeVilla)
            // TextViews under 'Transaction Type Layout : Row'
            R.id.txnTypeNewBooking -> updateSelectorsOfBasicInfo(3, R.id.txnTypeNewBooking)
            R.id.txnTypeResale -> updateSelectorsOfBasicInfo(3, R.id.txnTypeResale)
            // TextViews under 'Construction Status Layout : Row'
            R.id.consStatusReady -> updateSelectorsOfBasicInfo(4, R.id.consStatusReady)
            R.id.consStatusUnder -> updateSelectorsOfBasicInfo(4, R.id.consStatusUnder)
            // TextViews under 'BHK Type Layout : Row'
            R.id.bhkType1 -> updateSelectorsOfBasicInfo(5, R.id.bhkType1)
            R.id.bhkType2 -> updateSelectorsOfBasicInfo(5, R.id.bhkType2)
            R.id.bhkType3 -> updateSelectorsOfBasicInfo(5, R.id.bhkType3)
            R.id.bhkType4 -> updateSelectorsOfBasicInfo(5, R.id.bhkType4)
            R.id.bhkType5 -> updateSelectorsOfBasicInfo(5, R.id.bhkType5)
            R.id.bhkType6 -> updateSelectorsOfBasicInfo(5, R.id.bhkType6)
            // TextViews under 'Furnished Property Layout : Row'
            R.id.propFullyFurnished -> updateSelectorsOfBasicInfo(6, R.id.propFullyFurnished)
            R.id.propSemiFurnished -> updateSelectorsOfBasicInfo(6, R.id.propSemiFurnished)
            R.id.propUnFurnished -> updateSelectorsOfBasicInfo(6, R.id.propUnFurnished)
        }
    }

    /*
    * typeToUpdate {value}
    * value 0,1.... means rows
    * Eg. 0 -> TextViews under 'I Am'
    *     1 -> TextViews under 'Property For'
    * */
    private fun updateSelectorsOfBasicInfo(typeToUpdate: Int, textViewId: Int) {

        when (typeToUpdate) {
            0 -> {
                for (textViewsFromList in TextViewSupplier.iAmTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            1 -> {
                for (textViewsFromList in TextViewSupplier.propertyForTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            2 -> {
                for (textViewsFromList in TextViewSupplier.propertyTypeTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            3 -> {
                for (textViewsFromList in TextViewSupplier.txnTypeTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            4 -> {
                for (textViewsFromList in TextViewSupplier.constructionStatusTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            5 -> {
                for (textViewsFromList in TextViewSupplier.bhkTypeTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
            6 -> {
                for (textViewsFromList in TextViewSupplier.propFurnishedTextViewList) {
                    if (textViewId == textViewsFromList) setSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                    else setUnSelectorOnTextView(view.findViewById(textViewsFromList) as TextView)
                }
            }
        }
    }

    private fun setSelectorOnTextView(textViewWidget: TextView) {
        textViewWidget.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_pressed)
        textViewWidget.setTextColor(ContextCompat.getColor(activity!!, R.color.colorWhite))
    }

    private fun setUnSelectorOnTextView(textViewWidget: TextView) {
        textViewWidget.background = ContextCompat.getDrawable(activity!!, R.drawable.blue_button_unpressed)
        textViewWidget.setTextColor(ContextCompat.getColor(activity!!, R.color.colorBlueLinks))
    }
}