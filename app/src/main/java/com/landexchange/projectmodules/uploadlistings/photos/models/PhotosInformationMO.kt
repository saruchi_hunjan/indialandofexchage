package com.landexchange.projectmodules.uploadlistings.photos.models


/**
 * Created by Ashu Rajput on 5/7/2019.
 */
data class PhotosInformationMO(var itemType: Int)