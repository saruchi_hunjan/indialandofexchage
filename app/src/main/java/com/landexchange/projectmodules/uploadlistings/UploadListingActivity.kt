package com.landexchange.projectmodules.uploadlistings

import android.content.Intent
import android.graphics.Typeface
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboard.DashboardActivity
import com.landexchange.projectmodules.uploadlistings.photos.PhotosFragment.UploadListingListener
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.listing_uploaded_successfully.*
import kotlinx.android.synthetic.main.toolbar_header_light.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 5/6/2019.
 */
class UploadListingActivity : BaseActivity(), UploadListingListener {

    @Inject
    lateinit var ileUtility: ILEUtility

    private lateinit var uploadListingTabLayout: TabLayout
    private lateinit var uploadListingViewPager: ViewPager

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_upload_listing
    }

    override fun setUpUi() {
        uploadListingTabLayout = this.findViewById(R.id.uploadListingTabLayout)
        uploadListingViewPager = this.findViewById(R.id.uploadListingViewPager)
//        setFullModeScreenForStatusBar()
        setUpTabsWithViewPager()

        headerBackWhiteArrow.setOnClickListener(clickListener)
        lightHeaderMainTitle.text = resources.getString(R.string.uploadListing)
        setAndUpdateStepCounter("1")
    }

    private fun setAndUpdateStepCounter(stepCount: String) {
        lightHeaderStepCounterTV.text = ileUtility.fromHtml("Step <b><font color='#ffffff'>$stepCount</font></b>/3")
    }

    private fun setUpTabsWithViewPager() {
        uploadListingTabLayout.addTab(uploadListingTabLayout.newTab().setText(resources.getString(R.string.basicInfo)))
        uploadListingTabLayout.addTab(uploadListingTabLayout.newTab().setText(resources.getString(R.string.addressAddress)))
        uploadListingTabLayout.addTab(uploadListingTabLayout.newTab().setText(resources.getString(R.string.photos)))
//        uploadListingTabLayout.setTabTextColors(Color.parseColor("#c6c6c6"), Color.parseColor("#4a4a4a"))
        uploadListingTabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.colorTabTextUnselected),
            ContextCompat.getColor(this, R.color.colorLabels))

        uploadListingViewPager.offscreenPageLimit = 3
        uploadListingViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(uploadListingTabLayout))

        uploadListingTabLayout.addOnTabSelectedListener(onTabSelectedListener)

        uploadListingViewPager.adapter = UploadListingPagerAdapter(supportFragmentManager)
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.headerBackWhiteArrow -> finish()
            R.id.goToHomeButton -> startActivity(
                Intent(this@UploadListingActivity, DashboardActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }

    private val onTabSelectedListener = object : OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            when (tab!!.position) {
                0 -> setAndUpdateStepCounter("1")
                1 -> setAndUpdateStepCounter("2")
                2 -> setAndUpdateStepCounter("3")
            }
            uploadListingViewPager.currentItem = tab.position
        }
    }

    override fun onListUploadedSuccessfully() {
        listingUploadedSuccessfullyParentLayout.visibility = View.VISIBLE
        propertyUnderQualityCheck.text = ileUtility.fromHtml(
            "<small>Property</small><br>under quality checks!<br>" +
                    "<small><small><font color='#9e9e9e'>your listing will be made live after quality checks</font></small></small>"
        )
        createClickHereSpanString()
        goToHomeButton.setOnClickListener(clickListener)
    }

    private fun createClickHereSpanString() {
        try {
            val clickHereSpanString = SpannableString(resources.getString(R.string.seeListingWithClickHere))
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(textView: View) {
                    listingUploadedSuccessfullyParentLayout.visibility = View.GONE
                    uploadListingViewPager.currentItem = 0
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.isUnderlineText = false
                }
            }
            val startValue = 26
            val endValue = 36

            clickHereSpanString.setSpan(clickableSpan, startValue, endValue, 0)
            clickHereSpanString.setSpan(StyleSpan(Typeface.BOLD), startValue, endValue, 0)
            clickHereSpanString.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorBlueLinks)), startValue, endValue, 0
            )
            seeYourListing.text = clickHereSpanString
            seeYourListing.movementMethod = LinkMovementMethod.getInstance()
            seeYourListing.linksClickable = true

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        try {
            if (listingUploadedSuccessfullyParentLayout.isShown)
                listingUploadedSuccessfullyParentLayout.visibility = View.GONE
            else
                super.onBackPressed()
        } catch (e: java.lang.Exception) {
            super.onBackPressed()
        }
    }

}