package com.landexchange.projectmodules.dashboardfragments.favourites.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_favourite_properties.view.*

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class FavouritePropertyAdapter(context: Context, ileUtility: ILEUtility) :
    RecyclerView.Adapter<FavouritePropertyAdapter.FavouriteViewHolder>() {
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    private var deviceHeight = 0

    init {
        deviceHeight = ileUtility.getDeviceHeightForPropertyCardViews()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        val view = layoutInflater.inflate(R.layout.row_favourite_properties, parent, false)
        return FavouriteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 6
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        holder.bindFavouritesItems()
    }

    inner class FavouriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFavouritesItems() {
            itemView.favouritesRowParentLayout.layoutParams.height = deviceHeight
        }
    }
}