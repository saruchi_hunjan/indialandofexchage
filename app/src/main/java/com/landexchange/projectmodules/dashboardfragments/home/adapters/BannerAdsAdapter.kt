package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.PromotersDataMO
import com.landexchange.util.ILEUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_banner_ads.view.*

/**
 * Created by Ashu Rajput on 6/3/2019.
 */
class BannerAdsAdapter(val context: Context,
    val ileUtility: ILEUtility, private val promotersList: ArrayList<PromotersDataMO>) :
    RecyclerView.Adapter<BannerAdsAdapter.BannerAdsViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0

    init {
        deviceWidth = ileUtility.getDeviceWidthForFeaturedLocalitiesTiles()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerAdsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_banner_ads, parent, false)
        return BannerAdsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return promotersList.size
    }

    override fun onBindViewHolder(holder: BannerAdsViewHolder, position: Int) {
        holder.bindBannerAdsItems()
    }

    inner class BannerAdsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindBannerAdsItems() {
            itemView.bannerAdsRowParentLayout.layoutParams.width = deviceWidth
            Picasso.get().load(promotersList[layoutPosition].promotersImageUrl)
                .into(itemView.promotersImages)
        }
    }
}