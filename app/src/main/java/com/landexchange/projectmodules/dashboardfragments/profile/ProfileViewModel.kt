package com.landexchange.projectmodules.dashboardfragments.profile

import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Ashu Rajput on 5/16/2019.
 */
class ProfileViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<ProfileContractor.ProfileView>() {
}