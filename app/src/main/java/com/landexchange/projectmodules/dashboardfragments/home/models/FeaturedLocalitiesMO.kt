package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
data class FeaturedLocalitiesMO(
    @field:SerializedName("Area")
    var area: String? = null,

    @field:SerializedName("Per_sqrft")
    var perSqrFt: String? = null,

    @field:SerializedName("Percentage")
    var percentage: String? = null,

    @field:SerializedName("Groth")
    var growth: String? = null
)