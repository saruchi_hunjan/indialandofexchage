package com.landexchange.projectmodules.dashboardfragments.home

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboardfragments.home.adapters.HomeSearchableAdapter
import com.landexchange.projectmodules.dashboardfragments.home.models.HomeSearchableDataMO
import kotlinx.android.synthetic.main.layout_searcheable_data.*

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
class HomeSearchActivity : BaseActivity() {

    private lateinit var homeSearchableList: ArrayList<HomeSearchableDataMO>
    private lateinit var homeSearchableAdapter: HomeSearchableAdapter

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_searcheable_data
    }

    override fun setUpUi() {
        gradientStatusBar()
        searchableEditTextWidget.hint = resources.getString(R.string.homeSearchHints)
        headerSearchBackButton.setOnClickListener(onClickListener)
        searchableTitlesIncludeLayoutId.visibility = View.GONE
        setSearchableLocationLocalitiesProjectRecyclerView()
        searchableEditTextWidget.addTextChangedListener(textWatcherListener)
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.headerSearchBackButton -> finish()
        }
    }

    private val textWatcherListener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            Handler().postDelayed(Runnable { startFilteringMySearch(s.toString()) }, 100)
        }
    }

    private fun setSearchableLocationLocalitiesProjectRecyclerView() {
        try {
            searchableDataRecyclerView.layoutManager = LinearLayoutManager(this)
            homeSearchableAdapter = HomeSearchableAdapter(this, getSearchData())
            searchableDataRecyclerView.adapter = homeSearchableAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getSearchData(): ArrayList<HomeSearchableDataMO> {
        homeSearchableList = ArrayList()

        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Locality"))
        homeSearchableList.add(HomeSearchableDataMO("Omaxe realty group", "Developer"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Locality"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Developer"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Locality"))
        homeSearchableList.add(HomeSearchableDataMO("Omaxe realty group south extension", "Developer"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Project"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Project"))
        homeSearchableList.add(HomeSearchableDataMO("Lajpat Nagar", "Locality"))

        return homeSearchableList
    }

    private fun startFilteringMySearch(searchableString: String) {

        val filteredDataList = ArrayList<HomeSearchableDataMO>()
        for (searchMOFromMainList in homeSearchableList) {
            if (searchMOFromMainList.localityDevProjects.toLowerCase().contains(searchableString.toLowerCase()) ||
                searchMOFromMainList.localityDevProjectsType.toLowerCase().contains(searchableString.toLowerCase())
            )
                filteredDataList.add(searchMOFromMainList)
        }

        if (homeSearchableAdapter != null)
            homeSearchableAdapter.filterSearchDataList(filteredDataList)
    }

}