package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.CollectionsMO
import com.landexchange.util.ILEUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_collection.view.*

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
class CollectionsAdapter(
    val context: Context,
    val ileUtility: ILEUtility,
    private val collectionList: ArrayList<CollectionsMO>
) :
    RecyclerView.Adapter<CollectionsAdapter.CollectionViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0

    init {
        try {
            val deviceCalculatedWidthHeight = ileUtility.getDeviceWidthAndHeightForCollectionsTiles()
            deviceWidth = deviceCalculatedWidthHeight[0]
            deviceHeight = deviceCalculatedWidthHeight[1]
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionViewHolder {
        val view = layoutInflater.inflate(R.layout.row_collection, parent, false)
        return CollectionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return collectionList.size
    }

    override fun onBindViewHolder(holder: CollectionViewHolder, position: Int) {
        holder.bindCollectionItems()
    }

    inner class CollectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindCollectionItems() {

            try {
                itemView.collectionRowParentLayout.layoutParams.width = deviceWidth
                itemView.collectionRowParentLayout.layoutParams.height = deviceHeight

                itemView.collectionBottomDetailsTV.text =
                    ileUtility.fromHtml(
                        "<b>" + collectionList[layoutPosition].collection + "</b><br>" +
                                "<small><font color='#9e9e9e'>" + collectionList[layoutPosition].projectCount + " Projects</font></small>"
                    )

                Picasso.get()
                    .load(collectionList[layoutPosition].collectionImageURL)
                    .tag(context)
                    .into(itemView.collectionImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}