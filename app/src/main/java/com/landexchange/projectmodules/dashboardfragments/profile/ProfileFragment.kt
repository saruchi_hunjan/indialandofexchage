package com.landexchange.projectmodules.dashboardfragments.profile

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.accountoptions.editprofile.EditProfileActivity
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.projectmodules.dashboardfragments.profile.ProfileContractor.ProfileView
import com.landexchange.projectmodules.dashboardfragments.supersearch.adapters.SavedSearchesAdapter
import com.landexchange.projectmodules.dashboardfragments.supersearch.adapters.SeenProjectsAdapter
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 5/16/2019.
 */
class ProfileFragment : BaseFragment(), ProfileView {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    lateinit var profileViewModelFactory: ProfileViewModelFactory

    lateinit var profileViewModel: ProfileViewModel

    companion object {
        @JvmStatic
        fun newInstance() = ProfileFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_profile
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        profileViewModel =
            ViewModelProvider(this, profileViewModelFactory).get(ProfileViewModel::class.java)
        profileViewModel.setView(this)
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        var yourListings = ArrayList<FeaturedProjectMO>()
        updateFeaturedProjectTileItems(yourListings)
        updateSavedSearchesTileItems(yourListings)
        updateSeenProjectTileItems(yourListings)
        setTextOnRentSellBanner()
        view.profileEditButton.setOnClickListener(onClickListener)
    }

    override fun onYourListingsAPISuccessResponse(response: Any) {
    }

    override fun onSavedSearchesAPISuccessResponse(response: Any) {
    }

    override fun onSeenProjectsAPISuccessResponse(response: Any) {
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }

    private fun updateFeaturedProjectTileItems(yourListings: ArrayList<FeaturedProjectMO>) {
        view.profileYourListingRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        profileYourListingRecyclerView.adapter =
            SeenProjectsAdapter(activity!!,
                ileUtility,
                yourListings)
    }

    private fun updateSavedSearchesTileItems(yourListings: ArrayList<FeaturedProjectMO>) {

        val savedSearchesIncludeView = view.includeSavedSearchesID
        val leftTitleTV = savedSearchesIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.profileTitleSavedSearches)
        val rightTitleTV =
            savedSearchesIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "SavedSearches"
        rightTitleTV.setOnClickListener(onClickListener)

        view.savedSearchesRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        savedSearchesRecyclerView.adapter =
            SavedSearchesAdapter(activity!!,
                ileUtility)
    }

    private fun updateSeenProjectTileItems(yourListings: ArrayList<FeaturedProjectMO>) {

        val savedSearchesIncludeView = view.includeSeenProjectsID
        val leftTitleTV = savedSearchesIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.profileTitleSeenProjects)
        val rightTitleTV =
            savedSearchesIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "SeenProjects"
        rightTitleTV.setOnClickListener(onClickListener)

        view.seenProjectsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        seenProjectsRecyclerView.adapter =
            SeenProjectsAdapter(activity!!,
                ileUtility,
                yourListings,
                true)
    }

    private fun setTextOnRentSellBanner() {
        val sellRentIncludeView = view.includeSellRentBannerID
        val sellRentBannerTV = sellRentIncludeView.findViewById(R.id.bannerLeftText) as TextView

        sellRentBannerTV.text = ileUtility.fromHtml(
            "Sell / Rent you property free!<br>" +
                    "<small>We have over one lac buyers / tenants listed in our app</small>"
        )
        sellRentBannerTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_deal_hand, 0)
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.homeItemsTitleRight -> {
                when (it.tag) {
                    "SavedSearches" -> Log.e("IncludeClicks", "SavedSearches  SEE All Clicked")
                    "SeenProjects" -> Log.e("IncludeClicks", "SeenProjects SEE All Clicked")
                }
            }
            R.id.profileEditButton -> startActivity(Intent(activity,
                EditProfileActivity::class.java))
        }
    }
}