package com.landexchange.projectmodules.dashboardfragments.home

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.bookagent.BookAgentActivity
import com.landexchange.projectmodules.dashboardfragments.home.HomeContractor.HomeView
import com.landexchange.projectmodules.dashboardfragments.home.adapters.*
import com.landexchange.projectmodules.dashboardfragments.home.models.*
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO
import com.landexchange.projectmodules.seeall.SeeAllAgentsActivity
import com.landexchange.projectmodules.seeall.SeeAllPropertyActivity
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.ObjectUtil
import com.landexchange.util.iledialog.DialogChooseCity
import com.landexchange.util.showLongToast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_google_map.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.views_home_items.*
import kotlinx.android.synthetic.main.views_home_items.view.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 4/30/2019.
 */
class HomeFragment : BaseFragment(), HomeView {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    lateinit var homeViewModelFactory: HomeViewModelFactory
    private lateinit var homeViewModel: HomeViewModel

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    private var promotersAdsResponse: Any? = null
    private var featuredProjectResponse: Any? = null
    private var trendingProjectResponse: Any? = null
    private var recommendationProjectResponse: Any? = null
    private var collectionResponse: Any? = null
    private var topAgentResponse: Any? = null
    private var localitiesResponse: Any? = null
    private var cityLocalityJson: CityLocalitiesMO? = null

    private lateinit var topAgentsArrayList: ArrayList<TopAgentsMO>

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        homeViewModel = ViewModelProvider(this, homeViewModelFactory).get(HomeViewModel::class.java)
        homeViewModel.setView(this)
    }

    override fun fragmentOnCreate() {
        Handler().postDelayed({
            val cityLocalityIdMap = HashMap<String, Int>()
            if (cityLocalityJson == null) {
                cityLocalityIdMap[AppConstants.CITY_ID] = 1
                cityLocalityIdMap[AppConstants.AREA_ID] = 1
            } else {
                cityLocalityIdMap[AppConstants.CITY_ID] = cityLocalityJson!!.cityCode
                cityLocalityIdMap[AppConstants.AREA_ID] = cityLocalityJson!!.localityCode
            }

            //            homeViewModel.callingGetFeaturedProjectAPI(cityLocalityIdMap) // commenting as getting wrong Data from API
            /*homeViewModel.callingGetILERecommendationAPI(cityLocalityIdMap)
            homeViewModel.callingGetTrendingProjectAPI(cityLocalityIdMap)
            homeViewModel.callingGetCollectionsAPI(cityLocalityIdMap)
            homeViewModel.callingGetTopAgentsAPI(cityLocalityIdMap)
            homeViewModel.callingGetFeaturedLocalitiesAPI(cityLocalityIdMap)
            homeViewModel.callingGetPromotersAPI()*/
        }, 2000)
    }

    override fun setUpUi() {
        try {
            cityLocalityJson = appPreferences.cityLocalitiesJson

            if (cityLocalityJson != null) {
                homeHeaderBuyInTV.text =
                    ileUtility.fromHtml("<b>Buy<b> in ${cityLocalityJson!!.cityName}")
            }

            updateGoogleMapTileItems()
            //            setTextOnRentSellBanner()
            //            setTextOnLoginBanner()
            homeBookAnAgent.setOnClickListener(onClickListener)
            homeSearchEditText.setOnClickListener(onClickListener)
            homeHeaderBuyInTV.setOnClickListener(onClickListener)

            if (!ObjectUtil.isNull(promotersAdsResponse))
                onPromotersAPISuccessResponse(promotersAdsResponse!!)
            if (!ObjectUtil.isNull(featuredProjectResponse))
                onFeaturedProjectAPISuccessResponse(featuredProjectResponse!!)
            if (!ObjectUtil.isNull(trendingProjectResponse))
                onTrendingProjectAPISuccessResponse(trendingProjectResponse!!)
            if (!ObjectUtil.isNull(recommendationProjectResponse))
                onILERecommendationAPISuccessResponse(recommendationProjectResponse!!)
            if (!ObjectUtil.isNull(collectionResponse))
                onCollectionAPISuccessResponse(collectionResponse!!)
            if (!ObjectUtil.isNull(topAgentResponse))
                onTopAgentAPISuccessResponse(topAgentResponse!!)
            if (!ObjectUtil.isNull(localitiesResponse))
                onFeaturedLocalitiesAPISuccessResponse(localitiesResponse!!)

            /*compositeDisposable.add(homeHeaderBuyInTV.clicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { homeViewModel.callingGetCitiesAPI() })*/

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onSuccess(response: Any) {
        if (response is CitiesAPIResponse) {
            val chooseCityDialog = DialogChooseCity.newInstance(response.data!!)
            val fragTxn = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            chooseCityDialog.show(fragTxn, "ChooseCityDialog")
            chooseCityDialog.isCancelable = false

            /*val priorityDialog = DialogSelectPriority()
            val fragTxn1 = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            priorityDialog.show(fragTxn1, "PriorityDialog")
            priorityDialog.isCancelable = false*/
        }
    }

    override fun onFeaturedProjectAPISuccessResponse(response: Any) {
        featuredProjectResponse = response as FeaturedProjectAPIResponse
        featuredProjectResponse.let {
            val featuredProjectLists = (featuredProjectResponse as FeaturedProjectAPIResponse).data
            updateFeaturedProjectTileItems(featuredProjectLists!!)
        }
    }

    override fun onTrendingProjectAPISuccessResponse(response: Any) {
        trendingProjectResponse = response as FeaturedProjectAPIResponse
        trendingProjectResponse.let {
            val featuredProjectLists = (trendingProjectResponse as FeaturedProjectAPIResponse).data
            updateTrendingProjectsTileItems(featuredProjectLists!!)
        }
    }

    override fun onILERecommendationAPISuccessResponse(response: Any) {
        recommendationProjectResponse = response as FeaturedProjectAPIResponse
        recommendationProjectResponse.let {
            val featuredProjectLists =
                (recommendationProjectResponse as FeaturedProjectAPIResponse).data
            updateILERecommendationTileItems(featuredProjectLists!!)
        }
    }

    override fun onCollectionAPISuccessResponse(response: Any) {
        collectionResponse = response as CollectionsAPIResponse
        collectionResponse.let {
            val collectionDataList = (collectionResponse as CollectionsAPIResponse).data
            updateCollectionTileItems(collectionDataList!!)
        }
    }

    override fun onTopAgentAPISuccessResponse(response: Any) {
        if (response is TopAgentsAPIResponse) {
            response.let {
                topAgentsArrayList = it.data!!
                updateTopAgentsTileItems(topAgentsArrayList)
            }
        }
    }

    override fun onFeaturedLocalitiesAPISuccessResponse(response: Any) {
        localitiesResponse = response as FeaturedLocalitiesAPIResponse
        localitiesResponse.let {
            val featuredLocDataList = (localitiesResponse as FeaturedLocalitiesAPIResponse).data
            if (featuredLocDataList != null && featuredLocDataList.size > 0)
                updateFeaturedLocalitiesTileItems(featuredLocDataList)
        }
    }

    override fun onPromotersAPISuccessResponse(response: Any) {
        if (response is PromotersAPIResponse) {
            promotersAdsResponse = response
            val promotersAdsList = (promotersAdsResponse as PromotersAPIResponse).data
            if (promotersAdsList != null && promotersAdsList.size > 0)
                updatePromotersRecyclerView(promotersAdsList)
        }
    }

    override fun onFailure(appErrorMessage: String) {
        activity!!.showLongToast(appErrorMessage)
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    private fun updatePromotersRecyclerView(promotersList: ArrayList<PromotersDataMO>) {
        try {
            ileAdsRecyclerView.layoutManager = LinearLayoutManager(activity,
                LinearLayoutManager.HORIZONTAL, false)
            ileAdsRecyclerView.adapter = BannerAdsAdapter(activity!!, ileUtility, promotersList)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun updateGoogleMapTileItems() {
        val searchAgentTitleIncludeLayout = view.includeSearchAgentID
        val leftTitleTV =
            searchAgentTitleIncludeLayout.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeSearchAgents)

        val rightTitleTV =
            searchAgentTitleIncludeLayout.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.visibility = View.GONE
        //        val rightTitleTV = searchAgentTitleIncludeLayout.findViewById(R.id.homeItemsTitleRight) as TextView
        //        rightTitleTV.text = ""
        //        rightTitleTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_map_expand, 0)
        //        rightTitleTV.tag = "AgentSearchTitle"
        //        rightTitleTV.setOnClickListener(onClickListener)
        /*homeViewModel.getLocationData().observe(viewLifecycleOwner) { location -> {

        }}*/

        homeViewModel.getLocationData().observe(this, Observer {
            if (it != null)
                Log.e("HomeFragLoc", "Loc InHomeFrag : ${it.latitude} , ${it.longitude}")
        })

    }

    private fun updateFeaturedProjectTileItems(featureProjectList: ArrayList<FeaturedProjectMO>) {
        val featuredProjectTitleIncludeView = view.includeFeaturedProjectID
        val leftTitleTV =
            featuredProjectTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeFeaturedProject)
        val rightTitleTV =
            featuredProjectTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "FeaturedProjectTitle"
        rightTitleTV.setOnClickListener(onClickListener)

        view.featuredProjectRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        featuredProjectRecyclerView.adapter =
            FeaturedProjectsAdapter(activity!!, ileUtility, featureProjectList)
    }

    private fun updateTopAgentsTileItems(agentDataList: ArrayList<TopAgentsMO>) {
        val topAgentTitleIncludeView = view.includeTopAgentsID
        val leftTitleTV = topAgentTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeTopAgents)
        val rightTitleTV =
            topAgentTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "TopAgents"
        rightTitleTV.setOnClickListener(onClickListener)

        view.topAgentsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        //        val topAgentActivity=TopAgentsAdapter(activity!!, agentDataList)
        val topAgentAdapter = TopAgentsAdapter(activity!!)
        topAgentsRecyclerView.adapter = topAgentAdapter

        compositeDisposable.add(Observable.just(agentDataList).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { topAgentAdapter.updateTopAgentRecyclerView(it) })
    }

    private fun updateCollectionTileItems(collectionList: ArrayList<CollectionsMO>) {
        val collectionsTitleIncludeView = view.includeCollectionsID
        val leftTitleTV =
            collectionsTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeCollection)
        val rightTitleTV =
            collectionsTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.visibility = View.GONE

        view.collectionsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        collectionsRecyclerView.adapter = CollectionsAdapter(activity!!, ileUtility, collectionList)
    }

    private fun updateTrendingProjectsTileItems(featureProjectList: ArrayList<FeaturedProjectMO>) {
        val trendingProjectsTitleIncludeView = view.includeTrendingProjectsID
        val leftTitleTV =
            trendingProjectsTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeTrendingProjects)
        val rightTitleTV =
            trendingProjectsTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "TrendingProjects"
        rightTitleTV.setOnClickListener(onClickListener)
        view.trendingProjectsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        trendingProjectsRecyclerView.adapter =
            FeaturedProjectsAdapter(activity!!, ileUtility, featureProjectList)
    }

    private fun updateILERecommendationTileItems(ileRecommendedList: ArrayList<FeaturedProjectMO>) {
        val ileRecommendationTitleIncludeView = view.includeIleRecommendationID
        val leftTitleTV =
            ileRecommendationTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeILERecommendation)
        val rightTitleTV =
            ileRecommendationTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "ILERecommendation"
        rightTitleTV.setOnClickListener(onClickListener)

        view.ileRecommendationRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        ileRecommendationRecyclerView.adapter =
            FeaturedProjectsAdapter(activity!!, ileUtility, ileRecommendedList)
    }

    private fun updateFeaturedLocalitiesTileItems(featuredLocList: ArrayList<FeaturedLocalitiesMO>) {
        val featuredLocTitleIncludeView = view.includeFeaturedLocalitiesID
        val leftTitleTV =
            featuredLocTitleIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.homeFeatureLocalities)
        val rightTitleTV =
            featuredLocTitleIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.text = resources.getString(R.string.homeViewAllLocality)
        rightTitleTV.tag = "FeaturedLocalities"
        rightTitleTV.setOnClickListener(onClickListener)

        view.featuredLocalitiesRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        featuredLocalitiesRecyclerView.adapter =
            FeaturedLocalitiesAdapter(activity!!, ileUtility, featuredLocList)
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.homeItemsTitleRight -> {
                when (it.tag) {
                    //                    "AgentSearchTitle" -> Log.e("IncludeClicks", "Expand The MAP please")
                    "FeaturedProjectTitle" -> {
                        startActivity(Intent(activity, SeeAllPropertyActivity::class.java)
                            .putExtra(AppConstants.SEE_ALL_PROJECTS_KEY,
                                (featuredProjectResponse as FeaturedProjectAPIResponse).data))
                    }
                    "TopAgents" -> {
                        startActivity(Intent(activity, SeeAllAgentsActivity::class.java)
                            .putExtra(AppConstants.SEE_ALL_AGENT_KEY, topAgentsArrayList))
                    }
                    "TrendingProjects" -> {
                        startActivity(Intent(activity, SeeAllPropertyActivity::class.java)
                            .putExtra(AppConstants.SEE_ALL_PROJECTS_KEY,
                                (trendingProjectResponse as FeaturedProjectAPIResponse).data))
                    }
                    "ILERecommendation" -> {
                        startActivity(Intent(activity, SeeAllPropertyActivity::class.java)
                            .putExtra(AppConstants.SEE_ALL_PROJECTS_KEY,
                                (recommendationProjectResponse as FeaturedProjectAPIResponse).data))
                    }
                    "FeaturedLocalities" -> Log.e("IncludeClicks",
                        "FeaturedLocalities SEE All Clicked")
                }
            }
            R.id.homeBookAnAgent -> startActivity(Intent(activity, BookAgentActivity::class.java))
            R.id.homeSearchEditText -> startActivity(Intent(activity,
                HomeSearchActivity::class.java))
            R.id.homeHeaderBuyInTV -> homeViewModel.callingGetCitiesAPI()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}