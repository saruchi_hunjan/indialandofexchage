package com.landexchange.projectmodules.dashboardfragments.home

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 4/30/2019.
 */
class HomeContractor {
    interface HomeView : BaseView {
        fun onFeaturedProjectAPISuccessResponse(response: Any)
        fun onILERecommendationAPISuccessResponse(response: Any)
        fun onTrendingProjectAPISuccessResponse(response: Any)
        fun onCollectionAPISuccessResponse(response: Any)
        fun onTopAgentAPISuccessResponse(response: Any)
        fun onFeaturedLocalitiesAPISuccessResponse(response: Any)
        fun onPromotersAPISuccessResponse(response: Any)
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}