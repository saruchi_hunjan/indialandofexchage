package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.HomeSearchableDataMO
import kotlinx.android.synthetic.main.row_searchable_datas.view.*

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
class HomeSearchableAdapter(val context: Context, private var homeSearchableList: ArrayList<HomeSearchableDataMO>) :
    RecyclerView.Adapter<HomeSearchableAdapter.HomeSearchViewHolder>() {

    //    private lateinit var homeSearchableList: ArrayList<HomeSearchableDataMO>
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeSearchViewHolder {
        val view = layoutInflater.inflate(R.layout.row_searchable_datas, parent, false)
        return HomeSearchViewHolder(view)
    }

    override fun getItemCount(): Int {
        return homeSearchableList.size
    }

    override fun onBindViewHolder(holder: HomeSearchViewHolder, position: Int) {
        holder.bindHomeSearchableDataRows()
    }

    inner class HomeSearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindHomeSearchableDataRows() {

            itemView.homeLocDevProjectsTV.text = homeSearchableList[layoutPosition].localityDevProjects
            itemView.homeLocDevProjectsTypeTV.text = homeSearchableList[layoutPosition].localityDevProjectsType
        }
    }

    fun filterSearchDataList(filteredSearchableList: ArrayList<HomeSearchableDataMO>) {
        homeSearchableList = filteredSearchableList
        notifyDataSetChanged()
    }

}