package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
data class FeaturedProjectMO(

    @field:SerializedName("Name")
    var projectName: String? = null,

    @field:SerializedName("Price")
    var projectPrice: String? = null,

    @field:SerializedName("Image")
    var projectImageURL: String? = null,

    @field:SerializedName("shortdes")
    var projectShortDesc: String? = null,

    @field:SerializedName("prop_name")
    var propertyName: String? = null,

    @field:SerializedName("prop_type")
    var propertyType: String? = null,

    @field:SerializedName("prop_area")
    var propertyArea: String? = null,

    @field:SerializedName("beds")
    var propertyBeds: String? = null,

    @field:SerializedName("baths")
    var propertyBaths: String? = null,

    @field:SerializedName("garage")
    var propertyGarage: String? = null,

    @field:SerializedName("floor")
    var propertyFloor: String? = null
) : Serializable