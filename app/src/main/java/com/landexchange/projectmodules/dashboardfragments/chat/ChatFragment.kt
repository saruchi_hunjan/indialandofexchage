package com.landexchange.projectmodules.dashboardfragments.chat

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.chat.AddContactsForChat
import com.landexchange.projectmodules.dashboardfragments.chat.adapters.UsersChatListAdapter
import com.landexchange.util.AppConstants
import com.landexchange.util.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_chat_list.*

/**
 * Created by Ashu Rajput on 10/11/2019.
 */
class ChatFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = ChatFragment()
    }

    var chatUserList: ArrayList<MyChatUserListMO>? = null
    private lateinit var userChatListRecyclerView: RecyclerView

    override fun getLayoutId(): Int {
        return R.layout.fragment_chat_list
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        userChatListRecyclerView = view!!.findViewById(R.id.userChatListRecyclerView)

        if (chatUserList.isNullOrEmpty())
            fetchListOfChattedUsers()
        else
            Log.e("ChatFragment", "chatUserList is NOT null....Don't do anything..")

        addContactsToChat.setOnClickListener {
            startActivity(Intent(activity, AddContactsForChat::class.java))
        }
    }

    private fun fetchListOfChattedUsers() {
        Log.e("ChatFragment", "Coming to update chat list...")
        //[The one who is sending the msg to another user]
//        val senderChatUserId = "POhdIbQhq9bBl9DznmwhlreCEHf1"

        showProgressDialog("Loading chat history...")
        val senderChatUserId = appPreferences.fireBaseUniqueUserId

        val dbReference = FirebaseDatabase.getInstance()
            .reference.child(AppConstants.FB_CHAT_USER_LIST_KEY).child(senderChatUserId)

        chatUserList = ArrayList()

        dbReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(dbSnapshot: DataSnapshot) {

                hideProgressDialog()

                for (chatData in dbSnapshot.children) {
                    val userChatMO = chatData.getValue(MyChatUserListMO::class.java)
                    if (userChatMO!!.userId != senderChatUserId)
                        chatUserList!!.add(userChatMO)
                }

                if (chatUserList!!.size > 0) {
                    userChatListRecyclerView.layoutManager = LinearLayoutManager(activity)
                    val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(context!!,
                        R.drawable.recycler_divider)!!)
                    userChatListRecyclerView.addItemDecoration(itemDecorator)
                    userChatListRecyclerView.adapter =
                        UsersChatListAdapter(activity!!, chatUserList!!)
                } else {
                    userChatListRecyclerView.visibility = View.GONE
                    noChatConversationFoundMessageTV.visibility = View.VISIBLE
                }
            }
        })

    }
}