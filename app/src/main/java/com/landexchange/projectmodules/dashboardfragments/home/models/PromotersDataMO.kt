package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 7/2/2019.
 */
data class PromotersDataMO(
    @field:SerializedName("Name")
    var promotersName: String? = null,

    @field:SerializedName("Image")
    var promotersImageUrl: String? = null
)