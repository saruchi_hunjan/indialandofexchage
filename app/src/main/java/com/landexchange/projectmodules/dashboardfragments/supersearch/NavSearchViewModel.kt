package com.landexchange.projectmodules.dashboardfragments.supersearch

import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 6/11/2019.
 */
class NavSearchViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<NavSearchContractor.SuperSearchView>() {

    fun callingGetCitiesAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            io.reactivex.Observable.just(1)
                .subscribeOn(Schedulers.computation())
                .flatMap { remoteRepository.citiesAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, {
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("CityError")
                })
        )
    }
}