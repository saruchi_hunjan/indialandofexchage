package com.landexchange.projectmodules.dashboardfragments.home.models

/**
 * Created by Ashu Rajput on 5/20/2019.
 */
data class HomeSearchableDataMO(val localityDevProjects: String, val localityDevProjectsType: String)