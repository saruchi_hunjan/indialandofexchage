package com.landexchange.projectmodules.dashboardfragments.supersearch

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.dashboardfragments.supersearch.NavSearchContractor.SuperSearchView
import com.landexchange.projectmodules.dashboardfragments.supersearch.adapters.SavedSearchesAdapter
import com.landexchange.projectmodules.dashboardfragments.supersearch.adapters.SeenProjectsAdapter
import com.landexchange.projectmodules.filters.FiltersMainActivity
import com.landexchange.projectmodules.iamlookingto.adapters.CitiesAdapter
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse
import com.landexchange.projectmodules.iamlookingto.models.CitiesMO
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.iledialog.CityDialogListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.view.savedSearchesRecyclerView
import kotlinx.android.synthetic.main.fragment_super_search.*
import kotlinx.android.synthetic.main.fragment_super_search.view.*
import kotlinx.android.synthetic.main.fragment_super_search.view.seenProjectsRecyclerView
import kotlinx.android.synthetic.main.include_looking_into.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/11/2019.
 */
class NavSearchFragment : BaseFragment(), SuperSearchView, CityDialogListener {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var superSearchViewModelFactory: NavSearchViewModelFactory

    private lateinit var superSearchViewModel: NavSearchViewModel

    private var lookingToOptionsArray: List<TextView>? = null

    companion object {
        @JvmStatic
        fun newInstance() = NavSearchFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_super_search
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        superSearchViewModel =
            ViewModelProvider(this, superSearchViewModelFactory).get(NavSearchViewModel::class.java)
        superSearchViewModel.setView(this)
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        lookingToBuyButton.setOnClickListener(clickListener)
        lookingToRentButton.setOnClickListener(clickListener)
        lookingToCommercial.setOnClickListener(clickListener)
        viewPropertiesButton.setOnClickListener(clickListener)
        headerFiltersButton.setOnClickListener(clickListener)
        lookingToOptionsArray =
            listOf<TextView>(lookingToBuyButton, lookingToRentButton, lookingToCommercial)
        superSearchViewModel.callingGetCitiesAPI()
        viewPropertiesButton.text = resources.getString(R.string.dynamicPropertiesCount, "200")

        updateSavedSearchesTileItems()
        updateSeenProjectTileItems()
    }

    override fun onSuccess(response: Any) {
        if (response is CitiesAPIResponse)
            updateCityRecyclerView(response.data!!)
    }

    override fun onFailure(appErrorMessage: String) {
        if ("CityError" == appErrorMessage)
            updateCityRecyclerView(getCityDataFromJsonFile())
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    override fun onCitySelected(cityName: String, cityCode: Int) {
    }

    private fun updateCityRecyclerView(citiesList: ArrayList<CitiesMO>) {
        searchCitiesRecyclerView.adapter = CitiesAdapter(activity!!, citiesList, this, true)
    }

    private fun getCityDataFromJsonFile(): ArrayList<CitiesMO> {
        var citiesJSONResponse: CitiesAPIResponse? = null
        try {
            val cityJsonString =
                resources.openRawResource(R.raw.city_list).bufferedReader().use { it.readText() }
            citiesJSONResponse = Gson().fromJson(cityJsonString, CitiesAPIResponse::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return citiesJSONResponse!!.data!!
    }

    private fun updateSavedSearchesTileItems() {

        val savedSearchesIncludeView = view.includeSavedSearchesLayout
        val leftTitleTV = savedSearchesIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.profileTitleSavedSearches)
        val rightTitleTV =
            savedSearchesIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "SavedSearches"
        rightTitleTV.setOnClickListener(clickListener)

        view.savedSearchesRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL,
                false)
        savedSearchesRecyclerView.adapter = SavedSearchesAdapter(activity!!, ileUtility)
    }

    private fun updateSeenProjectTileItems() {

        val savedSearchesIncludeView = view.includeSeenProjectsLayout
        val leftTitleTV = savedSearchesIncludeView.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.profileTitleSeenProjects)
        val rightTitleTV =
            savedSearchesIncludeView.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "SeenProjects"
        rightTitleTV.setOnClickListener(clickListener)

        view.seenProjectsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL,
                false)
        seenProjectsRecyclerView.adapter = SeenProjectsAdapter(activity!!, ileUtility, ArrayList(),
            true)
    }

    private val clickListener = View.OnClickListener { view ->
        when {
            view.id == R.id.lookingToBuyButton -> updateSelectorOnLookingOptions(R.id.lookingToBuyButton)
            view.id == R.id.lookingToRentButton -> updateSelectorOnLookingOptions(R.id.lookingToRentButton)
            view.id == R.id.lookingToCommercial -> updateSelectorOnLookingOptions(R.id.lookingToCommercial)
            view.id == R.id.viewPropertiesButton -> {
            }
            view.id == R.id.headerFiltersButton -> {
                startActivity(Intent(activity, FiltersMainActivity::class.java)
                    .putExtra(AppConstants.FILTER_TYPE_KEY, AppConstants.NAV_SEARCH_FILTER))
            }
            view.id == R.id.homeItemsTitleRight -> {
                when (view.tag) {
                    "SavedSearches" -> Log.e("IncludeClicks", "SavedSearches  SEE All Clicked")
                    "SeenProjects" -> Log.e("IncludeClicks", "SeenProjects SEE All Clicked")
                }
            }
        }
    }

    private fun updateSelectorOnLookingOptions(textViewId: Int) {
        compositeDisposable.add(
            Observable.fromIterable(lookingToOptionsArray)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (textViewId == it.id) {
                        it.background =
                            ContextCompat.getDrawable(activity!!, R.drawable.blue_button_unpressed)
                        it.setTextColor(ContextCompat.getColor(activity!!, R.color.colorBlueLinks))
                    } else {
                        it.background =
                            ContextCompat.getDrawable(activity!!,
                                R.drawable.white_stroke_transparent_solid_curve)
                        it.setTextColor(ContextCompat.getColor(activity!!, R.color.colorWhite))
                    }
                }
        )
    }
}