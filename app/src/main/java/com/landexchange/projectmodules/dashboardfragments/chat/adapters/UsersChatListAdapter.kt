package com.landexchange.projectmodules.dashboardfragments.chat.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.chat.OneToOneChatActivity
import com.landexchange.projectmodules.dashboardfragments.chat.MyChatUserListMO
import kotlinx.android.synthetic.main.row_user_chat_list.view.*

/**
 * Created by Ashu Rajput on 10/17/2019.
 */
class UsersChatListAdapter(val context: Context,
    private val chatUserList: ArrayList<MyChatUserListMO>,
    private var isComingToSelectContact: Boolean = false) :
    RecyclerView.Adapter<UsersChatListAdapter.ChatListViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListViewHolder {
        val view = layoutInflater.inflate(R.layout.row_user_chat_list, parent, false)
        return ChatListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return chatUserList.size
    }

    override fun onBindViewHolder(holder: ChatListViewHolder, position: Int) {
        holder.bindView()
    }

    inner class ChatListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView() {
            itemView.chatUserNameTV.text = chatUserList[layoutPosition].userName
            if (isComingToSelectContact)
                itemView.chatLastMsgSentTV.text = context.resources.getString(R.string.chatDeafultLastMsg)
            else
                itemView.chatLastMsgSentTV.text = chatUserList[layoutPosition].lastMsgSent

            itemView.setOnClickListener {
                context.startActivity(Intent(context, OneToOneChatActivity::class.java)
                    .putExtra("ChatWithUserId", chatUserList[layoutPosition].userId)
                    .putExtra("ChatWithUserName", chatUserList[layoutPosition].userName)
                    .putExtra("ChatWithFCMTokenID", chatUserList[layoutPosition].fcmTokenId))

                if(isComingToSelectContact)
                    (context as Activity).finish()
            }
        }
    }
}