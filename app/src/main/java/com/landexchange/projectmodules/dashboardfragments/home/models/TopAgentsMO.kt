package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
data class TopAgentsMO(

    @field:SerializedName("Firstname")
    var firstname: String? = null,

    @field:SerializedName("Lastname")
    var lasttname: String? = null,

    @field:SerializedName("Image")
    var agentImageURL: String? = null,

    @field:SerializedName("Project")
    var projectCount: String? = null
) : Serializable