package com.landexchange.projectmodules.dashboardfragments.account.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.accountoptions.AccountOptionsActivity
import com.landexchange.projectmodules.bookagent.BookAgentActivity
import com.landexchange.util.AppConstants
import com.landexchange.util.iledialog.DialogRateILE
import com.landexchange.util.iledialog.DialogYesNo
import kotlinx.android.synthetic.main.row_account_options.view.*

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class AccountFragOptionsAdapter(val context: Context) :
    RecyclerView.Adapter<AccountFragOptionsAdapter.AccountOptionsViewHolder>() {

    private val optionsList = arrayListOf(context.resources.getString(R.string.accountNotification),
        context.resources.getString(R.string.accountSellRent),
        context.resources.getString(R.string.accountBookAgent),
        context.resources.getString(R.string.accountFeedbackRate),
        context.resources.getString(R.string.accountSettings),
        context.resources.getString(R.string.accountLogout))

    private val optionListIcons = arrayListOf(R.drawable.ic_account_notification,
        R.drawable.ic_account_sellrent,
        R.drawable.ic_account_bookagent,
        R.drawable.ic_account_feedback_rate,
        R.drawable.ic_account_setting,
        R.drawable.ic_account_logout)

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountOptionsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_account_options, parent, false)
        return AccountOptionsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return optionsList.size
    }

    override fun onBindViewHolder(holder: AccountOptionsViewHolder, position: Int) {
        holder.bindAccountOptionsViews()
    }

    inner class AccountOptionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindAccountOptionsViews() {
            itemView.accountOptionsValue.text = optionsList[layoutPosition]
            if (layoutPosition == optionListIcons.size - 1)
                itemView.accountOptionsValue.setCompoundDrawablesWithIntrinsicBounds(optionListIcons[layoutPosition],
                    0, 0, 0)
            else
                itemView.accountOptionsValue.setCompoundDrawablesWithIntrinsicBounds(optionListIcons[layoutPosition],
                    0, R.drawable.ic_blue_forward_arrow, 0)

            itemView.setOnClickListener {
                when (layoutPosition) {
                    0 -> context.startActivity(Intent(context, AccountOptionsActivity::class.java)
                        .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_NOTIFICATION))
                    2 -> context.startActivity(Intent(context, BookAgentActivity::class.java))
                    3 -> {
                        val rateILEDialog = DialogRateILE()
                        val fragTxn = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                        rateILEDialog.show(fragTxn, "RateILEDialog")
                        rateILEDialog.isCancelable = false
                    }
                    4 -> context.startActivity(Intent(context, AccountOptionsActivity::class.java)
                        .putExtra(AppConstants.ACCOUNT_MODULE_TYPE, AppConstants.ACCOUNT_MODULE_SETTINGS))
                    5 -> {
                        val logoutDialog = DialogYesNo.newInstance(AppConstants.DIALOG_TYPE_LOGOUT)
                        val fragTxn = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                        logoutDialog.show(fragTxn, context!!.resources.getString(R.string.fragTagNameLogoutDialog))
                        logoutDialog.isCancelable = false
                    }
                }
            }
        }
    }
}