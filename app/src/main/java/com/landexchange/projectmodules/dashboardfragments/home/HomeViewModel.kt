package com.landexchange.projectmodules.dashboardfragments.home

import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.*
import com.landexchange.IleApplication
import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 4/30/2019.
 */
class HomeViewModel constructor(val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository, val compositeDisposable: CompositeDisposable) :
    BaseViewModel<HomeContractor.HomeView>() {

    private val locationData: MutableLiveData<Location> = MutableLiveData()
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    init {
        getLocation()
    }

    private fun getLocation() {
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(IleApplication.getInstance())
        fusedLocationClient.let {
            it.lastLocation.addOnSuccessListener { location: Location? ->
                if (location != null) {
                    Log.e("Location", "Lat in ViewModel " + location.latitude)
                    locationData.value = location
                } else {
                    requestNewLocation()
                }
            }
        }
    }

    fun getLocationData(): LiveData<Location> {
        return locationData
    }

    private val locationCallBack = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult?) {
            for (location in p0!!.locations) {
                if (location != null) {
                    Log.e("Location", "LatLong in CallBack " + location.latitude)
                    removeCallBack()
                    break
                }
            }
        }
    }

    private fun removeCallBack() {
        fusedLocationClient.removeLocationUpdates(locationCallBack)
    }

    private fun requestNewLocation() {
        val locationRequest = LocationRequest()
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallBack, Looper.myLooper())
    }

    fun callingGetFeaturedProjectAPI(queryMap: HashMap<String, Int>) {
        //  getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.featuredProjectAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFeaturedProjectAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetILERecommendationAPI(queryMap: HashMap<String, Int>) {
        //        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.ileRecommendationAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onILERecommendationAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetTrendingProjectAPI(queryMap: HashMap<String, Int>) {
        //        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.trendingProjectAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onTrendingProjectAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetCollectionsAPI(queryMap: HashMap<String, Int>) {
        //        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.collectionsAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onCollectionAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetTopAgentsAPI(queryMap: HashMap<String, Int>) {
        //        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.topAgentsAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onTopAgentAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetFeaturedLocalitiesAPI(queryMap: HashMap<String, Int>) {
        //        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.featuredLocalitiesAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFeaturedLocalitiesAPISuccessResponse(commonResponse)
                }, {
                    //                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

    fun callingGetCitiesAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.citiesAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, {
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred in Cities API")
                })
        )
    }

    fun callingGetPromotersAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.promotersAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onPromotersAPISuccessResponse(commonResponse)
                }, {
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred in Promoters API")
                })
        )
    }

}