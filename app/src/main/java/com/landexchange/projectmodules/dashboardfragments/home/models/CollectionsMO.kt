package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
data class CollectionsMO(

    @field:SerializedName("Collection")
    var collection: String? = null,

    @field:SerializedName("Image")
    var collectionImageURL: String? = null,

    @field:SerializedName("count")
    var projectCount: Int? = null
)