package com.landexchange.projectmodules.dashboardfragments.chat


/**
 * Created by Ashu Rajput on 10/17/2019.
 */
data class MyChatUserListMO(var userId: String? = null, var lastMsgSent: String? = null,
    var msgDateTime: String? = null, var userImageUrl: String? = null, var userName: String? = null,
    var fcmTokenId: String? = null)