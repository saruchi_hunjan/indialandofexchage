package com.landexchange.projectmodules.dashboardfragments.supersearch.adapters

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_feature_project.view.*

/**
 * Created by Ashu Rajput on 5/16/2019.
 */
class SeenProjectsAdapter() : RecyclerView.Adapter<SeenProjectsAdapter.YourListingsViewHolder>() {
    private lateinit var context: Context
    private lateinit var ileUtility: ILEUtility
    private var isSeenProjectFlag: Boolean = false

    private lateinit var layoutInflater: LayoutInflater
    private var deviceWidth = 0
    private var deviceHeight = 0

    constructor(context: Context, ileUtility: ILEUtility, featureProjectList: ArrayList<FeaturedProjectMO>) : this() {
        this.context = context
        this.ileUtility = ileUtility
        layoutInflater = LayoutInflater.from(context)
    }

    constructor(context: Context, ileUtility: ILEUtility, featureProjectList: ArrayList<FeaturedProjectMO>,
        isSeenProjectFlag: Boolean) : this() {
        this.context = context
        this.ileUtility = ileUtility
        this.isSeenProjectFlag = isSeenProjectFlag
        layoutInflater = LayoutInflater.from(context)
    }

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YourListingsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_feature_project, parent, false)
        return YourListingsViewHolder(view)
    }

    override fun getItemCount(): Int {
//        return featureProjectList.size
        return 5
    }

    override fun onBindViewHolder(holder: YourListingsViewHolder, position: Int) {
        holder.bindFeaturedProjectItems()
    }

    inner class YourListingsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeaturedProjectItems() {
            try {
                itemView.featureProjectRowParentLayout.layoutParams.width = deviceWidth
                itemView.featureProjectRowParentLayout.layoutParams.height = deviceHeight

                /* itemView.projectDeveloperOrAgentTV.text = ileUtility.fromHtml(
                     featureProjectList[layoutPosition].projectName + "<br>" +
                             "<small><font color='#9e9e9e'>" + featureProjectList[layoutPosition].projectShortDesc + "</font></small>"
                 )
                 itemView.projectAptDetailsCumAddressTV.text = ileUtility.fromHtml(
                     featureProjectList[layoutPosition].propertyType + " " +
                             featureProjectList[layoutPosition].propertyName +
                             "<br>" + "<small><font color='#9e9e9e'>Address</font></small>"
                 )

                 itemView.projectArea.text = featureProjectList[layoutPosition].propertyArea
                 itemView.projectBedsCountTV.text = featureProjectList[layoutPosition].propertyBeds
                 itemView.projectBathCountTV.text = featureProjectList[layoutPosition].propertyBaths
                 itemView.projectGarageCountTV.text = featureProjectList[layoutPosition].propertyGarage
                 itemView.projectFloorCountTV.text = featureProjectList[layoutPosition].propertyFloor*/

                /*Picasso.get()
                    .load(featureProjectList[layoutPosition].projectImageURL)
                    .tag(context)
                    .into(itemView.projectImage)*/

                if (!isSeenProjectFlag)
                    itemView.projectActiveOrInactiveTV.visibility = View.VISIBLE

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = getDeviceWidth - (getDeviceWidth / 4)
            deviceHeight = getDeviceWidth - (getDeviceWidth/7)
        }
    }
}