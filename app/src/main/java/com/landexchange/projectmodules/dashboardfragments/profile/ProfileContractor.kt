package com.landexchange.projectmodules.dashboardfragments.profile

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 5/16/2019.
 */
class ProfileContractor {
    interface ProfileView : BaseView {
        fun onYourListingsAPISuccessResponse(response: Any)
        fun onSavedSearchesAPISuccessResponse(response: Any)
        fun onSeenProjectsAPISuccessResponse(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}