package com.landexchange.projectmodules.dashboardfragments.home.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 7/2/2019.
 */
data class PromotersAPIResponse(
    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: ArrayList<PromotersDataMO>? = null)