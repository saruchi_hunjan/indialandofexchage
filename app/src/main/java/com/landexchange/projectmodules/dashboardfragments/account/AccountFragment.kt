package com.landexchange.projectmodules.dashboardfragments.account

import android.annotation.SuppressLint
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.view.clicks
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.accountoptions.editprofile.EditProfileActivity
import com.landexchange.projectmodules.dashboardfragments.account.adapters.AccountFragOptionsAdapter
import com.landexchange.util.DividerItemDecoration
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class AccountFragment : BaseFragment() {

    /*@Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable*/

    companion object {
        @JvmStatic
        fun newInstance() = AccountFragment()
    }

    override fun initializeDagger() {
//        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_account
    }

    @SuppressLint("CheckResult")
    override fun setUpUi() {
        settingAccountOptionsRecyclerView()

        /*compositeDisposable.add(
            view.profileEditButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
                startActivity(Intent(activity, EditProfileActivity::class.java))
            })*/

        view.profileEditButton.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe {
            startActivity(Intent(activity, EditProfileActivity::class.java))
        }
    }

    private fun settingAccountOptionsRecyclerView() {
        accountOptionsRecyclerView.layoutManager = LinearLayoutManager(activity)
        val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)!!)
        accountOptionsRecyclerView.addItemDecoration(itemDecorator)
        accountOptionsRecyclerView.adapter = AccountFragOptionsAdapter(activity!!)
    }

    /*override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }*/
}