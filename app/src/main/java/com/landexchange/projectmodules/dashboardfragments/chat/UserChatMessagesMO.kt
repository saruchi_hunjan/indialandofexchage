package com.landexchange.projectmodules.dashboardfragments.chat

/**
 * Created by Ashu Rajput on 10/17/2019.
 */
data class UserChatMessagesMO(var userName: String? = null, var userId: String? = null,
    var msgDateTime: String? = null, var userMsg: String? = null, var fcmTokenId: String? = null,
    var notificationId: Int? = null)