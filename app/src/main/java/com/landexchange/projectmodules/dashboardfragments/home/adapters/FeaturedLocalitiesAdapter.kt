package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedLocalitiesMO
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_featured_localities.view.*

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
class FeaturedLocalitiesAdapter(
    val context: Context,
    val ileUtility: ILEUtility,
    val featuredLocList: ArrayList<FeaturedLocalitiesMO>
) :
    RecyclerView.Adapter<FeaturedLocalitiesAdapter.FeaturedLocalitiesViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0

    init {
        try {
            deviceWidth = ileUtility.getDeviceWidthForFeaturedLocalitiesTiles()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedLocalitiesViewHolder {
        val view = layoutInflater.inflate(R.layout.row_featured_localities, parent, false)
        return FeaturedLocalitiesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return featuredLocList.size
    }

    override fun onBindViewHolder(holder: FeaturedLocalitiesViewHolder, position: Int) {
        holder.bindFeaturedLocalityItems()
    }

    inner class FeaturedLocalitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeaturedLocalityItems() {
            try {
                itemView.featuredLocalitiesRowParentLayout.layoutParams.width = deviceWidth

                itemView.areaName.text = featuredLocList[layoutPosition].area

                itemView.areaPerSqFeet.text = ileUtility.fromHtml(context.resources.getString(R.string.perSquareFeet,
                    featuredLocList[layoutPosition].perSqrFt))

                if (featuredLocList[layoutPosition].growth == "DOWN") {
                    itemView.percentageGrowthOrDown.background = ContextCompat.getDrawable(context,
                        R.drawable.redish_round_border)
                    itemView.percentageGrowthOrDown.setCompoundDrawablesWithIntrinsicBounds(0,
                        0, R.drawable.ic_green_up_arrow, 0)
                    itemView.percentageGrowthOrDown.setTextColor(ContextCompat.getColor(context, R.color.colorRedLight))
                }
                itemView.percentageGrowthOrDown.text = featuredLocList[layoutPosition].percentage

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}