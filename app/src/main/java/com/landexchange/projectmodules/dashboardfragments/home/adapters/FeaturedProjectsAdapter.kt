package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.PropertyDetailsActivity
import com.landexchange.util.ILEUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_feature_project.view.*

/**
 * Created by Ashu Rajput on 5/9/2019.
 */
class FeaturedProjectsAdapter(val context: Context, val ileUtility: ILEUtility,
    private val featureProjectList: ArrayList<FeaturedProjectMO>) :
    RecyclerView.Adapter<FeaturedProjectsAdapter.FeaturedProjectViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedProjectViewHolder {
        val view = layoutInflater.inflate(R.layout.row_feature_project, parent, false)
        return FeaturedProjectViewHolder(view)
    }

    override fun getItemCount(): Int {
        return featureProjectList.size
    }

    override fun onBindViewHolder(holder: FeaturedProjectViewHolder, position: Int) {
        holder.bindFeaturedProjectItems()
    }

    inner class FeaturedProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeaturedProjectItems() {
            try {
                itemView.featureProjectRowParentLayout.layoutParams.width = deviceWidth
                itemView.featureProjectRowParentLayout.layoutParams.height = deviceHeight

                itemView.propertyRatingWithStar.visibility = View.GONE
                itemView.propertyFavouriteButton.visibility = View.VISIBLE

                itemView.projectDeveloperOrAgentTV.text = ileUtility.fromHtml(
                    featureProjectList[layoutPosition].projectName + "<br>" +
                            "<small><font color='#9e9e9e'>" + featureProjectList[layoutPosition].projectShortDesc + "</font></small>"
                )
                itemView.projectAptDetailsCumAddressTV.text = ileUtility.fromHtml(
                    featureProjectList[layoutPosition].propertyType + " " +
                            featureProjectList[layoutPosition].propertyName +
                            "<br>" + "<small><font color='#9e9e9e'>Address</font></small>"
                )

                itemView.projectArea.text = featureProjectList[layoutPosition].propertyArea
                itemView.projectBedsCountTV.text = featureProjectList[layoutPosition].propertyBeds
                itemView.projectBathCountTV.text = featureProjectList[layoutPosition].propertyBaths
                itemView.projectGarageCountTV.text = featureProjectList[layoutPosition].propertyGarage
                itemView.projectFloorCountTV.text = featureProjectList[layoutPosition].propertyFloor

                Picasso.get()
                    .load(featureProjectList[layoutPosition].projectImageURL)
                    .tag(context)
                    .into(itemView.projectImage)

                itemView.setOnClickListener {
                    context.startActivity(Intent(context, PropertyDetailsActivity::class.java))
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = getDeviceWidth - (getDeviceWidth / 4)
            deviceHeight = getDeviceWidth - 30
        }
    }
}