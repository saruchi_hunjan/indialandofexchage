package com.landexchange.projectmodules.dashboardfragments.favourites

import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.dashboardfragments.favourites.adapters.FavouritePropertyAdapter
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.fragment_favourites.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class FavouritesFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = FavouritesFragment()
    }

    @Inject
    lateinit var ileUtility: ILEUtility

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_favourites
    }

    override fun setUpUi() {
        setFavouritesPropertyRecyclerView()
    }

    private fun setFavouritesPropertyRecyclerView() {
        favouritesPropertyRecyclerView.layoutManager = LinearLayoutManager(activity)
        favouritesPropertyRecyclerView.adapter = FavouritePropertyAdapter(activity!!, ileUtility)
    }
}