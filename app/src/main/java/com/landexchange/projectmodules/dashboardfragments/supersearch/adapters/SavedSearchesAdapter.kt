package com.landexchange.projectmodules.dashboardfragments.supersearch.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_saved_searches.view.*

/**
 * Created by Ashu Rajput on 5/17/2019.
 */
class SavedSearchesAdapter(val context: Context, val ileUtility: ILEUtility) :
    RecyclerView.Adapter<SavedSearchesAdapter.SavedSearchesViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0

    init {
        try {
            deviceWidth = ileUtility.getDeviceWidthForFeaturedLocalitiesTiles()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedSearchesViewHolder {
        val view = layoutInflater.inflate(R.layout.row_saved_searches, parent, false)
        return SavedSearchesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: SavedSearchesViewHolder, position: Int) {
        holder.bindSavedSearchesItems()
    }

    inner class SavedSearchesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindSavedSearchesItems() {
            try {
                itemView.searchedLocalityTV.layoutParams.width = deviceWidth
                /*itemView.searchedLocalityTV.text = "Greater Kailash"
                itemView.searchedLocalityDetailsTV.text = "Max 1 Cr, 2BHK, New Projects"*/
                itemView.searchedLocalityTV.text =
                    ileUtility.fromHtml(
                        "Greater Kailash<br><br>" +
                                "<b><small><font color='#888888'>" + "Max 1 Cr, 2BHK, New Projects" + "</font></small></b>"
                    )

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


}