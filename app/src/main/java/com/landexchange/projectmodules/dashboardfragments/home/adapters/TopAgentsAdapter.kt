package com.landexchange.projectmodules.dashboardfragments.home.adapters

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.TopAgentsMO
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_top_agents.view.*

/**
 * Created by Ashu Rajput on 5/10/2019.
 */
class TopAgentsAdapter(val context: Context) :
    RecyclerView.Adapter<TopAgentsAdapter.TopAgentsViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0
    private var agentDataList = ArrayList<TopAgentsMO>()

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopAgentsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_top_agents, parent, false)
        return TopAgentsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return agentDataList.size
    }

    override fun onBindViewHolder(holder: TopAgentsViewHolder, position: Int) {
        holder.bindTopAgentsItems()
    }

    inner class TopAgentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindTopAgentsItems() {
            try {
                itemView.topAgentsRowParentLayout.layoutParams.width = deviceWidth
                itemView.topAgentsRowParentLayout.layoutParams.height = deviceHeight

                Picasso.get()
                    .load(agentDataList[layoutPosition].agentImageURL)
                    .tag(context)
                    .into(itemView.agentImageView)

                itemView.agentFullName.text = agentDataList[layoutPosition].firstname +
                        agentDataList[layoutPosition].lasttname

                itemView.agentProjectCount.text = agentDataList[layoutPosition].projectCount

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = (getDeviceWidth / 3) + (getDeviceWidth / 15)
            deviceHeight = (getDeviceWidth / 3) + (getDeviceWidth / 7)
        }
    }

    fun updateTopAgentRecyclerView(agentDataList: ArrayList<TopAgentsMO>) {
        this.agentDataList = agentDataList
        notifyDataSetChanged()
    }
}