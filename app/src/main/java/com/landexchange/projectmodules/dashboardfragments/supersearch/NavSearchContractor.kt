package com.landexchange.projectmodules.dashboardfragments.supersearch

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 6/11/2019.
 */
class NavSearchContractor {
    interface SuperSearchView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}