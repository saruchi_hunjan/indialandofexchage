package com.landexchange.projectmodules.login

import com.landexchange.base.BaseViewModel
import com.landexchange.projectmodules.login.models.LoginRequestBodyMO
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException


/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class LoginViewModel constructor(val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository, val compositeDisposable: CompositeDisposable) :
    BaseViewModel<LoginContractor.LoginView>() {

    //    fun callingValidateMobileNumberAPI(mobileNo: String) {
    fun callingValidateMobileNumberAPI(loginBody: LoginRequestBodyMO) {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.loginAPICall(loginBody) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, { throwable ->
                    getView()!!.setLoaderVisibility(false)
                    if (throwable is HttpException)
                        getView()!!.onFailure(throwable.code())
                    else
                        getView()!!.onFailure(500)
                }))
    }

    fun callingResendOtpAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.resendOtpAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccessResendOTP(commonResponse.apiMessage!!)
                }, { throwable ->
                    getView()!!.setLoaderVisibility(false)
                    if (throwable is HttpException)
                        getView()!!.onFailure(throwable.code())
                    else
                        getView()!!.onFailure(500)
                }))
    }
}