package com.landexchange.projectmodules.login.models

/**
 * Created by Ashu Rajput on 6/22/2019.
 */
data class UserInfoMO(val userFirstName: String,
    val userLastName: String, val userEmail: String, val userMobileNo: String)