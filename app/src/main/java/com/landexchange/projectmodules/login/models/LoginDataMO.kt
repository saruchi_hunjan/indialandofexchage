package com.landexchange.projectmodules.login.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/8/2019.
 */
data class LoginDataMO(
    @field:SerializedName("message")
    var message: String? = null,

    //    @field:SerializedName("Type") // Previously Used
    @field:SerializedName("user_type")
    var type: String? = null,

    @field:SerializedName("name")
    var fullName: String? = null,

    @field:SerializedName("firstname")
    var firstName: String? = null,

    @field:SerializedName("lastname")
    var lastName: String? = null,

    @field:SerializedName("api_token")
    var apiToken: String? = null,

    @field:SerializedName("is_phone_verified")
    var isPhoneVerified: String? = null,

    @field:SerializedName("email")
    var email: String? = null,

    @field:SerializedName("otp")
    var mobileOTP: String? = null,

    //    @field:SerializedName("mobile")
    @field:SerializedName("phonenumber")
    var mobile: String? = null
)