package com.landexchange.projectmodules.login.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/6/2019.
 */
data class LoginAPIResponse(

    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: LoginDataDetailsMO? = null
)