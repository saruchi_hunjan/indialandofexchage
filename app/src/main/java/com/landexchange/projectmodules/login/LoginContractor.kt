package com.landexchange.projectmodules.login

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class LoginContractor {
    interface LoginView : BaseView {
        fun onSuccess(response: Any)
        fun onSuccessResendOTP(message: String)
        fun onFailure(errorResponse: Any)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}