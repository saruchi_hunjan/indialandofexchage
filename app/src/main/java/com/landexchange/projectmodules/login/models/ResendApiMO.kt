package com.landexchange.projectmodules.login.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 1/20/2020.
 */
data class ResendApiMO(
    @field:SerializedName("status")
    var apiStatus: String? = null,

    @field:SerializedName("message")
    var apiMessage: String? = null
)