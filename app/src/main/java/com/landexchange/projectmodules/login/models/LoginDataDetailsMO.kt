package com.landexchange.projectmodules.login.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 1/20/2020.
 */
data class LoginDataDetailsMO(

    @field:SerializedName("user")
    var userInfo: LoginDataMO? = null,

    @field:SerializedName("token")
    var apiToken: String? = null,

    @field:SerializedName("message")
    var apiMessage: String? = null
)