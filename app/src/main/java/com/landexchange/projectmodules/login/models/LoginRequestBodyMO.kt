package com.landexchange.projectmodules.login.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 12/10/2019.
 */
data class LoginRequestBodyMO(
    @field:SerializedName("username")
    var username: String? = null,

    @field:SerializedName("password")
    var password: String? = null
)