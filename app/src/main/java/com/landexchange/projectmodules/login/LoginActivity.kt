package com.landexchange.projectmodules.login

import android.content.Intent
import android.graphics.Typeface
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.chat.ChatUserMO
import com.landexchange.projectmodules.iamlookingto.IAmLookingActivity
import com.landexchange.projectmodules.login.LoginContractor.LoginView
import com.landexchange.projectmodules.login.models.LoginAPIResponse
import com.landexchange.projectmodules.login.models.LoginRequestBodyMO
import com.landexchange.projectmodules.login.models.UserInfoMO
import com.landexchange.projectmodules.webpagelinks.ILEWebPages
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.toolbar_header_dark.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class LoginActivity : BaseActivity(), LoginView {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    lateinit var loginViewModelFactory: LoginViewModelFactory

    private lateinit var loginViewModel: LoginViewModel

    private var isGoForwardClickedByUser = false

    private lateinit var selectedCountryCode: String

    private lateinit var loginResponse: LoginAPIResponse

    private lateinit var auth: FirebaseAuth

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        loginViewModel =
            ViewModelProvider(this, loginViewModelFactory).get(LoginViewModel::class.java)
        loginViewModel.setView(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_create_account
    }

    override fun setUpUi() {
        headerMainTitle.text = resources.getText(R.string.login_to_account)
        headerStepperMessageTV.text = resources.getText(R.string.enter_mob_no)
        headerSkipButton.visibility = View.GONE
        mobileNumberLayout.visibility = View.GONE
        createAccountNameET.visibility = View.GONE
        loginButton.visibility = View.GONE

        createTOUAndPrivacyPolicySpan()

        createAccountGoForward.setOnClickListener(clickListener)
        headerBackArrowButton.setOnClickListener(clickListener)
        createAccountResendOTP.setOnClickListener(clickListener)
        createAccountVerifyOTP.setOnClickListener(clickListener)
        initCountryCodePicker()

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
    }

    override fun onSuccess(response: Any) {
        loginResponse = response as LoginAPIResponse

        loginResponse.apply {
            when (statusMessage) {
                AppConstants.SUCCESS_STATUS_MESSAGE -> {
                    appPreferences.apiTokenId = data!!.apiToken

                    if (data!!.userInfo!!.isPhoneVerified.isNullOrBlank()) {
                        showOTPLayout(data!!.userInfo!!.mobile!!)
                    } else {
                        /*if (appPreferences.fcmId.isNotBlank() || appPreferences.fcmId.isNotEmpty())
                            loginUserFromFireBaseDB()
                        else
                            getFCMTokenIdFromFireBase()*/

                        moveToIAmLookingActivity()
                    }
                }
                AppConstants.FAILURE_STATUS_MESSAGE -> showSnakeBarMessages(createAccountMobileNoET,
                    data!!.apiMessage)
                else -> showSnakeBarMessages(createAccountMobileNoET,
                    resources.getString(R.string.internal_server_error))
            }
        }
    }

    override fun onSuccessResendOTP(message: String) {
        showSnakeBarMessages(createAccountOtpET, message)
    }

    override fun onFailure(errorResponse: Any) {
        if (errorResponse as Int == 401) showSnakeBarMessages(createAccountOtpET,
            resources.getString(R.string.error_username_pw))
        else showSnakeBarMessages(createAccountOtpET,
            resources.getString(R.string.internal_server_error))
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    private fun initCountryCodePicker() {
        selectedCountryCode = countryCodePicker.defaultCountryCodeWithPlus
        countryCodePicker.setOnCountryChangeListener {
            selectedCountryCode = "+" + it.phoneCode
        }
    }

    private val clickListener = View.OnClickListener { view ->
        if (view.id == R.id.headerBackArrowButton) {
            onBackPressed()
        } else if (view.id == R.id.createAccountGoForward) {
            /*if (createAccountMobileNoET.text.toString().isEmpty() && createAccountMobileNoET.text.toString().length < 10)
                showSnakeBarMessages(createAccountMobileNoET,
                    resources.getString(R.string.validationMsgValidMobNo))
            else {
                hideKeyboard(createAccountGoForward)
                loginViewModel.callingValidateMobileNumberAPI(createAccountMobileNoET.text.toString())
            }*/

            if (createAccountEmailET.text.toString().isEmpty() || createAccountEmailET.text.toString().trim().isEmpty())
                showSnakeBarMessages(createAccountEmailET,
                    resources.getString(R.string.validationMsgValidEmail))
            else if (createAccountPasswordET.text.toString().isEmpty() || createAccountPasswordET.text.toString().trim().isEmpty())
                showSnakeBarMessages(createAccountEmailET,
                    resources.getString(R.string.validationMsgValidPW))
            else {
                val loginRequestBody = LoginRequestBodyMO(createAccountEmailET.text.toString(),
                    createAccountPasswordET.text.toString())
                loginViewModel.callingValidateMobileNumberAPI(loginRequestBody)
            }

        } else if (view.id == R.id.createAccountVerifyOTP) {
            if (createAccountOtpET.text.toString().isEmpty() && createAccountOtpET.text.toString().length < 4)
                showSnakeBarMessages(createAccountOtpET,
                    resources.getString(R.string.validationMsgValidDigitOTP))
            else if (createAccountOtpET.text.toString() != loginResponse.data!!.userInfo!!.mobileOTP) {
                showSnakeBarMessages(createAccountOtpET,
                    resources.getString(R.string.validationMsgValidOTP))
            } else
                moveToIAmLookingActivity()

        } else if (view.id == R.id.createAccountResendOTP) {
            loginViewModel.callingResendOtpAPI()
        }
    }

    private fun getFCMTokenIdFromFireBase() {
        FirebaseInstanceId.getInstance()
            .instanceId.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("Notification", "getInstanceId failed", task.exception)
                return@OnCompleteListener
            }
            val token = task.result?.token
            appPreferences.fcmId = token
            Log.d("Notification", token!!)
            loginUserFromFireBaseDB()
        })
    }

    private fun loginUserFromFireBaseDB() {
        showProgressDialog("Please wait...")
        auth.signInWithEmailAndPassword(createAccountEmailET.text.toString(),
            createAccountPasswordET.text.toString()).addOnCompleteListener(this) { task ->
            hideProgressDialog()
            if (task.isSuccessful) {
                val fireBaseUser = auth.currentUser
                val dbReference = FirebaseDatabase.getInstance().reference.child("ChatUsers")
                    .child(fireBaseUser!!.uid)
                //                    dbReference.child("ChatUsers").child(fireBaseUser.uid).setValue(userChatMO)

                dbReference.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        Log.w("FirebaseDetails", "loginUserWithEmail:onCancelled")
                    }

                    override fun onDataChange(dataSnapShot: DataSnapshot) {
                        if (dataSnapShot.exists()) {
                            val chatMO = dataSnapShot.getValue(ChatUserMO::class.java)
                            chatMO!!.fcmTokenId = appPreferences.fcmId
                            appPreferences.setFireBaseUserName(chatMO.userName)
                            dbReference.setValue(chatMO)

                            appPreferences.fireBaseUniqueUserId = fireBaseUser.uid
                            if (chatMO.uniqueNotificationId != null) appPreferences.uniqueNotificationId =
                                chatMO.uniqueNotificationId!!
                            moveToIAmLookingActivity(chatMO)
                        }
                    }
                })

            } else {
                Log.w("FirebaseDetails", "createUserWithEmail:failure", task.exception)
                showSnakeBarMessages(createAccountEmailET, task.exception!!.message)
            }
        }
    }

    private fun showOTPLayout(mobileNoWithCountryCode: String) {
        isGoForwardClickedByUser = true
        createResendSpanString()
        //        headerStepperCounterTV.text = ileUtility.fromHtml("Step <b><font color='#4a4a4a'>2</font></b>/2")

        createAccountNameMobileRootLayout.visibility = View.GONE
        createAccountOTPLayout.visibility = View.VISIBLE
        headerStepperMessageTV.text =
            ileUtility.fromHtml("Verify your phone number<br>Code sent to <font color='#01c9d5'>"
                    + mobileNoWithCountryCode + "</font>")
    }

    private fun hideOTPLayout() {
        isGoForwardClickedByUser = false
        createAccountNameMobileRootLayout.visibility = View.VISIBLE
        createAccountOTPLayout.visibility = View.GONE
        createAccountOtpET.setText("")
        headerStepperMessageTV.text = resources.getText(R.string.enter_mob_no)
    }

    private fun createResendSpanString() {
        val resendOTPSpanString = SpannableString(resources.getString(R.string.resendOTPLink))
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }
        resendOTPSpanString.setSpan(clickableSpan, 30, 36, 0)
        resendOTPSpanString.setSpan(StyleSpan(Typeface.BOLD), 30, 36, 0)
        resendOTPSpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorBlueLinks)), 30, 36, 0)
        createAccountResendOTP.text = resendOTPSpanString
        createAccountResendOTP.movementMethod = LinkMovementMethod.getInstance()
        createAccountResendOTP.linksClickable = true
    }

    private fun createTOUAndPrivacyPolicySpan() {
        val tncPrivacyPolicySpanString =
            SpannableString(resources.getString(R.string.createAccountTnCPrivacyPolicy))
        val tncClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@LoginActivity, ILEWebPages::class.java).putExtra(
                    AppConstants.WEB_VIEW_KEY,
                    AppConstants.WEB_VIEW_TOU))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }
        val privacyPolicyClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@LoginActivity, ILEWebPages::class.java).putExtra(
                    AppConstants.WEB_VIEW_KEY,
                    AppConstants.WEB_VIEW_PRIVACY_POLICY))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }

        tncPrivacyPolicySpanString.setSpan(tncClickableSpan, 32, 44, 0)
        tncPrivacyPolicySpanString.setSpan(privacyPolicyClickableSpan, 49, 63, 0)
        tncPrivacyPolicySpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorLabels)), 32, 44, 0)
        tncPrivacyPolicySpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorLabels)), 49, 63, 0)
        createAccountTnCPrivacyPolicyTV.text = tncPrivacyPolicySpanString
        createAccountTnCPrivacyPolicyTV.movementMethod = LinkMovementMethod.getInstance()
        createAccountTnCPrivacyPolicyTV.linksClickable = true
    }

    override fun onBackPressed() {
        if (isGoForwardClickedByUser) hideOTPLayout()
        else super.onBackPressed()
    }

    private fun moveToIAmLookingActivity(chatMO: ChatUserMO) {
        try {
            val fullName = chatMO.userName!!.split(" ")
            val firstName: String
            val lastName: String

            if (fullName.size > 1) {
                firstName = fullName[0]
                lastName = fullName[1]
            } else {
                firstName = fullName[0]
                lastName = ""
            }

            val userInfoMO =
                UserInfoMO(firstName, lastName, chatMO.userEmailId!!, chatMO.userMobileNo!!)

            appPreferences.saveUserInformation(userInfoMO)
            appPreferences.saveLoginStatus(true)

            startActivity(Intent(this@LoginActivity,
                IAmLookingActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun moveToIAmLookingActivity() {
        try {
            loginResponse.data!!.userInfo!!.apply {
                val fullName = fullName!!.split(" ")
                val firstName: String
                val lastName: String

                if (fullName.size > 1) {
                    firstName = fullName[0]
                    lastName = fullName[1]
                } else {
                    firstName = fullName[0]
                    lastName = ""
                }
                val userInfoMO = UserInfoMO(firstName, lastName, email!!, mobile!!)
                appPreferences.saveUserInformation(userInfoMO)
                appPreferences.saveLoginStatus(true)
                startActivity(Intent(this@LoginActivity, IAmLookingActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}