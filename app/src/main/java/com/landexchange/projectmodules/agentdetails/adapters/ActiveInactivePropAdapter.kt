package com.landexchange.projectmodules.agentdetails.adapters

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.projectmodules.seeall.SeeAllPropertyActivity
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_feature_project.view.*

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class ActiveInactivePropAdapter(val context: Context, val ileUtility: ILEUtility,
    private val propertyList: ArrayList<FeaturedProjectMO>) :
    RecyclerView.Adapter<ActiveInactivePropAdapter.ActiveInactiveViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveInactiveViewHolder {
        val view = layoutInflater.inflate(R.layout.row_feature_project, parent, false)
        return ActiveInactiveViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 6
    }

    override fun onBindViewHolder(holder: ActiveInactiveViewHolder, position: Int) {
        holder.bindFeaturedProjectItems()
    }

    inner class ActiveInactiveViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeaturedProjectItems() {
            try {
                itemView.featureProjectRowParentLayout.layoutParams.width = deviceWidth
                itemView.featureProjectRowParentLayout.layoutParams.height = deviceHeight

                /*itemView.projectDeveloperOrAgentTV.text = ileUtility.fromHtml(
                    propertyList[layoutPosition].projectName + "<br>" +
                            "<small><font color='#9e9e9e'>" + propertyList[layoutPosition].projectShortDesc + "</font></small>"
                )
                itemView.projectAptDetailsCumAddressTV.text = ileUtility.fromHtml(
                    propertyList[layoutPosition].propertyType + " " +
                            propertyList[layoutPosition].propertyName +
                            "<br>" + "<small><font color='#9e9e9e'>Address</font></small>"
                )

                itemView.projectArea.text = propertyList[layoutPosition].propertyArea
                itemView.projectBedsCountTV.text = propertyList[layoutPosition].propertyBeds
                itemView.projectBathCountTV.text = propertyList[layoutPosition].propertyBaths
                itemView.projectGarageCountTV.text = propertyList[layoutPosition].propertyGarage
                itemView.projectFloorCountTV.text = propertyList[layoutPosition].propertyFloor

                Picasso.get()
                    .load(propertyList[layoutPosition].projectImageURL)
                    .tag(context)
                    .into(itemView.projectImage)*/

                itemView.setOnClickListener {
                    context.startActivity(Intent(context, SeeAllPropertyActivity::class.java))
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = getDeviceWidth - (getDeviceWidth / 4)
            deviceHeight = getDeviceWidth - (getDeviceWidth / 6)
        }
    }

}