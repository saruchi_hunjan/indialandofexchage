package com.landexchange.projectmodules.agentdetails

import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class AgentDetailsModelView constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<AgentContractor.AgentDetailsView>() {

}