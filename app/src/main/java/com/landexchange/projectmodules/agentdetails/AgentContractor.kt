package com.landexchange.projectmodules.agentdetails

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class AgentContractor {
    interface AgentDetailsView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}