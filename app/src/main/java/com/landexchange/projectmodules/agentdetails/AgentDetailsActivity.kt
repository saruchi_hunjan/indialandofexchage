package com.landexchange.projectmodules.agentdetails

import android.content.Intent
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.agentdetails.AgentContractor.AgentDetailsView
import com.landexchange.projectmodules.agentdetails.adapters.PropertyTabPagerAdapter
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.iledialog.DialogUserRating
import com.squareup.picasso.Picasso
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_agent_details.*
import kotlinx.android.synthetic.main.include_chat_contact_share_layout.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class AgentDetailsActivity : BaseActivity(), AgentDetailsView {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    private lateinit var activeInactivePropTabLayout: TabLayout
    private lateinit var activeInactivePropViewPager: ViewPager

    override fun getLayoutId(): Int {
        return R.layout.activity_agent_details
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun onSuccess(response: Any) {
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }

    override fun setUpUi() {
        gradientStatusBar()
        try {
            val agentDetailsMO = intent.getSerializableExtra(AppConstants.AGENT_DETAILS_KEY) as AgentsDetailMO
            agentDetailsMO.let {
                Picasso.get().load(it.agentImageURL).into(agentDetailsProfilePic)
                headerAgentNameTV.text = resources.getString(R.string.dynamicAgentName, it.firstName, it.lastName)
                agentDetailsName.text = resources.getString(R.string.dynamicAgentName, it.firstName, it.lastName)
                agentRatingValue.text = resources.getString(R.string.dynamicRatingWithSlash, 4, 5)

                agentsActiveProperty.text =
                    ileUtility.fromHtml("<b><font color='#3b3b3b'>12</font></b> Active<br>&nbsp;&nbsp;&nbsp;&nbsp;Property")
                agentsClosedProperty.text =
                    ileUtility.fromHtml("<b><font color='#3b3b3b'>18</font></b> Closed<br>&nbsp;&nbsp;&nbsp;&nbsp;Property")
            }

            activeInactivePropTabLayout = this.findViewById(R.id.activeInactivePropTabLayout)
            activeInactivePropViewPager = this.findViewById(R.id.activeInactivePropViewPager)
            headerAgentNameTV.setOnClickListener(onClickListener)
            rateOrScheduleButton.setOnClickListener(onClickListener)
            shareButton.setOnClickListener(onClickListener)

            agentRatingBar.setOnTouchListener { v, event ->
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        showAgentRatingDialogBox()
                    }
                }
                v?.onTouchEvent(event) ?: true
            }

            setDynamicValuesOnLabelsTV()
            setUpTabsWithViewPager()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setDynamicValuesOnLabelsTV() {
        chatWithButton.text = resources.getString(R.string.dynamicChatWith, "Akash")
        callOrContactButton.text = resources.getString(R.string.dynamicCallAgent, "Akash")
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.headerAgentNameTV -> this.onBackPressed()
            R.id.rateOrScheduleButton -> showAgentRatingDialogBox()
            R.id.shareButton -> shareProfileWithIntents()
        }
    }

    private fun setUpTabsWithViewPager() {

        activeInactivePropViewPager.layoutParams.height = ileUtility.getDeviceHeightForActivePropCardView()

        activeInactivePropTabLayout.addTab(activeInactivePropTabLayout.newTab().setText(resources.getString(R.string.activeProperty)))
        activeInactivePropTabLayout.addTab(activeInactivePropTabLayout.newTab().setText(resources.getString(R.string.inactiveProperty)))
        activeInactivePropTabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.colorGreyLabels),
            ContextCompat.getColor(this, R.color.colorBlueLinks))

        activeInactivePropViewPager.offscreenPageLimit = 2
        activeInactivePropViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(
            activeInactivePropTabLayout))

        activeInactivePropTabLayout.addOnTabSelectedListener(onTabSelectedListener)

        activeInactivePropViewPager.adapter = PropertyTabPagerAdapter(supportFragmentManager)
    }

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            activeInactivePropViewPager.currentItem = tab!!.position
        }
    }

    private fun showAgentRatingDialogBox() {
        val logoutDialog = DialogUserRating()
        logoutDialog.show(supportFragmentManager.beginTransaction(), "UserRatingDialog")
        logoutDialog.isCancelable = false
    }

    private fun shareProfileWithIntents() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, "I suggest this app for you : ILE")
        intent.type = "text/plain"
        startActivity(intent)
    }

    override fun onBackPressed() {
        supportFinishAfterTransition()
    }
}