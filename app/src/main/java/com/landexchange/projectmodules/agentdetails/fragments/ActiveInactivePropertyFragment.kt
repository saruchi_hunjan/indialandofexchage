package com.landexchange.projectmodules.agentdetails.fragments

import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.agentdetails.adapters.ActiveInactivePropAdapter
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.fragment_active_inactive_property.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class ActiveInactivePropertyFragment : BaseFragment() {

    @Inject
    lateinit var ileUtility: ILEUtility

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_active_inactive_property
    }

    override fun setUpUi() {
        try {
            activeInactivePropRecyclerView.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            activeInactivePropRecyclerView.adapter = ActiveInactivePropAdapter(activity!!, ileUtility, ArrayList())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}