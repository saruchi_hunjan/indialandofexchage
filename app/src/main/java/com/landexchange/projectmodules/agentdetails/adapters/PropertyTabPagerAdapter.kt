package com.landexchange.projectmodules.agentdetails.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.landexchange.projectmodules.agentdetails.fragments.ActiveInactivePropertyFragment

/**
 * Created by Ashu Rajput on 6/7/2019.
 */
class PropertyTabPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = ActiveInactivePropertyFragment()
            1 -> fragment = ActiveInactivePropertyFragment()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 2
    }

}