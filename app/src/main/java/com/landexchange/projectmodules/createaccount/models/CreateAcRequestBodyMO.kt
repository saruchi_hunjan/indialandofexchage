package com.landexchange.projectmodules.createaccount.models

import com.google.gson.annotations.SerializedName


/**
 * Created by Ashu Rajput on 12/10/2019.
 */
data class CreateAcRequestBodyMO(

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("email")
    var email: String? = null,

    @field:SerializedName("password")
    var password: String? = null,

    @field:SerializedName("phonenumber")
    var phonenumber: String? = null,

    @field:SerializedName("user_type")
    var user_type: String? = null
)