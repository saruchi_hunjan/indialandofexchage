package com.landexchange.projectmodules.createaccount

import android.content.Intent
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.chat.ChatUserMO
import com.landexchange.projectmodules.createaccount.CreateAccountContractor.CreateAccountView
import com.landexchange.projectmodules.createaccount.models.CreateAcRequestBodyMO
import com.landexchange.projectmodules.iamlookingto.IAmLookingActivity
import com.landexchange.projectmodules.login.LoginActivity
import com.landexchange.projectmodules.login.models.LoginAPIResponse
import com.landexchange.projectmodules.login.models.UserInfoMO
import com.landexchange.projectmodules.webpagelinks.ILEWebPages
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.toolbar_header_dark.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 4/24/2019.
 */

class CreateAccountActivity : BaseActivity(), CreateAccountView {

    @Inject
    lateinit var ileUtility: ILEUtility

    @Inject
    lateinit var createAccountViewModelFactory: CreateAccountViewModelFactory
    private lateinit var createAccountViewModel: CreateAccountViewModel

    private var isGoForwardClickedByUser = false
    private lateinit var selectedCountryCode: String

    private lateinit var auth: FirebaseAuth

    private lateinit var receivedOTP: String

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        createAccountViewModel = ViewModelProvider(this,
            createAccountViewModelFactory).get(CreateAccountViewModel::class.java)
        createAccountViewModel.setView(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_create_account
    }

    private val clickListener = View.OnClickListener { view ->
        if (view.id == R.id.headerBackArrowButton) {
            onBackPressed()
        } else if (view.id == R.id.createAccountGoForward) {
            if (createAccountNameET.text.toString().isEmpty() && createAccountNameET.text.toString().trim().isEmpty())
                showSnakeBarMessages(createAccountNameET,
                    resources.getString(R.string.validationMsgValidName))
            else if (createAccountEmailET.text.toString().isEmpty() && createAccountEmailET.text.toString().trim().isEmpty())
                showSnakeBarMessages(createAccountEmailET,
                    resources.getString(R.string.validationMsgValidEmail))
            else if (createAccountPasswordET.text.toString().isEmpty() && createAccountPasswordET.text.toString().trim().isEmpty())
                showSnakeBarMessages(createAccountEmailET,
                    resources.getString(R.string.validationMsgValidPW))
            else if (createAccountMobileNoET.text.toString().isEmpty() && createAccountMobileNoET.text.toString().length < 10)
                showSnakeBarMessages(createAccountMobileNoET,
                    resources.getString(R.string.validationMsgValidMobNo))
            else {
                callCreateAccountAPIWithValues()
            }
        } else if (view.id == R.id.createAccountVerifyOTP) {
            if (createAccountOtpET.text.toString().isEmpty() && createAccountOtpET.text.toString().length < 4)
                showSnakeBarMessages(createAccountVerifyOTP,
                    resources.getString(R.string.validationMsgValidDigitOTP))
            else if (createAccountOtpET.text.toString() != receivedOTP) {
                showSnakeBarMessages(createAccountOtpET,
                    resources.getString(R.string.validationMsgValidOTP))
            } else {
                hideKeyboard(createAccountVerifyOTP)
                if (appPreferences.fcmId.isNotBlank() || appPreferences.fcmId.isNotEmpty())
                    registerUserToFireBaseDB()
                else
                    getFCMTokenIdFromFireBase()
            }
        } else if (view.id == R.id.createAccountResendOTP) {
            createAccountViewModel.callingResendOtpAPI()
        }
    }

    private fun initCountryCodePicker() {
        selectedCountryCode = countryCodePicker.defaultCountryCodeWithPlus
        countryCodePicker.setOnCountryChangeListener {
            selectedCountryCode = "+" + it.phoneCode
        }
    }

    private fun createTOUAndPrivacyPolicySpan() {

        val tncPrivacyPolicySpanString =
            SpannableString(resources.getString(R.string.createAccountTnCPrivacyPolicy))
        val tncClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@CreateAccountActivity, ILEWebPages::class.java)
                    .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_TOU))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }
        val privacyPolicyClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@CreateAccountActivity, ILEWebPages::class.java)
                    .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_PRIVACY_POLICY))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }

        tncPrivacyPolicySpanString.setSpan(tncClickableSpan, 32, 44, 0)
        tncPrivacyPolicySpanString.setSpan(privacyPolicyClickableSpan, 49, 63, 0)
        tncPrivacyPolicySpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorLabels)), 32, 44, 0)
        tncPrivacyPolicySpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorLabels)), 49, 63, 0)
        createAccountTnCPrivacyPolicyTV.text = tncPrivacyPolicySpanString
        createAccountTnCPrivacyPolicyTV.movementMethod = LinkMovementMethod.getInstance()
        createAccountTnCPrivacyPolicyTV.linksClickable = true
    }

    private fun createResendSpanString() {
        val resendOTPSpanString = SpannableString(resources.getString(R.string.resendOTPLink))
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }

        resendOTPSpanString.setSpan(clickableSpan, 30, 36, 0)
        resendOTPSpanString.setSpan(StyleSpan(Typeface.BOLD), 30, 36, 0)
        resendOTPSpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorBlueLinks)), 30, 36, 0)
        createAccountResendOTP.text = resendOTPSpanString
        createAccountResendOTP.movementMethod = LinkMovementMethod.getInstance()
        createAccountResendOTP.linksClickable = true
    }

    private fun createSpanLoginString() {

        val loginSpanString = SpannableString(resources.getString(R.string.alreadyHaveAccount))
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@CreateAccountActivity, LoginActivity::class.java))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }

        loginSpanString.setSpan(clickableSpan, 25, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginSpanString.setSpan(StyleSpan(Typeface.BOLD), 25, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginSpanString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,
            R.color.colorBlueLinks)), 25, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginButton.text = loginSpanString
        loginButton.movementMethod = LinkMovementMethod.getInstance()
        loginButton.linksClickable = true
    }

    private fun showOTPLayout() {
        hideKeyboard(createAccountGoForward)
        isGoForwardClickedByUser = true
        createResendSpanString()
        headerStepperCounterTV.text =
            ileUtility.fromHtml("Step <b><font color='#4a4a4a'>2</font></b>/2")

        createAccountNameMobileRootLayout.visibility = View.GONE
        createAccountOTPLayout.visibility = View.VISIBLE
        headerStepperMessageTV.text =
            ileUtility.fromHtml("Verify your phone number<br>Code sent to <font color='#01c9d5'>"
                    + selectedCountryCode + createAccountMobileNoET.text.toString() + "</font>")
    }

    private fun hideOTPLayout() {
        isGoForwardClickedByUser = false
        createAccountNameMobileRootLayout.visibility = View.VISIBLE
        createAccountOTPLayout.visibility = View.GONE
        createAccountOtpET.setText("")
        headerStepperMessageTV.text = resources.getString(R.string.createAccountHeaderStepperMsg)
    }

    override fun onBackPressed() {
        if (isGoForwardClickedByUser)
            hideOTPLayout()
        else
            super.onBackPressed()
    }

    override fun setUpUi() {
        headerSkipButton.visibility = View.GONE
        headerStepperMessageTV.text = resources.getString(R.string.createAccountHeaderStepperMsg)
        createTOUAndPrivacyPolicySpan()
        createSpanLoginString()

        createAccountGoForward.setOnClickListener(clickListener)
        headerBackArrowButton.setOnClickListener(clickListener)
        createAccountVerifyOTP.setOnClickListener(clickListener)
        createAccountResendOTP.setOnClickListener(clickListener)
        initCountryCodePicker()

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
    }

    override fun onSuccess(response: Any) {
        val apiResponse = response as LoginAPIResponse

        apiResponse.apply {
            when (apiResponse.statusMessage) {
                AppConstants.SUCCESS_STATUS_MESSAGE -> {
                    appPreferences.apiTokenId = data!!.apiToken
                    //                    receivedOTP = data!!.userInfo!!.mobileOTP!!

                    if (data!!.userInfo!!.isPhoneVerified.isNullOrBlank())
                        showOTPLayout()
                    else {
                        if (appPreferences.fcmId.isNotBlank() || appPreferences.fcmId.isNotEmpty())
                            registerUserToFireBaseDB()
                        else
                            getFCMTokenIdFromFireBase()
                    }
                }
                AppConstants.FAILURE_STATUS_MESSAGE -> showSnakeBarMessages(createAccountMobileNoET,
                    apiResponse.data!!.apiMessage)
                else -> showSnakeBarMessages(createAccountMobileNoET,
                    resources.getString(R.string.internal_server_error))
            }
        }
    }

    override fun onFailure(errorResponse: Any) {
        when {
            errorResponse is String -> showSnakeBarMessages(createAccountOtpET, errorResponse)
            errorResponse as Int == 401 -> showSnakeBarMessages(createAccountOtpET,
                resources.getString(R.string.error_api_email))
            else -> showSnakeBarMessages(createAccountOtpET,
                resources.getString(R.string.internal_server_error))
        }
    }

    override fun onSuccessResendOTP(message: String) {
        showSnakeBarMessages(createAccountVerifyOTP, message)
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    private fun registerUserToFireBaseDB() {
        showProgressDialog("Please wait...")
        auth.createUserWithEmailAndPassword(createAccountEmailET.text.toString(),
            createAccountPasswordET.text.toString())
            .addOnCompleteListener(this) { task ->
                hideProgressDialog()
                if (task.isSuccessful) {
                    val fireBaseUser = auth.currentUser
                    val dbReference = FirebaseDatabase.getInstance().reference

                    if (appPreferences.uniqueNotificationId == 0)
                        appPreferences.uniqueNotificationId = String.hashCode()

                    val userChatMO = ChatUserMO(createAccountNameET.text.toString(),
                        fireBaseUser!!.uid, createAccountEmailET.text.toString(),
                        "I am available for chat.", appPreferences.fcmId,
                        createAccountMobileNoET.text.toString(),
                        appPreferences.uniqueNotificationId)

                    dbReference.child("ChatUsers").child(fireBaseUser.uid).setValue(userChatMO)

                    appPreferences.fireBaseUniqueUserId = fireBaseUser.uid
                    appPreferences.setFireBaseUserName(createAccountNameET.text.toString())

                    moveToIAmLookingActivity()

                } else {
                    Log.w("FirebaseDetails", "createUserWithEmail:failure", task.exception)
                    showSnakeBarMessages(createAccountEmailET, task.exception!!.message)
                }
            }
    }

    private fun callCreateAccountAPIWithValues() {
        val signUpBody = CreateAcRequestBodyMO(createAccountNameET.text.toString(),
            createAccountEmailET.text.toString(), createAccountPasswordET.text.toString(),
            createAccountMobileNoET.text.toString(), "user")
        createAccountViewModel.callCreateAccountAPI(signUpBody)
    }

    private fun getFCMTokenIdFromFireBase() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Notification", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                val token = task.result?.token
                appPreferences.fcmId = token
                //                Log.d("Notification", token)
                registerUserToFireBaseDB()
            })
    }

    private fun moveToIAmLookingActivity() {
        try {
            val userName = createAccountNameET.text.toString().trim()
            val firstName: String
            var lastName = ""
            if (userName.contains(" ")) {
                userName.replace("\\s+".toRegex(), " ")
                firstName = userName.split(" ")[0]
                lastName = userName.split(" ")[1]
            } else
                firstName = userName

            val userInfoMO = UserInfoMO(firstName, lastName, createAccountEmailET.text.toString(),
                createAccountMobileNoET.text.toString())

            appPreferences.saveUserInformation(userInfoMO)
            appPreferences.saveSignUpStatus(true)

            startActivity(Intent(this@CreateAccountActivity, IAmLookingActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}