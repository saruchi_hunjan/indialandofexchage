package com.landexchange.projectmodules.createaccount.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/30/2019.
 */
class CreateAccountDataMO(
    @field:SerializedName("user")
    var user: CreateAcRequestBodyMO? = null,

    @field:SerializedName("token")
    var token: String? = null,

    @field:SerializedName("message")
    var message: String? = null)