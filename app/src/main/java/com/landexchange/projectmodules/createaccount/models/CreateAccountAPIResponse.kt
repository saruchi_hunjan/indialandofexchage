package com.landexchange.projectmodules.createaccount.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/30/2019.
 */
data class CreateAccountAPIResponse(
    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: CreateAccountDataMO? = null
)