package com.landexchange.projectmodules.createaccount

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 4/24/2019.
 */
class CreateAccountContractor {
    interface CreateAccountView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(errorResponse: Any)
        fun onSuccessResendOTP(message: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}