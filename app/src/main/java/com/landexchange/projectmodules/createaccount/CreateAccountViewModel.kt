package com.landexchange.projectmodules.createaccount

import android.util.Log
import com.google.gson.Gson
import com.landexchange.IleApplication
import com.landexchange.base.BaseViewModel
import com.landexchange.networking.APIErrorResponseMO
import com.landexchange.projectmodules.createaccount.models.CreateAcRequestBodyMO
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 4/24/2019.
 */
class CreateAccountViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<CreateAccountContractor.CreateAccountView>() {

    var gson: Gson = IleApplication.getAppComponent().gson
    @Inject
    lateinit var retrofit: Retrofit

    fun callCreateAccountAPI(userDetailsBody: CreateAcRequestBodyMO) {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.createAccountAPICall(userDetailsBody) }
                .observeOn(AndroidSchedulers.mainThread())
                //Here we are using Consumer as 1st argument in subscribe(Consumer,Throwable [as 2nd argument])
                .subscribe({
                    getView()!!.setLoaderVisibility(false)
                    if (it.code() == 200) {
                        getView()!!.onSuccess(it)
                    } else {
                        val body = gson.fromJson(it.errorBody()!!.string(),
                            APIErrorResponseMO::class.java)
                        body.apply {
                            getView()!!.onFailure(data!!.errorMessage.toString())
                        }
                    }
                }, {
                    getView()!!.setLoaderVisibility(false)
                    if (it is HttpException) getView()!!.onFailure(it.code())
                    else getView()!!.onFailure(500)
                })
        )
    }

    fun callingResendOtpAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.resendOtpAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccessResendOTP(commonResponse.apiMessage!!)
                }, {
                    getView()!!.setLoaderVisibility(false)
                    if (it is HttpException)
                        getView()!!.onFailure(it.code())
                    else
                        getView()!!.onFailure(500)
                }))
    }
}