package com.landexchange.projectmodules.iamlookingto.models

/**
 * Created by Ashu Rajput on 5/30/2019.
 */
data class CityLocalitiesMO(var cityName: String, var cityCode: Int, var localityName: String, var localityCode: Int)