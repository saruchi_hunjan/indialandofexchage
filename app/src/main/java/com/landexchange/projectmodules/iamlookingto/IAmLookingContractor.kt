package com.landexchange.projectmodules.iamlookingto

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class IAmLookingContractor {

    interface IAmLookingView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}