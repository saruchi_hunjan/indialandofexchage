package com.landexchange.projectmodules.iamlookingto.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.iamlookingto.IAmLookingActivity
import com.landexchange.projectmodules.iamlookingto.models.CitiesMO
import com.landexchange.projectmodules.popularlocalities.AreaLocalitiesActivity
import com.landexchange.util.AppConstants
import com.landexchange.util.iledialog.CityDialogListener
import com.landexchange.util.showLongToast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_cities.view.*

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class CitiesAdapter() : RecyclerView.Adapter<CitiesAdapter.CitiesViewHolder>() {

    private lateinit var context: Context
    private lateinit var cityList: List<CitiesMO>
    private var isComingFromHomeScreen = false
    private var isComingFromSuperSearch = false
    private lateinit var cityDialogListener: CityDialogListener

    private lateinit var layoutInflater: LayoutInflater

    constructor(context: Context, cityList: List<CitiesMO>) : this() {
        this.context = context
        this.cityList = cityList
        layoutInflater = LayoutInflater.from(context)
    }

    constructor(context: Context, cityList: List<CitiesMO>, isComingFromHomeScreen: Boolean,
        cityDialogListener: CityDialogListener) : this() {
        this.context = context
        this.cityList = cityList
        this.isComingFromHomeScreen = isComingFromHomeScreen
        this.cityDialogListener = cityDialogListener
        layoutInflater = LayoutInflater.from(context)
    }

    constructor(context: Context, cityList: List<CitiesMO>, cityDialogListener: CityDialogListener,
        isComingFromSuperSearch: Boolean) : this() {
        this.context = context
        this.cityList = cityList
        this.cityDialogListener = cityDialogListener
        this.isComingFromSuperSearch = isComingFromSuperSearch
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder {
        val view = layoutInflater.inflate(R.layout.row_cities, parent, false)
        return CitiesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int) {
        holder.bindCitiesData()
    }

    inner class CitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindCitiesData() {
            itemView.setOnClickListener {
                if (IAmLookingActivity.iAmOptionsSelected) {
                    updateSelectedValueInList(layoutPosition)
                    notifyDataSetChanged()
                } else {
                    context.showLongToast(context.resources.getString(R.string.iAmNotSelectedMsg))
                }
            }

            try {
                if (isComingFromSuperSearch) {
                    if (cityList[layoutPosition].isCitySelected)
                        itemView.lookingToCitiesTV.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                    else
                        itemView.lookingToCitiesTV.setTextColor(ContextCompat.getColor(context,
                            R.color.colorDarkGreyDivider))

                } else {
                    if (cityList[layoutPosition].isCitySelected) {
                        itemView.lookingToCitiesTV.background =
                            ContextCompat.getDrawable(context, R.drawable.blue_button_pressed)
                        itemView.lookingToCitiesTV.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                    } else {
                        itemView.lookingToCitiesTV.background = null
                        itemView.lookingToCitiesTV.setTextColor(ContextCompat.getColor(context, R.color.colorBlueLinks))
                    }
                }

                itemView.lookingToCitiesTV.text = cityList[layoutPosition].cityName
                Picasso.get()
                    .load(cityList[layoutPosition].cityURL)
                    .placeholder(R.drawable.logo_city_delhi)
                    .tag(context)
                    .into(itemView.lookingCityLogo)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun updateSelectedValueInList(selectedIndex: Int) {
            when {
                isComingFromHomeScreen -> cityDialogListener.onCitySelected(cityList[selectedIndex].cityName,
                    cityList[selectedIndex].citycode)
                isComingFromSuperSearch -> cityList.forEachIndexed { index, element ->
                    run { element.isCitySelected = index == selectedIndex }
                }
                else -> {
                    cityList.forEachIndexed { index, element ->
                        run { element.isCitySelected = index == selectedIndex }
                    }
                    context.startActivity(Intent(context, AreaLocalitiesActivity::class.java)
                        .putExtra(AppConstants.SELECTED_CITY_NAME_KEY, cityList[selectedIndex].cityName)
                        .putExtra(AppConstants.SELECTED_CITY_CODE_KEY, cityList[selectedIndex].citycode))

                    /*cityDialogListener.onCitySelected(cityList[selectedIndex].cityName,
                        cityList[selectedIndex].citycode)*/
                }
            }
        }
    }


}