package com.landexchange.projectmodules.iamlookingto

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.iamlookingto.IAmLookingContractor.IAmLookingView
import com.landexchange.projectmodules.iamlookingto.adapters.CitiesAdapter
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse
import com.landexchange.projectmodules.iamlookingto.models.CitiesMO
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_iam_looking.*
import kotlinx.android.synthetic.main.include_looking_into.*
import kotlinx.android.synthetic.main.toolbar_header_dark.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class IAmLookingActivity : BaseActivity(), IAmLookingView {

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var iAmLookingViewModelFactory: IAmLookingViewModelFactory

    private lateinit var iAmLookingViewModel: IAmLookingViewModel

    private var lookingToOptionsArray: List<TextView>? = null

    companion object {
        var iAmOptionsSelected = false
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_iam_looking
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        iAmLookingViewModel =
            ViewModelProvider(this, iAmLookingViewModelFactory).get(IAmLookingViewModel::class.java)
        iAmLookingViewModel.setView(this)
    }

    override fun setUpUi() {
        headerMainTitle.text = resources.getString(R.string.onboarding_signup_msg)
        headerStepperMessageTV.visibility = View.GONE
        headerSkipButton.visibility = View.GONE
        headerBackArrowButton.setOnClickListener(clickListener)
        lookingToBuyButton.setOnClickListener(clickListener)
        lookingToRentButton.setOnClickListener(clickListener)
        lookingToCommercial.setOnClickListener(clickListener)
//        lookingToMortgageButton.setOnClickListener(clickListener)
//        lookingToOtherServicesButton.setOnClickListener(clickListener)

        lookingToOptionsArray =
            listOf<TextView>(lookingToBuyButton, lookingToRentButton, lookingToCommercial)
//        lookingToMortgageButton, lookingToOtherServicesButton

        iAmLookingViewModel.callingGetCitiesAPI()
    }

    override fun onSuccess(response: Any) {
        val citiesAPIResponse = response as CitiesAPIResponse
        setUpCitiesRecyclerViews(citiesAPIResponse.data!!)
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    private val clickListener = View.OnClickListener { view ->
        when {
            view.id == R.id.headerBackArrowButton -> finish()
            view.id == R.id.lookingToBuyButton -> updateSelectorOnLookingOptionsWithRx(R.id.lookingToBuyButton)
            view.id == R.id.lookingToRentButton -> updateSelectorOnLookingOptionsWithRx(R.id.lookingToRentButton)
            view.id == R.id.lookingToCommercial -> updateSelectorOnLookingOptionsWithRx(R.id.lookingToCommercial)
        }
    }

    /*private fun updateSelectorOnLookingOptions(textViewId: Int) {
        for (textViewsFromList in lookingToOptionsArray!!) {
            if (textViewId == textViewsFromList.id) {
                textViewsFromList.background = ContextCompat.getDrawable(this, R.drawable.blue_button_pressed)
                textViewsFromList.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            } else {
                textViewsFromList.background = ContextCompat.getDrawable(this, R.drawable.blue_button_unpressed)
                textViewsFromList.setTextColor(ContextCompat.getColor(this, R.color.colorBlueLinks))
            }
        }
    }*/

    private fun updateSelectorOnLookingOptionsWithRx(textViewId: Int) {
        iAmOptionsSelected = true
        compositeDisposable.add(
            Observable.fromIterable(lookingToOptionsArray)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (textViewId == it.id) {
                        it.background =
                            ContextCompat.getDrawable(this, R.drawable.blue_button_pressed)
                        it.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
                    } else {
                        it.background =
                            ContextCompat.getDrawable(this, R.drawable.blue_button_unpressed)
                        it.setTextColor(ContextCompat.getColor(this, R.color.colorBlueLinks))
                    }
                }
        )
    }

    private fun setUpCitiesRecyclerViews(citiesArrayList: ArrayList<CitiesMO>) {
        citiesRecyclerView.layoutManager = GridLayoutManager(this, 3)
        citiesRecyclerView.adapter = CitiesAdapter(this, citiesArrayList)
    }

    /*private fun validateCitySelected(lookingType: String): Boolean {
        return if (isCitySelected) {
            when (lookingType) {
                "BUY" -> updateSelectorOnLookingOptionsWithRx(R.id.lookingToBuyButton)
                "RENT" -> {
                    updateSelectorOnLookingOptionsWithRx(R.id.lookingToRentButton)
                    startActivity(Intent(this@IAmLookingActivity, UploadListingActivity::class.java))
                }
                "COMMERCIAL" -> updateSelectorOnLookingOptionsWithRx(R.id.lookingToCommercial)
            }
            true
        } else {
            showSnakeBarMessages(citiesRecyclerView, resources.getString(R.string.cityNotSelectedMsg))
            false
        }
    }*/

    /*override fun onCitySelected(cityName: String, cityCode: Int) {
        if (iAmOptionsSelected) {
            startActivity(Intent(this, AreaLocalitiesActivity::class.java)
                .putExtra(AppConstants.SELECTED_CITY_NAME_KEY, cityName)
                .putExtra(AppConstants.SELECTED_CITY_CODE_KEY, cityCode))
        } else
            showSnakeBarMessages(citiesRecyclerView, resources.getString(R.string.iAmNotSelectedMsg))
    }*/
}