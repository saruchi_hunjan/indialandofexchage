package com.landexchange.projectmodules.iamlookingto

import com.landexchange.base.BaseViewModel
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
class IAmLookingViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<IAmLookingContractor.IAmLookingView>() {

    fun callingGetCitiesAPI() {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            io.reactivex.Observable.just(1)
                .subscribeOn(Schedulers.computation())
                .flatMap { remoteRepository.citiesAPICall() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer<CitiesAPIResponse> { commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, Consumer<Throwable> { throwable ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

}