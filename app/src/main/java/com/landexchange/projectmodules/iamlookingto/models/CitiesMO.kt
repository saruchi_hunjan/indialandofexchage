package com.landexchange.projectmodules.iamlookingto.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 5/3/2019.
 */
data class CitiesMO(
    @field:SerializedName("Citycode")
    var citycode: Int,

    @field:SerializedName("Image")
    var cityURL: String,

    @field:SerializedName("CityName")
    var cityName: String,

    var isCitySelected: Boolean
)