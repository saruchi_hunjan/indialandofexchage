package com.landexchange.projectmodules.iamlookingto.models

import com.google.gson.annotations.SerializedName


/**
 * Created by Ashu Rajput on 5/8/2019.
 */
data class CitiesAPIResponse(

    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: ArrayList<CitiesMO>? = null
)