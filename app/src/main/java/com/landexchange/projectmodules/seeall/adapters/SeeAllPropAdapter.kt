package com.landexchange.projectmodules.seeall.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.PropertyDetailsActivity
import com.landexchange.util.ILEUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_feature_project.view.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 6/24/2019.
 */
class SeeAllPropAdapter(val context: Context,
    private val propertyLists: ArrayList<FeaturedProjectMO>) :
    RecyclerView.Adapter<SeeAllPropAdapter.SeeAllPropViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    @Inject
    lateinit var ileUtility: ILEUtility

    private var deviceHeight = 0

    init {
        IleApplication.getRoomComponent().inject(this)
        deviceHeight = ileUtility.getDeviceHeightForPicturePreview()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeeAllPropViewHolder {
        val view = layoutInflater.inflate(R.layout.row_feature_project, parent, false)
        return SeeAllPropViewHolder(view)
    }

    override fun getItemCount(): Int {
        return propertyLists.size
    }

    override fun onBindViewHolder(holder: SeeAllPropViewHolder, position: Int) {
        holder.bindFeaturedProjectItems()
    }

    inner class SeeAllPropViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeaturedProjectItems() {
            try {
                itemView.featureProjectRowParentLayout.layoutParams.height = deviceHeight

                itemView.propertyRatingWithStar.visibility = View.GONE
                itemView.propertyGiveRating.visibility = View.VISIBLE
                if (layoutPosition % 2 == 0) { // Temp condition : remove once done{
                    itemView.propertyPriceLabelTV.text = context.resources.getString(R.string.propPriceOnRequest)
                    itemView.projectAmenitiesTV.visibility = View.VISIBLE
                    itemView.projectAmenitiesTV.text = context.resources.getString(R.string.featureProdAmenities)
                }

                itemView.projectDeveloperOrAgentTV.text = ileUtility.fromHtml(
                    propertyLists[layoutPosition].projectName + "<br>" +
                            "<small><font color='#9e9e9e'>" + propertyLists[layoutPosition].projectShortDesc + "</font></small>")
                itemView.projectAptDetailsCumAddressTV.text = ileUtility.fromHtml(
                    propertyLists[layoutPosition].propertyType + " " +
                            propertyLists[layoutPosition].propertyName +
                            "<br>" + "<small><font color='#9e9e9e'>Address</font></small>")

                itemView.projectArea.text = propertyLists[layoutPosition].propertyArea
                itemView.projectBedsCountTV.text = propertyLists[layoutPosition].propertyBeds
                itemView.projectBathCountTV.text = propertyLists[layoutPosition].propertyBaths
                itemView.projectGarageCountTV.text = propertyLists[layoutPosition].propertyGarage
                itemView.projectFloorCountTV.text = propertyLists[layoutPosition].propertyFloor

                Picasso.get().load(propertyLists[layoutPosition].projectImageURL)
                    .tag(context).into(itemView.projectImage)

                itemView.setOnClickListener {
                    context.startActivity(Intent(context, PropertyDetailsActivity::class.java))
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}