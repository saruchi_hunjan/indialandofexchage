package com.landexchange.projectmodules.seeall

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.bookagent.SearchAgentActivity
import com.landexchange.projectmodules.bookagent.adapter.BookAgentAdapter
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import com.landexchange.projectmodules.filters.FiltersMainActivity
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO
import com.landexchange.projectmodules.popularlocalities.AreaLocalitiesActivity
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_see_all_agents.*
import kotlinx.android.synthetic.main.bottom_sheet_sorting.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 6/27/2019.
 */
class SeeAllAgentsActivity : BaseActivity() {

    @Inject
    lateinit var ileUtility: ILEUtility

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var cityLocalityJson: CityLocalitiesMO
    private var agentListDetails = ArrayList<AgentsDetailMO>()

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    private val changeCityNameCode = 3
    private lateinit var bookAgentAdapter: BookAgentAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_see_all_agents
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        gradientStatusBar()
        updateHeaderCityText()
        bottomSheetBehavior = BottomSheetBehavior.from(sorting_bottom_sheet_layout)

        updateSeeAllAgentsRecyclerView()

        bookAgentHeaderBackButton.setOnClickListener(clickListener)
        sortingButton.setOnClickListener(clickListener)
        sortingAlphabetAtoZ.setOnClickListener(clickListener)
        sortingAlphabetZToA.setOnClickListener(clickListener)
        sortingRatingHighToLow.setOnClickListener(clickListener)
        sortingRatingLowToHigh.setOnClickListener(clickListener)
        bookAgentCityLocationTV.setOnClickListener(clickListener)
        bookAgentShowInList.setOnClickListener(clickListener)
        filtersButton.setOnClickListener(clickListener)
        searchAgentsButton.setOnClickListener(clickListener)
    }

    private fun updateHeaderCityText() {
        cityLocalityJson = appPreferences.cityLocalitiesJson
        if (cityLocalityJson.cityName.length > 15)
            bookAgentCityLocationTV.layoutParams.width = 190
        bookAgentCityLocationTV.text = cityLocalityJson.cityName
    }

    private fun updateSeeAllAgentsRecyclerView() {
        try {
            val receivedBundle = intent.extras
            if (receivedBundle != null) {
                if (receivedBundle.containsKey(AppConstants.SEE_ALL_AGENT_KEY))
                    agentListDetails =
                        receivedBundle.getSerializable(AppConstants.SEE_ALL_AGENT_KEY) as ArrayList<AgentsDetailMO>

                if (agentListDetails.size > 0) {
                    seeAllAgentsListRecyclerView.layoutManager = LinearLayoutManager(this)
                    bookAgentAdapter = BookAgentAdapter(this, ileUtility)
                    seeAllAgentsListRecyclerView.adapter = bookAgentAdapter

                    if (receivedBundle.containsKey(AppConstants.SORTING_TYPE_KEY)) {
                        when (receivedBundle.getString(AppConstants.SORTING_TYPE_KEY)) {
                            AppConstants.SORTING_ATOZ -> sortAtoZAlphabet()
                            AppConstants.SORTING_ZTOA -> sortZtoAAlphabet()
                        }

                    } else
                        updateAgentsRecyclerView(agentListDetails)
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun updateAgentsRecyclerView(sortedAgentList: ArrayList<AgentsDetailMO>) {
        if (sortedAgentList.size > 0) {
            compositeDisposable.add(Observable.just(sortedAgentList).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { bookAgentAdapter.updateBookAgentsRecyclerView(it) })
        }
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.sortingButton -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                else
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingAlphabetAtoZ -> {
                sortAtoZAlphabet()
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingAlphabetZToA -> {
                sortZtoAAlphabet()
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingRatingHighToLow -> {
                sortRatingHighToLow()
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingRatingLowToHigh -> {
                sortRatingLowToHigh()
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.bookAgentCityLocationTV -> {
                startActivityForResult(Intent(this, AreaLocalitiesActivity::class.java)
                    .putExtra(AppConstants.SELECTED_CITY_NAME_KEY, cityLocalityJson.cityName)
                    .putExtra(AppConstants.SELECTED_CITY_CODE_KEY, cityLocalityJson.cityCode)
                    .putExtra(AppConstants.COMING_FROM_AGENT_KEY, true), changeCityNameCode)
            }
            R.id.bookAgentShowInList -> {
            }

            R.id.filtersButton -> {
                startActivity(Intent(this, FiltersMainActivity::class.java)
                    .putExtra(AppConstants.FILTER_TYPE_KEY, AppConstants.AGENT_FILTER))
            }
            R.id.searchAgentsButton -> startActivity(Intent(this, SearchAgentActivity::class.java))

            R.id.bookAgentHeaderBackButton -> finish()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == changeCityNameCode && resultCode == Activity.RESULT_OK) {
            updateHeaderCityText()
        }
    }

    //SORTING A TO Z[ascending]
    private fun sortAtoZAlphabet() {
        val sortedAToZAgents = agentListDetails.sortedBy { agentsMO ->
            agentsMO.firstName + " " + agentsMO.lastName
        }
        updateAgentsRecyclerView(ArrayList(sortedAToZAgents))
    }

    //SORTING Z TO A[descending]
    private fun sortZtoAAlphabet() {
        val sortedZToAAgents = agentListDetails.sortedByDescending { agentsMO ->
            agentsMO.firstName + " " + agentsMO.lastName
        }
        updateAgentsRecyclerView(ArrayList(sortedZToAAgents))
    }

    //SORTING RATING HIGH TO LOW
    private fun sortRatingHighToLow() {
        try {
            val sortedRatingHighToLow = agentListDetails.sortedByDescending { agentsMO ->
                agentsMO.rating
            }
            updateAgentsRecyclerView(ArrayList(sortedRatingHighToLow))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //SORTING RATING LOW TO HIGH
    private fun sortRatingLowToHigh() {
        try {
            val sortedRatingLowToHigh = agentListDetails.sortedByDescending { agentsMO ->
                agentsMO.rating!!
            }
            updateAgentsRecyclerView(ArrayList(sortedRatingLowToHigh))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}