package com.landexchange.projectmodules.seeall

import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.projectmodules.filters.FiltersMainActivity
import com.landexchange.projectmodules.seeall.adapters.SeeAllPropAdapter
import com.landexchange.util.AppConstants
import kotlinx.android.synthetic.main.activity_see_all_property.*
import kotlinx.android.synthetic.main.bottom_sheet_sorting.*

/**
 * Created by Ashu Rajput on 6/24/2019.
 */
class SeeAllPropertyActivity : BaseActivity() {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun getLayoutId(): Int {
        return R.layout.activity_see_all_property
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        gradientStatusBar()

        bottomSheetBehavior = BottomSheetBehavior.from(sorting_bottom_sheet_layout)

        propertySortingButton.setOnClickListener(clickListener)
        sortingAlphabetAtoZ.setOnClickListener(clickListener)
        sortingAlphabetZToA.setOnClickListener(clickListener)
        sortingRatingHighToLow.setOnClickListener(clickListener)
        sortingRatingLowToHigh.setOnClickListener(clickListener)
        propertyFilterButton.setOnClickListener(clickListener)

        val propertyLists =
            intent.getSerializableExtra(AppConstants.SEE_ALL_PROJECTS_KEY) as ArrayList<FeaturedProjectMO>
        if (propertyLists.size > 0) {
            seeAllPropertyTitle.text = resources.getString(R.string.activePropertyCount, propertyLists.size)
            seeAllPropertiesRecyclerView.layoutManager = LinearLayoutManager(this)
            seeAllPropertiesRecyclerView.adapter = SeeAllPropAdapter(this,propertyLists)
        }
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.propertySortingButton -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                else
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.sortingAlphabetAtoZ -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.sortingAlphabetZToA -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.sortingRatingHighToLow -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.sortingRatingLowToHigh -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.propertyFilterButton -> {
                startActivity(Intent(this, FiltersMainActivity::class.java)
//                    .putExtra(AppConstants.FILTER_TYPE_KEY, AppConstants.SEE_ALL_PROP_FILTER))
                    .putExtra(AppConstants.FILTER_TYPE_KEY, AppConstants.PROPERTY_FILTER))
            }
        }
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        else
            super.onBackPressed()
    }

}