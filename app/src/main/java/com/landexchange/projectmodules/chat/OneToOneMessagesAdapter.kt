package com.landexchange.projectmodules.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.chat.UserChatMessagesMO
import kotlinx.android.synthetic.main.row_left_chat_message.view.*
import kotlinx.android.synthetic.main.row_right_chat_message.view.*

/**
 * Created by Ashu Rajput on 10/18/2019.
 */
class OneToOneMessagesAdapter(val context: Context,
    private val chatMessagesList: ArrayList<UserChatMessagesMO>,
    private val myChatUserId: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private val leftMessageView: Int = 1
    private val rightMessageView: Int = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        if (viewType == leftMessageView) {
            val view = layoutInflater.inflate(R.layout.row_left_chat_message, parent, false)
            viewHolder = LeftUsersChatViewHolder(view!!)
        } else if (viewType == rightMessageView) {
            val view = layoutInflater.inflate(R.layout.row_right_chat_message, parent, false)
            viewHolder = RightUsersChatViewHolder(view!!)
        }
        return viewHolder!!
    }

    override fun getItemViewType(position: Int): Int {
//        val viewType = chatMessagesList[position].msgViewType
        val userId = chatMessagesList[position].userId
        /*if (viewType != null) {
            return if (viewType == AppConstants.LEFT_MSG_VIEW)
                leftMessageView
            else
                rightMessageView
        }*/
        if (userId != null) {
            return if (userId == myChatUserId)
                rightMessageView
            else
                leftMessageView
        }
        return 0
    }

    override fun getItemCount(): Int {
        return chatMessagesList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RightUsersChatViewHolder)
            holder.rightBindViews()
        else if (holder is LeftUsersChatViewHolder)
            holder.leftBindView()
    }

    inner class RightUsersChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun rightBindViews() {
            itemView.rightMessageBodyTV.text = chatMessagesList[layoutPosition].userMsg
        }
    }

    inner class LeftUsersChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun leftBindView() {
//            itemView.leftUserName.text = chatMessagesList[layoutPosition].userName
            itemView.leftMessageBodyTV.text = chatMessagesList[layoutPosition].userMsg
        }
    }

    fun insertAndUpdateRV(userChatMessagesMO: UserChatMessagesMO) {
        chatMessagesList.add(userChatMessagesMO)
        notifyItemChanged(chatMessagesList.size)
//        notifyDataSetChanged()
    }
}