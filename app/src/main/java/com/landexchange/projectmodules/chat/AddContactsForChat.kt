package com.landexchange.projectmodules.chat

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboardfragments.chat.MyChatUserListMO
import com.landexchange.projectmodules.dashboardfragments.chat.adapters.UsersChatListAdapter
import com.landexchange.util.AppConstants
import com.landexchange.util.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_add_contacts.*
import kotlinx.android.synthetic.main.include_header_curve.*

/**
 * Created by Ashu Rajput on 10/25/2019.
 */
class AddContactsForChat : BaseActivity() {

    private lateinit var contactList: ArrayList<MyChatUserListMO>

    override fun getLayoutId(): Int {
        return R.layout.activity_add_contacts
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        gradientStatusBar()
        clearAllButtonTV.visibility = View.GONE
        moduleTitleName.text = resources.getString(R.string.select_contacts_string)
        fetchListOfChattedUsers()
    }

    private fun fetchListOfChattedUsers() {
        val dbReference = FirebaseDatabase.getInstance()
            .reference.child(AppConstants.FB_CHAT_USERS_KEY)

        contactList = ArrayList()
        val senderUserId = appPreferences.fireBaseUniqueUserId

        dbReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(dbSnapshot: DataSnapshot) {
                for (chatData in dbSnapshot.children) {
                    val userChatMO = chatData.getValue(MyChatUserListMO::class.java)
                    if (userChatMO!!.userId != senderUserId)
                        contactList.add(userChatMO)
                }

                if (contactList.size > 0) {
                    setUpContactRecyclerView()
                }
            }
        })
    }

    private fun setUpContactRecyclerView() {
        addContactsForChatRV.layoutManager = LinearLayoutManager(this)
        val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(this,
            R.drawable.recycler_divider)!!)
        addContactsForChatRV.addItemDecoration(itemDecorator)
        addContactsForChatRV.adapter = UsersChatListAdapter(this, contactList, true)
    }
}