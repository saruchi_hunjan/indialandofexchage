package com.landexchange.projectmodules.chat

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboardfragments.chat.MyChatUserListMO
import com.landexchange.projectmodules.dashboardfragments.chat.UserChatMessagesMO
import com.landexchange.util.AppConstants
import com.landexchange.util.AppPreferences
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.include_header_curve.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Ashu Rajput on 10/11/2019.
 */
class OneToOneChatActivity : BaseActivity() {

    private lateinit var receiverEndChatUserId: String
    private lateinit var chatMessagesList: ArrayList<UserChatMessagesMO>
    private lateinit var senderChatUserId: String
    private lateinit var receiverChatUserName: String
    private lateinit var receiverFcmTokenId: String
    private lateinit var dbReference: DatabaseReference
    private var isFirstTime: Boolean = true
    private lateinit var oneToOneMessagesAdapter: OneToOneMessagesAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_chat
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {

        gradientStatusBar()
        clearAllButtonTV.visibility = View.GONE

        val bundle = intent.extras
        if (bundle != null) {
            receiverChatUserName = bundle.getString("ChatWithUserName", "")
            moduleTitleName.text = receiverChatUserName
            receiverEndChatUserId = bundle.getString("ChatWithUserId", "")
            receiverFcmTokenId = bundle.getString("ChatWithFCMTokenID", "")
            appPreferences.receiverChatId = receiverEndChatUserId
        }

        senderChatUserId = appPreferences.fireBaseUniqueUserId

        getChatDetailsFromFireBaseDB()
        chatSendButton.setOnClickListener {
            if (chatInputMessageBox.text.toString().isNotEmpty() && chatInputMessageBox.text.toString().isNotBlank()) {
                if (chatMessagesList.size == 0) {
                    updateMyChatUserListForBothSenderAndReceiver()
                }
                sendMessageToUser()
            }
        }
    }

    private fun getChatDetailsFromFireBaseDB() {
        chatMessagesList = ArrayList()
        try {
            val childKey = compareAndGenerateKeyLexicographically(senderChatUserId,
                receiverEndChatUserId)

            dbReference = FirebaseDatabase.getInstance().reference
                .child(AppConstants.FB_CHAT_DETAILS_KEY)
                .child(childKey)

            //Initializing
            initMessageWatcher()

            dbReference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    if (dataSnapshot.exists()) {
                        Log.e("DBReference", "Child Count : " + dataSnapshot.childrenCount)
                        for (chatMessagesSnapshot in dataSnapshot.children) {
                            val userChatMessages =
                                chatMessagesSnapshot.getValue(UserChatMessagesMO::class.java)
                            chatMessagesList.add(userChatMessages!!)
                        }
                        isFirstTime = if (chatMessagesList.size > 0) {
                            setUpChatMessagesRV()
                            scrollChatMessagesToLastIndex()
                            false
                        } else {
//                            initMessageWatcher()
                            false
                        }
                    } else {
                        Log.e("DBReference", "No Child Data Exists....")
                        setUpChatMessagesRV()
                        isFirstTime = false
                    }
                }
            })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*
    * if (string1 > string2) it returns a positive value.
    * if both the strings are equal lexicographically
    * i.e.(string1 == string2) it returns 0.
    * if (string1 < string2) it returns a negative value.
    * */
    private fun compareAndGenerateKeyLexicographically(senderId: String,
        receiverId: String): String {
        val compareResult = senderId.compareTo(receiverId)
        return if (compareResult > 0)
            senderId + "_" + receiverId
        else
            receiverId + "_" + senderId
    }

    private fun initMessageWatcher() {
        //Using below child event listener to update RV every time received or send message
        dbReference.addChildEventListener(object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.e("DBReference", "onChildMoved")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                if (!isFirstTime)
                    Log.e("DBReference", "onChildChanged")
            }

            override fun onChildAdded(data: DataSnapshot, p1: String?) {
                if (!isFirstTime) {
                    if (data.exists()) {
                        Log.e("DBReference", "onChildAdded -- " + data)
                        val userChatMessages = data.getValue(UserChatMessagesMO::class.java)
                        oneToOneMessagesAdapter.insertAndUpdateRV(userChatMessages!!)
                        scrollChatMessagesToLastIndex()
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.e("DBReference", "onChildRemoved")
            }

            override fun onCancelled(p0: DatabaseError) {
                Log.e("DBReference", "onCancelled")
            }
        })
    }

    private fun setUpChatMessagesRV() {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.isSmoothScrollbarEnabled = true
        oneToOneChatRecyclerView.layoutManager = linearLayoutManager
        oneToOneMessagesAdapter = OneToOneMessagesAdapter(this, chatMessagesList, senderChatUserId)
        oneToOneChatRecyclerView.adapter = oneToOneMessagesAdapter
    }

    private fun scrollChatMessagesToLastIndex() {
        val itemPos = oneToOneChatRecyclerView.adapter!!.itemCount - 1
        oneToOneChatRecyclerView.smoothScrollToPosition(itemPos)
        oneToOneMessagesAdapter.notifyDataSetChanged();
    }

    private fun updateMyChatUserListForBothSenderAndReceiver() {

        //Updating Receiver's data under Sender's child node
        val receiverUnderSendersDBRef = FirebaseDatabase.getInstance().reference
            .child(AppConstants.FB_CHAT_USER_LIST_KEY)
            .child(senderChatUserId)
            .child(receiverEndChatUserId)

        val msgSToR = MyChatUserListMO()
        msgSToR.msgDateTime = Date().time.toString()
        msgSToR.userImageUrl = ""
        msgSToR.lastMsgSent = "I am available for chat"
        msgSToR.userId = receiverEndChatUserId
        msgSToR.userName = receiverChatUserName
        msgSToR.fcmTokenId = receiverFcmTokenId

        receiverUnderSendersDBRef.setValue(msgSToR) { dbError, dbRef ->
            if (dbError == null) {
                Log.e("DBReference", "receiverUnderSendersDBRef Success Inserted")
            } else {
                Log.e("DBReference", "receiverUnderSendersDBRef Error Occurred : $dbError")
            }
        }

        //Updating Senders's data under Receiver's child node
        val senderUnderReceiverDBRef = FirebaseDatabase.getInstance().reference
            .child(AppConstants.FB_CHAT_USER_LIST_KEY)
            .child(receiverEndChatUserId)
            .child(senderChatUserId)

        val msgRToS = MyChatUserListMO()
        msgRToS.msgDateTime = Date().time.toString()
        msgRToS.userImageUrl = ""
        msgRToS.lastMsgSent = "I am available for chat"
        msgRToS.userId = senderChatUserId
        msgRToS.userName = appPreferences.fireBaseUniqueUserName
        msgRToS.fcmTokenId = appPreferences.fcmId

        senderUnderReceiverDBRef.setValue(msgRToS) { dbError, dbRef ->
            if (dbError == null) {
                Log.e("DBReference", "senderUnderReceiverDBRef Success Inserted")
            } else {
                Log.e("DBReference", "senderUnderReceiverDBRef Error Occurred : $dbError")
            }
        }
    }

    private fun sendMessageToUser() {
        val composeMsg = UserChatMessagesMO()
        composeMsg.msgDateTime = Date().time.toString()
//        composeMsg.fcmTokenId = appPreferences.fcmId
        // Inserting receiver's FCM id as receiver going to receive msg if its in BG
        composeMsg.fcmTokenId = receiverFcmTokenId
        composeMsg.userMsg = chatInputMessageBox.text.toString()
        composeMsg.userId = senderChatUserId
        composeMsg.userName = appPreferences.fireBaseUniqueUserName
        composeMsg.notificationId = appPreferences.uniqueNotificationId

        dbReference.push().setValue(composeMsg) { dbError, dbRef ->
            if (dbError == null) {
                Log.e("DBReference", "Success Inserted")
                chatInputMessageBox.setText("")
            } else {
                Log.e("DBReference", "Error Occurred : $dbError")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        appPreferences.removeKeyValueFromPref(AppPreferences.KEY_RECEIVER_CHAT_ID)
    }
}