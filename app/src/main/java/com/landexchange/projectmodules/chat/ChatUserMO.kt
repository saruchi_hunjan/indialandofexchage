package com.landexchange.projectmodules.chat


/**
 * Created by Ashu Rajput on 10/11/2019.
 */
data class ChatUserMO(var userName: String? = null,
    var userId: String? = null,
    var userEmailId: String? = null,
    var userMessage: String? = null,
    var fcmTokenId: String? = null,
    var userMobileNo: String? = null,
    var uniqueNotificationId: Int? = null)