package com.landexchange.projectmodules.accountoptions.feedback

import com.landexchange.R
import com.landexchange.base.BaseFragment

/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class FeedbackFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = FeedbackFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_feedback
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
    }

    override fun initializeDagger() {
    }

}