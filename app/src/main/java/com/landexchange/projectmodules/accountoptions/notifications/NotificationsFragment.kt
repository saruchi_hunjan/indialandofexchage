package com.landexchange.projectmodules.accountoptions.notifications

import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.accountoptions.notifications.NotificationContractor.NotificationView
import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationsMO
import com.landexchange.util.DividerItemDecoration
import com.landexchange.util.RecyclerItemTouchHelper
import com.landexchange.util.RecyclerItemTouchHelper.RecyclerItemTouchHelperListener
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.include_no_notifications_layout.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class NotificationsFragment : BaseFragment(), NotificationView, RecyclerItemTouchHelperListener {

    @Inject
    lateinit var notificationViewModelFactory: NotificationViewModelFactory

    private lateinit var notificationViewModel: NotificationViewModel

    private lateinit var notificationList: ArrayList<NotificationsMO>

    private lateinit var notificationAdapter: NotificationsAdapter

    companion object {
        @JvmStatic
        fun newInstance() = NotificationsFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_notifications
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        notificationViewModel = ViewModelProvider(this,
            notificationViewModelFactory).get(NotificationViewModel::class.java)
        notificationViewModel.setView(this)
    }

    override fun fragmentOnCreate() {
    }

    @Suppress("UNCHECKED_CAST")
    override fun onSuccess(response: Any) {
        notificationList = response as ArrayList<NotificationsMO>
        if (notificationList.size > 0)
            setUpNotificationRecyclerView()
        else {
            notificationsRecyclerView.visibility = View.GONE
            noNotificationParentLayout.visibility = View.VISIBLE
        }
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }

    override fun setUpUi() {
        val userId = "16"
        notificationViewModel.callingNotificationsDetailsAPI(userId)
    }

    private fun setUpNotificationRecyclerView() {
        notificationsRecyclerView.visibility = View.VISIBLE
        noNotificationParentLayout.visibility = View.GONE

        notificationsRecyclerView.layoutManager = LinearLayoutManager(activity)
        notificationsRecyclerView.itemAnimator = DefaultItemAnimator()
        notificationAdapter = NotificationsAdapter(activity!!, notificationList)
        val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(context!!,
            R.drawable.recycler_divider)!!)
        notificationsRecyclerView.addItemDecoration(itemDecorator)
        notificationsRecyclerView.adapter = notificationAdapter

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(notificationsRecyclerView)
    }

    fun clearAllNotifications() {
        if (notificationList.size > 0) {
            try {
                notificationList.clear()
                notificationAdapter.notifyDataSetChanged()
                notificationsRecyclerView.visibility = View.GONE
                noNotificationParentLayout.visibility = View.VISIBLE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    //USE OR ENABLE BELOW CODE IF WANTS TO DELETE NOTIFICATION ON SWIPE RATHER THAN CLICKING ON DELETE BUTTON
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {

        if (viewHolder is NotificationsAdapter.NotificationViewHolder) {
            // get the removed item name to display it in snack bar
//            val name = cartList.get(viewHolder.getAdapterPosition()).getName()

            // backup of removed item for undo purpose
//            val deletedItem = cartList.get(viewHolder.getAdapterPosition())
//            val deletedIndex = viewHolder.getAdapterPosition()

            // remove the item from recycler view


            // showing snack bar with Undo option
            val snackBar = Snackbar.make(viewHolder.itemView,
                "Do you want to delete this notification", Snackbar.LENGTH_LONG)
            snackBar.setAction("Yes") {
                notificationAdapter.removeItem(viewHolder.getAdapterPosition())
            }
            snackBar.setActionTextColor(Color.YELLOW)
            snackBar.show()
        }
    }
}