package com.landexchange.projectmodules.accountoptions.settings

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.util.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_settings.*

/**
 * Created by Ashu Rajput on 7/3/2019.
 */
class SettingsFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = SettingsFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_settings
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        settingOptionsRecyclerView.layoutManager = LinearLayoutManager(activity)
        val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)!!)
        settingOptionsRecyclerView.addItemDecoration(itemDecorator)
        settingOptionsRecyclerView.adapter = SettingOptionsAdapter(activity!!)
    }
}