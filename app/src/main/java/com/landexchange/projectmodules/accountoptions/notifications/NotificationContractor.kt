package com.landexchange.projectmodules.accountoptions.notifications

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class NotificationContractor {
    interface NotificationView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}