package com.landexchange.projectmodules.accountoptions.notifications

import com.landexchange.base.BaseViewModel
import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationsMO
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class NotificationViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<NotificationContractor.NotificationView>() {

    fun callingNotificationsDetailsAPI(userID: String) {
        getView()!!.setLoaderVisibility(true)

        compositeDisposable.add(
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { remoteRepository.notificationsAPICall(userID) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(it.data!!)
                }, {
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error received while fetching data from Notification API")
                })
        )
    }

    private fun getNotificationDetails(): ArrayList<NotificationsMO> {
        return arrayListOf(NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
            "Newely constructed 3BHK properties in Malviya Nagar for you.", 1),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Your property just got 10 new views. Seems its getting boosted.", 2),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Your property just got 10 new views. Seems its getting boosted.", 2),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Newely constructed 3BHK properties in Malviya Nagar for you.", 1),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Newely constructed 3BHK properties in Malviya Nagar for you.", 1),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Your property just got 10 new views. Seems its getting boosted.", 2),
            NotificationsMO("http://indialandexchange.com//indialand//assets//images//property//19.jpg",
                "Your property just got 10 new views. Seems its getting boosted.", 2))
    }
}