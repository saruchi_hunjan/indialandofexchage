package com.landexchange.projectmodules.accountoptions.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationsMO
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_notifications.view.*

/**
 * Created by Ashu Rajput on 6/1/2019.
 */
class NotificationsAdapter(val context: Context, private val notificationList: ArrayList<NotificationsMO>) :
    RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder>() {

    private val layoutInt: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = layoutInt.inflate(R.layout.row_notifications, parent, false)
        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindNotificationItems()
    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var notificationViewForeground: LinearLayout

        fun bindNotificationItems() {
            try {
                notificationViewForeground = itemView.findViewById(R.id.notificationViewForeground)
                Picasso.get()
                    .load(notificationList[layoutPosition].notificationImage)
//                .placeholder(R.drawable.logo_city_delhi)
                    .tag(context)
                    .into(itemView.notificationImageView)

                itemView.notificationContentTV.text = notificationList[layoutPosition].notificationContent
                if (notificationList[layoutPosition].notificationType == 1)
                    itemView.notificationViewNowButton.visibility = View.VISIBLE
                else if (notificationList[layoutPosition].notificationType == 2)
                    itemView.notificationSeeViewersButton.visibility = View.VISIBLE

                /*itemView.notificationDeleteButton.setOnClickListener {
                    removeItem(layoutPosition)
                }*/

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun removeItem(position: Int) {
        notificationList.removeAt(position)
        notifyItemRemoved(position)
    }
}