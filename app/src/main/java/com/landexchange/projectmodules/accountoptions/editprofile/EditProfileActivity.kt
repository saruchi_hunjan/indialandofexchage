package com.landexchange.projectmodules.accountoptions.editprofile

import android.view.View
import com.landexchange.R
import com.landexchange.base.BaseActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*

/**
 * Created by Ashu Rajput on 5/17/2019.
 */
class EditProfileActivity : BaseActivity() {

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_edit_profile
    }

    override fun setUpUi() {
        gradientStatusBar()
        editProfileBackButton.setOnClickListener(onClickListener)
        editProfileSaveButton.setOnClickListener(onClickListener)
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.editProfileBackButton -> finish()
            R.id.editProfileSaveButton -> finish()
        }
    }

}