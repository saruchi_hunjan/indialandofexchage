package com.landexchange.projectmodules.accountoptions.notifications.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 10/5/2019.
 */
data class NotificationAPIResponse(

    @field:SerializedName("StatusCode")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: ArrayList<NotificationsMO>? = null
)