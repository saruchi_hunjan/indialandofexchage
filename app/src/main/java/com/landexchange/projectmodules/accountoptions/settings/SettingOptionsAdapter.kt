package com.landexchange.projectmodules.accountoptions.settings

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.webpagelinks.ILEWebPages
import com.landexchange.util.AppConstants
import kotlinx.android.synthetic.main.row_setting_options.view.*

/**
 * Created by Ashu Rajput on 7/3/2019.
 */
class SettingOptionsAdapter(val context: Context) :
    RecyclerView.Adapter<SettingOptionsAdapter.SettingOptionsViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    private val optionsList = arrayListOf(context.resources.getString(R.string.tou_title),
        context.resources.getString(R.string.privacy_policy_title),
        context.resources.getString(R.string.about_us_title),
        context.resources.getString(R.string.contact_us_title),
        context.resources.getString(R.string.shareAppText))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingOptionsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_setting_options, parent, false)
        return SettingOptionsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return optionsList.size
    }

    override fun onBindViewHolder(holder: SettingOptionsViewHolder, position: Int) {
        holder.bindSettingOptionsItems()
    }

    inner class SettingOptionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindSettingOptionsItems() {
            itemView.settingOptionsItemsLabel.text = optionsList[layoutPosition]
            itemView.setOnClickListener {
                when (layoutPosition) {
                    0 -> context.startActivity(Intent(context, ILEWebPages::class.java)
                        .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_TOU))
                    1 -> context.startActivity(Intent(context, ILEWebPages::class.java)
                        .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_PRIVACY_POLICY))
                    2 -> context.startActivity(Intent(context, ILEWebPages::class.java)
                        .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_ABOUT_US))
                    3 -> context.startActivity(Intent(context, ILEWebPages::class.java)
                        .putExtra(AppConstants.WEB_VIEW_KEY, AppConstants.WEB_VIEW_CONTACT_US))
                    4 -> {
                        val intent = Intent()
                        intent.action = Intent.ACTION_SEND
                        intent.putExtra(Intent.EXTRA_TEXT, "I suggest this app for you : ILE")
                        intent.type = "text/plain"
                        context.startActivity(intent)
                    }
                }
            }
        }
    }
}