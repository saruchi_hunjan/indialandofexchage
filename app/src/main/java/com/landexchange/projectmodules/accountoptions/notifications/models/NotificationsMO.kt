package com.landexchange.projectmodules.accountoptions.notifications.models

/**
 * Created by Ashu Rajput on 6/1/2019.
 */

/**
 * @param notificationType
 * notificationType=1: View Now
 * notificationType=2: See Viewers
 */
data class NotificationsMO(var notificationImage: String, var notificationContent: String, var notificationType: Int)