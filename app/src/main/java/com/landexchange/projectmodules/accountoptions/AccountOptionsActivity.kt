package com.landexchange.projectmodules.accountoptions

import android.view.View
import com.jakewharton.rxbinding3.view.clicks
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.accountoptions.feedback.FeedbackFragment
import com.landexchange.projectmodules.accountoptions.notifications.NotificationsFragment
import com.landexchange.projectmodules.accountoptions.settings.SettingsFragment
import com.landexchange.util.AppConstants
import com.sisindia.csat.util.addFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.include_header_curve.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 5/31/2019.
 */
class AccountOptionsActivity : BaseActivity() {

    @Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    override fun getLayoutId(): Int {
        return R.layout.activity_account_options
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        gradientStatusBar()
        getValueFromBundle()

        compositeDisposable.add(moduleTitleName.clicks().observeOn(AndroidSchedulers.mainThread()).subscribe { finish() })

        compositeDisposable.add(clearAllButtonTV.clicks().observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                var fragment =
                    supportFragmentManager.findFragmentByTag(mResources.getString(R.string.fragTagNotificationName)) as NotificationsFragment
                fragment.clearAllNotifications()
                clearAllButtonTV.visibility = View.GONE
            })
    }

    private fun getValueFromBundle() {
        val receivedBundle = intent.extras
        if (receivedBundle != null) {
            if (receivedBundle.containsKey(AppConstants.ACCOUNT_MODULE_TYPE)) {
                when (receivedBundle.getString(AppConstants.ACCOUNT_MODULE_TYPE)) {
                    AppConstants.ACCOUNT_MODULE_NOTIFICATION -> {
                        moduleTitleName.text = resources.getString(R.string.accountNotification)
                        addFragment(NotificationsFragment.newInstance(), R.id.accountOptionsFrameLayout,
                            mResources.getString(R.string.fragTagNotificationName))
                    }
                    AppConstants.ACCOUNT_MODULE_FEEDBACK -> {
                        clearAllButtonTV.visibility = View.GONE
                        moduleTitleName.text = resources.getString(R.string.accountFeedback)
                        addFragment(FeedbackFragment.newInstance(), R.id.accountOptionsFrameLayout,
                            mResources.getString(R.string.fragTagFeedbackName))
                    }
                    AppConstants.ACCOUNT_MODULE_SETTINGS -> {
                        clearAllButtonTV.visibility = View.GONE
                        moduleTitleName.text = resources.getString(R.string.accountSettings)
                        addFragment(SettingsFragment.newInstance(), R.id.accountOptionsFrameLayout,
                            mResources.getString(R.string.fragTagSettingsName))
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}