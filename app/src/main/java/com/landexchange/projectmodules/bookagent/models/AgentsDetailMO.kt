package com.landexchange.projectmodules.bookagent.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Ashu Rajput on 5/14/2019.
 */
data class AgentsDetailMO(
    @field:SerializedName("agentId")
    var agentID: String? = null,

    @field:SerializedName("Firstname")
    var firstName: String? = null,

    @field:SerializedName("Lastname")
    var lastName: String? = null,

    @field:SerializedName("Image")
    var agentImageURL: String? = null,

    @field:SerializedName("location")
    var agentLocation: String? = null,

    @field:SerializedName("latitude")
    var latitue: String? = null,

    @field:SerializedName("longitude")
    var longitude: String? = null,

    @field:SerializedName("openproject")
    var openProjectCount: String? = null,

    @field:SerializedName("closeproject")
    var closeProjectCount: String? = null,

    @field:SerializedName("rating")
    var rating: Float? = null,

    var type: Int? = null
) : Serializable