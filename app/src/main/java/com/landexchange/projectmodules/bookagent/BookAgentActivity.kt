package com.landexchange.projectmodules.bookagent

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.view.View
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.agentdetails.AgentDetailsActivity
import com.landexchange.projectmodules.bookagent.BookAgentContractor.BookAgentView
import com.landexchange.projectmodules.bookagent.adapter.BookAgentAdapter
import com.landexchange.projectmodules.bookagent.models.AgentsDetailAPIResponse
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import com.landexchange.projectmodules.filters.FiltersMainActivity
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO
import com.landexchange.projectmodules.popularlocalities.AreaLocalitiesActivity
import com.landexchange.projectmodules.seeall.SeeAllAgentsActivity
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.showLongToast
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_book_agent.*
import kotlinx.android.synthetic.main.bottom_sheet_sorting.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 5/13/2019.
 */
class BookAgentActivity : BaseActivity(), BookAgentView, OnMapReadyCallback {

    @Inject
    lateinit var ileUtility: ILEUtility

    private lateinit var googleMap: GoogleMap
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var bookAgentViewModelFactory: BookAgentViewModelFactory

    private lateinit var bookAgentViewModel: BookAgentViewModel
    private lateinit var agentList: ArrayList<AgentsDetailMO>
    private lateinit var cityLocalityJson: CityLocalitiesMO

    private val locationPermissionCode = 2
    private val changeCityNameCode = 3
    private lateinit var agentDetailsList: ArrayList<AgentsDetailMO>

    override fun getLayoutId(): Int {
        return R.layout.activity_book_agent
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        bookAgentViewModel =
            ViewModelProvider(this, bookAgentViewModelFactory).get(BookAgentViewModel::class.java)
        bookAgentViewModel.setView(this)
    }

    override fun setUpUi() {
        gradientStatusBar()
        updateHeaderCityText()
        setUpAgentsOnMap()
        bottomSheetBehavior = BottomSheetBehavior.from(sorting_bottom_sheet_layout)
        bookAgentHeaderBackButton.setOnClickListener(clickListener)
        sortingButton.setOnClickListener(clickListener)
        sortingAlphabetAtoZ.setOnClickListener(clickListener)
        sortingAlphabetZToA.setOnClickListener(clickListener)
        sortingRatingHighToLow.setOnClickListener(clickListener)
        sortingRatingLowToHigh.setOnClickListener(clickListener)
        bookAgentCityLocationTV.setOnClickListener(clickListener)
        bookAgentShowInList.setOnClickListener(clickListener)
        filtersButton.setOnClickListener(clickListener)
        searchAgentsButton.setOnClickListener(clickListener)
    }

    private fun updateHeaderCityText() {
        cityLocalityJson = appPreferences.cityLocalitiesJson
        if (cityLocalityJson.cityName.length > 15)
            bookAgentCityLocationTV.layoutParams.width = 190
        bookAgentCityLocationTV.text = cityLocalityJson.cityName
    }

    override fun onSuccess(response: Any) {
        if (response is AgentsDetailAPIResponse) {
            response.let {
                agentDetailsList = it.data!!
                if (agentDetailsList.size > 0) {
                    updateAgentPinsOnMap(agentDetailsList)
                    updateAgentsRecyclerView(agentDetailsList)
                }
            }
        }
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
        if (isLoaderVisible) showProgressDialog(mResources.getString(R.string.message_loading_please_wait))
        else hideProgressDialog()
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.sortingButton -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                else
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingAlphabetAtoZ -> {
                callAllAgentsListActivity(AppConstants.SORTING_ATOZ, true)
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingAlphabetZToA -> {
                callAllAgentsListActivity(AppConstants.SORTING_ZTOA, true)
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingRatingHighToLow -> {
                callAllAgentsListActivity(AppConstants.SORTING_RATING_HIGH_TO_LOW, true)
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.sortingRatingLowToHigh -> {
                callAllAgentsListActivity(AppConstants.SORTING_RATING_LOW_TO_HIGH, true)
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            R.id.bookAgentCityLocationTV -> {
                startActivityForResult(Intent(this, AreaLocalitiesActivity::class.java)
                    .putExtra(AppConstants.SELECTED_CITY_NAME_KEY, cityLocalityJson.cityName)
                    .putExtra(AppConstants.SELECTED_CITY_CODE_KEY, cityLocalityJson.cityCode)
                    .putExtra(AppConstants.COMING_FROM_AGENT_KEY, true), changeCityNameCode)
            }
            R.id.bookAgentShowInList -> callAllAgentsListActivity("", false)

            R.id.filtersButton -> {
                startActivity(Intent(this, FiltersMainActivity::class.java)
                    .putExtra(AppConstants.FILTER_TYPE_KEY, AppConstants.AGENT_FILTER))
            }
            R.id.searchAgentsButton -> startActivity(Intent(this@BookAgentActivity,
                SearchAgentActivity::class.java))

            R.id.bookAgentHeaderBackButton -> finish()
        }
    }

    private fun callAllAgentsListActivity(sortingType: String, isSortingNeed: Boolean) {

        val agentListIntent = Intent(this, SeeAllAgentsActivity::class.java)
        agentListIntent.putExtra(AppConstants.SEE_ALL_AGENT_KEY, agentDetailsList)
        if (isSortingNeed)
            agentListIntent.putExtra(AppConstants.SORTING_TYPE_KEY, sortingType)
        startActivity(agentListIntent)
    }

    private val onMarkerClickListener = GoogleMap.OnMarkerClickListener {
        try {
            startActivity(Intent(this, AgentDetailsActivity::class.java)
                .putExtra(AppConstants.AGENT_DETAILS_KEY, agentDetailsList[it.tag as Int]))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        true
    }

    private fun setUpAgentsOnMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.agentsMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap!!
        googleMap.setOnMarkerClickListener(onMarkerClickListener)
        checkLocationRTP()
    }

    private fun updateAgentPinsOnMap(agentList: ArrayList<AgentsDetailMO>) {
        try {
            this.agentList = agentList

            agentList.forEachIndexed { index, agentDetailsMO ->
                compositeDisposable.add(getSingleBitmapOfAgent(Picasso.get(),
                    agentDetailsMO.agentImageURL!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (agentDetailsMO.latitue!!.isNotEmpty() && agentDetailsMO.longitude!!.isNotEmpty())
                            updateAgentPicOnPinFromBitmap(it, agentDetailsMO, index)
                    }, Throwable::printStackTrace))
            }

            /*for (agentDetailsMO in agentList) {
                compositeDisposable.add(getSingleBitmapOfAgent(Picasso.get(), agentDetailsMO.agentImageURL!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (agentDetailsMO.latitue!!.isNotEmpty() && agentDetailsMO.longitude!!.isNotEmpty())
                            updateAgentPicOnPinFromBitmap(it, agentDetailsMO)
                    }, Throwable::printStackTrace))
            }*/

            //CREATING CIRCLE ON MAP AND FOCUSING CAMERA ON THAT PIN
            try {
                /* val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                 val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                 val latitude = location.latitude
                 val longitude = location.longitude*/

                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))

                val latitude = agentList[0].latitue!!.toDouble()
                val longitude = agentList[0].longitude!!.toDouble()

                val radiusArea = ileUtility.getDiameterFromDeviceWidth()
                googleMap.addCircle(CircleOptions().center(LatLng(latitude, longitude)).radius(
                    radiusArea)
                    .strokeWidth(0f).fillColor(ContextCompat.getColor(this,
                        R.color.colorLightTransparent)))

                googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(latitude, longitude)))
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f))

            } catch (e: Exception) {
                e.printStackTrace()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun updateAgentPicOnPinFromBitmap(profileBitmap: Bitmap,
        agentMO: AgentsDetailMO, indexId: Int) {
        try {
            val markerOptions =
                MarkerOptions().position(LatLng(agentMO.latitue!!.toDouble(),
                    agentMO.longitude!!.toDouble()))
                    .icon(BitmapDescriptorFactory.fromBitmap(ileUtility.getPicOnMarkerFromCustomView(
                        this, profileBitmap)))

            val marker = googleMap.addMarker(markerOptions)
            marker.tag = indexId

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getSingleBitmapOfAgent(picasso: Picasso, url: String): Single<Bitmap> =
        Single.create {
            try {
                if (!it.isDisposed) {
                    val bitmap: Bitmap = picasso.load(url).get()
                    it.onSuccess(bitmap)
                }
            } catch (e: Throwable) {
                it.onError(e)
            }
        }

    private fun updateAgentsRecyclerView(agentListDetails: ArrayList<AgentsDetailMO>) {
        bookAgentsBottomRecyclerView.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val bookAgentAdapter = BookAgentAdapter(this, ileUtility)
        bookAgentsBottomRecyclerView.adapter = bookAgentAdapter
        compositeDisposable.add(Observable.just(agentListDetails).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { bookAgentAdapter.updateBookAgentsRecyclerView(it) })
    }

    private fun checkLocationRTP() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION),
                locationPermissionCode)
        else
            fetchCurrentLocationFromGPS()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
        permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == locationPermissionCode) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showLongToast("Location permission is required")
            } else {
                fetchCurrentLocationFromGPS()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetchCurrentLocationFromGPS() {
        val agentDetailsMap = HashMap<String, Int>()
        agentDetailsMap[AppConstants.CITY_ID] = cityLocalityJson.cityCode
        agentDetailsMap[AppConstants.AREA_ID] = cityLocalityJson.localityCode
        bookAgentViewModel.callingAgentDetailsAPI(agentDetailsMap)
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == changeCityNameCode && resultCode == Activity.RESULT_OK) {
            updateHeaderCityText()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}