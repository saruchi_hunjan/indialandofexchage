package com.landexchange.projectmodules.bookagent.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.agentdetails.AgentDetailsActivity
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_book_agent.view.*

/**
 * Created by Ashu Rajput on 5/13/2019.
 */
class BookAgentAdapter(val context: Context, val ileUtility: ILEUtility) :
    RecyclerView.Adapter<BookAgentAdapter.BookAgentViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private val layoutWidth = ileUtility.getDeviceWidthAndHeightForCardViews()
    private var bookAgentList = ArrayList<AgentsDetailMO>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookAgentViewHolder {
        val view = layoutInflater.inflate(R.layout.row_book_agent, parent, false)
        return BookAgentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bookAgentList.size
    }

    override fun onBindViewHolder(holder: BookAgentViewHolder, position: Int) {
        holder.bindAgentItems()
    }

    inner class BookAgentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindAgentItems() {
            try {
                itemView.bookAgentRowParentLayout.layoutParams.width = layoutWidth
                itemView.agentName.text = context.resources.getString(R.string.dynamicAgentName,
                    bookAgentList[layoutPosition].firstName, bookAgentList[layoutPosition].lastName)

                itemView.agentsActiveProperty.text =
                    ileUtility.fromHtml("<b><font color='#3b3b3b'>12</font></b> Active<br>&nbsp;&nbsp;&nbsp;&nbsp;Property")
                itemView.agentsClosedProperty.text =
                    ileUtility.fromHtml("<b><font color='#3b3b3b'>18</font></b> Closed<br>&nbsp;&nbsp;&nbsp;&nbsp;Property")

                Picasso.get().load(bookAgentList[layoutPosition].agentImageURL)
//                .placeholder(R.drawable.logo_city_delhi)
                    .tag(context)
                    .into(itemView.agentProfilePic)

                itemView.setOnClickListener {
                    try {
                        val pair1 = Pair<View, String>(itemView.agentName,
                            context.getString(R.string.transitionAgentName))
                        val pair2 = Pair<View, String>(itemView.agentProfilePic,
                            context.getString(R.string.transitionAgentProfile))
                        val pair3 = Pair<View, String>(itemView.agentCallingButton,
                            context.getString(R.string.transitionAgentCalling))
                        val options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(context as Activity, pair1, pair2, pair3).toBundle()
                        context.startActivity(Intent(context, AgentDetailsActivity::class.java)
                            .putExtra(AppConstants.AGENT_DETAILS_KEY, bookAgentList[layoutPosition]), options)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun updateBookAgentsRecyclerView(bookAgentList: ArrayList<AgentsDetailMO>) {
        this.bookAgentList = bookAgentList
        notifyDataSetChanged()
    }

}