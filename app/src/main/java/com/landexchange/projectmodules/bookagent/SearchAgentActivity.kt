package com.landexchange.projectmodules.bookagent

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.widget.TextViewTextChangeEvent
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.bookagent.adapter.SearchableAgentsAdapter
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.home_items_titles.*
import kotlinx.android.synthetic.main.layout_searcheable_data.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 5/15/2019.
 */
class SearchAgentActivity : BaseActivity() {

    @Inject
    @field:Named("activity")
    lateinit var compositeDisposable: CompositeDisposable

    private lateinit var searchableAgentsAdapter: SearchableAgentsAdapter

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_searcheable_data
    }

    override fun setUpUi() {
        gradientStatusBar()
        homeItemsTitleLeft.text = resources.getString(R.string.recentSearch)
        headerSearchBackButton.setOnClickListener(onClickListener)
        homeItemsTitleRight.setOnClickListener(onClickListener)
        searchableAgentsRecyclerView()
        executeAgentsSearchProcess()
    }

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.headerSearchBackButton -> finish()
            R.id.homeItemsTitleRight -> {
            }
        }
    }

    private fun searchableAgentsRecyclerView() {
        searchableDataRecyclerView.layoutManager = LinearLayoutManager(this)
        searchableAgentsAdapter = SearchableAgentsAdapter(this, tempFunction())
        searchableDataRecyclerView.adapter = searchableAgentsAdapter
    }

    private fun tempFunction(): ArrayList<AgentsDetailMO> {

        val agentList = arrayListOf(
            AgentsDetailMO("", "Anil", "Kumar", "", "", "",
                "", "", "", 4.6f, 0),
            AgentsDetailMO("", "Akash", "Kumar", "", "", "",
                "", "", "", 4.1f, 0),
            AgentsDetailMO("", "", "", "", "", "",
                "", "", "", 0f, 1),
            AgentsDetailMO("", "Pinku", "Kumar", "", "", "",
                "", "", "", 3.1f, 0),
            AgentsDetailMO("", "Corbett", "Kumar", "", "", "",
                "", "", "", 4.1f, 0),
            AgentsDetailMO("", "Dino", "Kumar", "", "", "",
                "", "", "", 4.5f, 0),
            AgentsDetailMO("", "Satish", "Kumar", "", "", "",
                "", "", "", 5.0f, 0),
            AgentsDetailMO("", "Mahesh", "Kumar", "", "", "",
                "", "", "", 4.3f, 0),
            AgentsDetailMO("", "Rajesh", "Kumar", "", "", "",
                "", "", "", 1.0f, 0),
            AgentsDetailMO("", "Happu", "Kumar", "", "", "",
                "", "", "", 2.3f, 0),
            AgentsDetailMO("", "Gappu", "Kumar", "", "", "",
                "", "", "", 3.0f, 0))

        return agentList
    }

    private fun executeAgentsSearchProcess() {
        compositeDisposable.add(
            searchableEditTextWidget.textChangeEvents()
                .skipInitialValue()
                .debounce(250, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<TextViewTextChangeEvent>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: TextViewTextChangeEvent) {
                        searchableAgentsAdapter.filter.filter(t.text)
                    }

                    override fun onError(e: Throwable) {
                    }
                })
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}