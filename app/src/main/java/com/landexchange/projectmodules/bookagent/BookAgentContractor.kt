package com.landexchange.projectmodules.bookagent

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 5/14/2019.
 */
class BookAgentContractor {
    interface BookAgentView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}