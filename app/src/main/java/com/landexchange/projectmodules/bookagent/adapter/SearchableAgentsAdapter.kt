package com.landexchange.projectmodules.bookagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.bookagent.models.AgentsDetailMO
import kotlinx.android.synthetic.main.row_searchable_agents.view.*

/**
 * Created by Ashu Rajput on 5/15/2019.
 */
class SearchableAgentsAdapter(val context: Context, private val agentsList: List<AgentsDetailMO>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var agentsFilterableList: List<AgentsDetailMO>

    companion object {
        const val TYPE_ITEM = 0
        const val TYPE_LINE = 1
    }

    init {
        agentsFilterableList = agentsList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            TYPE_ITEM -> {
                val view = layoutInflater.inflate(R.layout.row_searchable_agents, parent, false)
                SearchableAgentViewHolder(view)
            }
            TYPE_LINE -> {
                val view = layoutInflater.inflate(R.layout.divider_line_full, parent, false)
                DividerViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        return agentsFilterableList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (agentsList[position].type == TYPE_ITEM)
            TYPE_ITEM
        else
            TYPE_LINE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SearchableAgentViewHolder)
            holder.bindSearchableAgentViews()
    }

    inner class SearchableAgentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindSearchableAgentViews() {
            try {
                itemView.agentName.text = context.resources.getString(R.string.dynamicAgentName,
                    agentsList[layoutPosition].firstName, agentsFilterableList[layoutPosition].lastName)
                itemView.agentRatingValue.text = agentsFilterableList[layoutPosition].rating.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    inner class DividerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                if (charSequence.isNullOrEmpty())
                    agentsFilterableList = agentsList
                else {
                    val loopedSearchingList = ArrayList<AgentsDetailMO>()
                    for (agentsMO in agentsList) {
                        if (agentsMO.firstName!!.toLowerCase().contains(charSequence.toString().toLowerCase()))
                            loopedSearchingList.add(agentsMO)
                    }
                    agentsFilterableList = loopedSearchingList
                }

                val filterResults = FilterResults()
                filterResults.values = agentsFilterableList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                agentsFilterableList = results!!.values as List<AgentsDetailMO>
                notifyDataSetChanged()
            }
        }
    }
}