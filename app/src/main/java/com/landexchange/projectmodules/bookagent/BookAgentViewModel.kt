package com.landexchange.projectmodules.bookagent

import com.landexchange.base.BaseViewModel
import com.landexchange.projectmodules.bookagent.models.AgentsDetailAPIResponse
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ashu Rajput on 5/14/2019.
 */
class BookAgentViewModel constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<BookAgentContractor.BookAgentView>() {

    fun callingAgentDetailsAPI(queryMap: HashMap<String, Int>) {
        getView()!!.setLoaderVisibility(true)
        compositeDisposable.add(
            io.reactivex.Observable.just(1)
                .subscribeOn(Schedulers.computation())
                .flatMap { remoteRepository.agentsDetailsAPICall(queryMap) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer<AgentsDetailAPIResponse> { commonResponse ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onSuccess(commonResponse)
                }, Consumer<Throwable> { throwable ->
                    getView()!!.setLoaderVisibility(false)
                    getView()!!.onFailure("Error Occurred")
                })
        )
    }

}