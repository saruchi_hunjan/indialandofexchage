package com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.PropertyConfigFragment

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PropertyConfigTabPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = PropertyConfigFragment()
            1 -> fragment = PropertyConfigFragment()
            2 -> fragment = PropertyConfigFragment()
            3 -> fragment = PropertyConfigFragment()
//            1 -> fragment = ActiveInactivePropertyFragment()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 4
    }

}