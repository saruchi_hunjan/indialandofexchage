package com.landexchange.projectmodules.dedicatedscreens.compare

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.projectmodules.dedicatedscreens.compare.models.ComparablePropertyMO
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_compare_property.view.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 6/20/2019.
 */
class ComparePropertyAdapter(val context: Context, private val listOfComparableProp: ArrayList<ComparablePropertyMO>) :
    RecyclerView.Adapter<ComparePropertyAdapter.ComparePropViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var layoutWidth = 0

    @Inject
    lateinit var ileUtility: ILEUtility

    init {
        IleApplication.getRoomComponent().inject(this)
        layoutWidth = ileUtility.getDeviceWidthForCompareCardView()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComparePropViewHolder {
        val view = layoutInflater.inflate(R.layout.row_compare_property, parent, false)
        return ComparePropViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfComparableProp.size
    }

    override fun onBindViewHolder(holder: ComparePropViewHolder, position: Int) {
        holder.bindComparablePropItems()
    }

    inner class ComparePropViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindComparablePropItems() {
            try {
                if (listOfComparableProp.size > 1)
                    itemView.comparablePropertiesParentLayout.layoutParams.width = layoutWidth

                itemView.comparablePropPrice.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.comparablePropPrice,
                        listOfComparableProp[layoutPosition].propertyPrice))

                itemView.comparableDevOrAgentName.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.comparableDevOrAgent,
                        listOfComparableProp[layoutPosition].propertyName, "Omaxe Reality Group"))

                itemView.comparableApartmentAddressSqFeet.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.comparableAptAddressSqFeet,
                        "2 BHK", "Khirki Ext Malviya Nagar", "1400 Sq Feet"))

                itemView.propertyAvgPrice.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.avgPrice, "32K"))

                itemView.propertyPossessionName.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.dynamicPossessionName, "Omaxe Nile"))
                itemView.propertyType.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.avgPrice, "32K"))
                itemView.propertyAmenities.text =
                    ileUtility.fromHtml(context.resources.getString(R.string.dynamicPossessionName, "Omaxe Nile"))

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}