package com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PropertyAmenitiesAdapter(val context: Context) :
    RecyclerView.Adapter<PropertyAmenitiesAdapter.AmenitiesViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AmenitiesViewHolder {
        val view = layoutInflater.inflate(R.layout.row_prop_amenities, parent, false)
        return AmenitiesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: AmenitiesViewHolder, position: Int) {
        holder.bindAmenitiesItems()
    }

    inner class AmenitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindAmenitiesItems() {
//            itemView.propAmenitiesValue.text = ""
        }
    }
}