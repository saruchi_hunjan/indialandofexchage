package com.landexchange.projectmodules.dedicatedscreens.propertydetails

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PropertyDetailsContractor {
    interface PropDetailsView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}