package com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectMO
import com.landexchange.util.ILEUtility
import kotlinx.android.synthetic.main.row_feature_project.view.*


/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class SimilarProjectsAdapter(val context: Context, val ileUtility: ILEUtility,
    private val featureProjectList: ArrayList<FeaturedProjectMO>) :
    RecyclerView.Adapter<SimilarProjectsAdapter.SimilarProjectViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var deviceWidth = 0
    private var deviceHeight = 0

    init {
        getDeviceWidthAndHeight()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarProjectViewHolder {
        val view = layoutInflater.inflate(R.layout.row_feature_project, parent, false)
        return SimilarProjectViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: SimilarProjectViewHolder, position: Int) {
        holder.bindSimilarProjectItems()
    }

    inner class SimilarProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindSimilarProjectItems() {
            try {
                itemView.featureProjectRowParentLayout.layoutParams.width = deviceWidth
                itemView.featureProjectRowParentLayout.layoutParams.height = deviceHeight

                /*itemView.projectDeveloperOrAgentTV.text = ileUtility.fromHtml(
                    featureProjectList[layoutPosition].projectName + "<br>" +
                            "<small><font color='#9e9e9e'>" + featureProjectList[layoutPosition].projectShortDesc + "</font></small>"
                )
                itemView.projectAptDetailsCumAddressTV.text = ileUtility.fromHtml(
                    featureProjectList[layoutPosition].propertyType + " " +
                            featureProjectList[layoutPosition].propertyName +
                            "<br>" + "<small><font color='#9e9e9e'>Address</font></small>"
                )

                itemView.projectArea.text = featureProjectList[layoutPosition].propertyArea
                itemView.projectBedsCountTV.text = featureProjectList[layoutPosition].propertyBeds
                itemView.projectBathCountTV.text = featureProjectList[layoutPosition].propertyBaths
                itemView.projectGarageCountTV.text = featureProjectList[layoutPosition].propertyGarage
                itemView.projectFloorCountTV.text = featureProjectList[layoutPosition].propertyFloor

                Picasso.get()
                    .load(featureProjectList[layoutPosition].projectImageURL)
                    .tag(context)
                    .into(itemView.projectImage)*/

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getDeviceWidthAndHeight() {
        if (deviceWidth == 0 && deviceHeight == 0) {
            val getDeviceWidth = Resources.getSystem().displayMetrics.widthPixels
            deviceWidth = getDeviceWidth - (getDeviceWidth / 4)
            deviceHeight = getDeviceWidth - 30
        }
    }

}