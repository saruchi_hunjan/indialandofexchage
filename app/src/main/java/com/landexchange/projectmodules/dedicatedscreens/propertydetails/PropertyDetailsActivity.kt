package com.landexchange.projectmodules.dedicatedscreens.propertydetails

import android.content.Intent
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dedicatedscreens.compare.ComparePropertiesActivity
import com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.ExploreNeighbourhoodActivity
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters.*
import com.landexchange.util.AppConstants
import com.landexchange.util.ILEUtility
import com.landexchange.util.adapters.ILEPagerAdapter
import com.landexchange.util.iledialog.DialogYesNo
import kotlinx.android.synthetic.main.activity_property_details.*
import kotlinx.android.synthetic.main.include_banner_view.*
import kotlinx.android.synthetic.main.include_chat_contact_share_layout.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 6/14/2019.
 */
class PropertyDetailsActivity : BaseActivity(), PropertyDetailsContractor.PropDetailsView {

    private lateinit var propertyConfigTabLayout: TabLayout
    private lateinit var propertyConfigViewPager: ViewPager

    @Inject
    lateinit var ileUtility: ILEUtility

    override fun getLayoutId(): Int {
        return R.layout.activity_property_details
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {

        setDynamicValuesOnLabelsAndTVs()

        setFullModeScreenForStatusBar()
        showOverFlowMenu()
        showPropertyImagesOnPager()
        setUpPropertyDetailsRecyclerView()
        setUp3DAdsBanner()
        setUpConfigTabsWithViewPager()
        setUpAmenitiesRecyclerView()
        priceTrendsLabelTV.text = resources.getString(R.string.dynamicPriceTrends, "Malviya Nagar")
        setUpPriceTrendsRecyclerView()
        updateSimilarProjectsRVItems()

        exploreNeighbourhoodButton.setOnClickListener(clickListener)
        shareButton.setOnClickListener(clickListener)
        exportPDFButton.setOnClickListener(clickListener)
    }

    override fun onSuccess(response: Any) {
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }

    private fun showOverFlowMenu() {
        dotsOverFlowMenu.setOnClickListener {
            val popUpMenu = PopupMenu(this, it)
            popUpMenu.gravity = Gravity.END
            popUpMenu.apply {
                menuInflater.inflate(R.menu.property_option_menu, menu)
                setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menuCompareScreen -> {
                            startActivity(Intent(this@PropertyDetailsActivity, ComparePropertiesActivity::class.java))
                        }
                        R.id.menuShareProject -> {
                        }
                    }
                    true
                }
            }.show()
        }
    }

    private fun setDynamicValuesOnLabelsAndTVs() {

        /*val spanString = SpannableString(resources.getString(R.string.dynamicEMIValue, "42K"))

        val underLineSpan = UnderlineSpan()
        val underLinePaint = TextPaint()
        underLinePaint.color = ContextCompat.getColor(this, R.color.colorBlueLinks)
        underLineSpan.updateDrawState(underLinePaint)
        spanString.setSpan(underLineSpan, 0, spanString.length, 0)*/

        emiValueTV.text = resources.getString(R.string.dynamicEMIValue, "42K")

        propertyTypeInBHKs.text = resources.getString(R.string.dynamicPropValueInBHKs, "3 BHK")

        chatWithButton.text = resources.getString(R.string.dynamicChatWith, "Concord")
        callOrContactButton.text = resources.getString(R.string.dynamicContact, "Concord")
        rateOrScheduleButton.text = resources.getString(R.string.scheduleTour)
    }

    private fun showPropertyImagesOnPager() {
        propertyImageViewPager.adapter = ILEPagerAdapter(this, false)
        circularIndicator.setViewPager(propertyImageViewPager)
    }

    private fun setUpPropertyDetailsRecyclerView() {
        propertyDetailsRecyclerView.layoutManager = GridLayoutManager(this, 2,
            LinearLayoutManager.HORIZONTAL, false)
        propertyDetailsRecyclerView.adapter = PropertyDetailsAdapter(this)
    }

    private fun setUp3DAdsBanner() {
        banner3DText.text = ileUtility.fromHtml(resources.getString(R.string.banner3DAdText1) + "<br><small>" +
                resources.getString(R.string.banner3DAdText2, "Purvankar Height") + "</small>")
    }

    private fun setUpConfigTabsWithViewPager() {

        propertyConfigTabLayout = this.findViewById(R.id.propertyConfigTabLayout)
        propertyConfigViewPager = this.findViewById(R.id.propertyConfigViewPager)

//        propertyConfigViewPager.layoutParams.height = ileUtility.getDeviceHeightForConfigurationCardView()

        propertyConfigTabLayout.addTab(propertyConfigTabLayout.newTab().setText(resources.getString(R.string.bhkTypeOne)))
        propertyConfigTabLayout.addTab(propertyConfigTabLayout.newTab().setText(resources.getString(R.string.bhkTypeTwo)))
        propertyConfigTabLayout.addTab(propertyConfigTabLayout.newTab().setText(resources.getString(R.string.bhkTypeThree)))
        propertyConfigTabLayout.addTab(propertyConfigTabLayout.newTab().setText(resources.getString(R.string.bhkTypeFour)))
        propertyConfigTabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.colorGreyLabels),
            ContextCompat.getColor(this, R.color.colorBlueLinks))

        propertyConfigViewPager.offscreenPageLimit = 4

        propertyConfigViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(propertyConfigTabLayout))
        propertyConfigTabLayout.addOnTabSelectedListener(onTabSelectedListener)
        propertyConfigViewPager.adapter = PropertyConfigTabPagerAdapter(supportFragmentManager)
    }

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            propertyConfigViewPager.currentItem = tab!!.position
        }
    }

    private fun setUpAmenitiesRecyclerView() {
        amenitiesRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        amenitiesRecyclerView.adapter = PropertyAmenitiesAdapter(this)
    }

    private fun setUpPriceTrendsRecyclerView() {
        priceTrendsRecyclerView.layoutManager = LinearLayoutManager(this)
        priceTrendsRecyclerView.adapter = PriceTrendsAdapter(this)
    }

    private fun updateSimilarProjectsRVItems() {
        val leftTitleTV = includeSimilarProjectID.findViewById(R.id.homeItemsTitleLeft) as TextView
        leftTitleTV.text = resources.getText(R.string.similarProjectLabel, "Malviya…")
        val rightTitleTV = includeSimilarProjectID.findViewById(R.id.homeItemsTitleRight) as TextView
        rightTitleTV.tag = "SimilarProjects"
//        rightTitleTV.setOnClickListener(onClickListener)

        similarProjectRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        similarProjectRecyclerView.adapter = SimilarProjectsAdapter(this, ileUtility, ArrayList())
    }

    private val clickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.exploreNeighbourhoodButton -> startActivity(Intent(this, ExploreNeighbourhoodActivity::class.java))
            R.id.shareButton -> shareProfileWithIntents()
            R.id.exportPDFButton -> {
                val exportPDFDialog = DialogYesNo.newInstance(AppConstants.DIALOG_TYPE_EXPORT_PDF)
                exportPDFDialog.show(supportFragmentManager.beginTransaction(),
                    resources.getString(R.string.fragTagNameLogoutDialog))
                exportPDFDialog.isCancelable = false
            }
        }
    }

    private fun shareProfileWithIntents() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, "I suggest this app for you : ILE")
        intent.type = "text/plain"
        startActivity(intent)
    }
}