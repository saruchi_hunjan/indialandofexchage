package com.landexchange.projectmodules.dedicatedscreens.compare

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dedicatedscreens.compare.models.ComparablePropertyMO
import com.landexchange.util.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_compare_properties.*
import kotlinx.android.synthetic.main.include_header_curve.*

/**
 * Created by Ashu Rajput on 6/20/2019.
 */
class ComparePropertiesActivity : BaseActivity() {

    private lateinit var addPropertiesButton: TextView

    override fun getLayoutId(): Int {
        return R.layout.activity_compare_properties
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        gradientStatusBar()
        addPropertiesButton = findViewById(R.id.clearAllButtonTV)
        moduleTitleName.text = resources.getString(R.string.compareProperties)
        addPropertiesButton.text = ""
        addPropertiesButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_white_plus, 0)
        setUpComparePropertiesRV()

        addPropertiesButton.setOnClickListener { }
    }

    private fun setUpComparePropertiesRV() {
        comparePropertiesRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val listOfProp = arrayListOf(
            ComparablePropertyMO("Omaxe Nile","2.5"),
            ComparablePropertyMO("Gaur SunCity","2.74"))

        val itemDecorator = DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.recycler_divider)!!)
        comparePropertiesRecyclerView.addItemDecoration(itemDecorator)
        comparePropertiesRecyclerView.adapter = ComparePropertyAdapter(this, listOfProp)
    }

}