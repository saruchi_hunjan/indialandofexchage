package com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R
import kotlinx.android.synthetic.main.row_price_trends.view.*

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PriceTrendsAdapter(val context: Context) :
    RecyclerView.Adapter<PriceTrendsAdapter.PriceTrendsViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PriceTrendsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_price_trends, parent, false)
        return PriceTrendsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: PriceTrendsViewHolder, position: Int) {
        holder.bindPriceTrendsItems()
    }

    inner class PriceTrendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPriceTrendsItems() {
            itemView.priceTrendPerSqFeet.text = "₹ 2,300 sq Feet"
            itemView.priceTrendPercentageGrowth.text = "5.93%"
            itemView.priceTrendPercentageGrowth.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_green_up_arrow, 0)
        }
    }
}