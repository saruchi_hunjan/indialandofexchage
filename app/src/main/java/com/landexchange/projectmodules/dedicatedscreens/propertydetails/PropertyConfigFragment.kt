package com.landexchange.projectmodules.dedicatedscreens.propertydetails

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters.PropertyDetailsAdapter
import kotlinx.android.synthetic.main.fragment_prop_config.*

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PropertyConfigFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_prop_config
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        try {
            propConfigRecyclerView.layoutManager = GridLayoutManager(activity, 2,
                LinearLayoutManager.HORIZONTAL, false)
            propConfigRecyclerView.adapter = PropertyDetailsAdapter(activity!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}