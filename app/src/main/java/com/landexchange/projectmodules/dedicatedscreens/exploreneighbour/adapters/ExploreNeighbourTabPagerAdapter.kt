package com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.fragments.ExploreNeighbourFragment

/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class ExploreNeighbourTabPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = ExploreNeighbourFragment()
            1 -> fragment = ExploreNeighbourFragment()
            2 -> fragment = ExploreNeighbourFragment()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 3
    }
}