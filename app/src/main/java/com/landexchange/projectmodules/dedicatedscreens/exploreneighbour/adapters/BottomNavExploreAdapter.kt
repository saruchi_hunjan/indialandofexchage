package com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R

/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class BottomNavExploreAdapter(val context: Context) :
    RecyclerView.Adapter<BottomNavExploreAdapter.BottomNavNeighbourViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BottomNavNeighbourViewHolder {
        val view = layoutInflater.inflate(R.layout.row_commute_essential_lifestyle, parent, false)
        return BottomNavNeighbourViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: BottomNavNeighbourViewHolder, position: Int) {
        holder.bindPriceTrendsItems()
    }

    inner class BottomNavNeighbourViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPriceTrendsItems() {

        }
    }
}