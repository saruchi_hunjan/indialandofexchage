package com.landexchange.projectmodules.dedicatedscreens.picturetour

import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.util.ILEUtility
import com.landexchange.util.adapters.ILEPagerAdapter
import kotlinx.android.synthetic.main.activity_tour_pictures.*
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class TourPictureActivity : BaseActivity() {

    @Inject
    lateinit var ileUtility: ILEUtility
    private lateinit var pictureSliderTabLayout: TabLayout
    private lateinit var pictureSliderViewPager: ViewPager

    private val tabTitleList = arrayOf("Outdoor", "Living Room", "Kitchen", "Bathroom",
        "Garden", "Bed Room", "Garage", "Balcony", "Contact")

    private var isAlreadyDefaultSet=true

    override fun getLayoutId(): Int {
        return R.layout.activity_tour_pictures
    }

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        setUpConfigTabsWithViewPager()
        footerPictureCounter.text = resources.getString(R.string.dynamicPicPreviewCounter, 1, tabTitleList.size)
    }

    private fun setUpConfigTabsWithViewPager() {
        try {

            pictureViewPagerParentLayout.layoutParams.height = ileUtility.getDeviceHeightForPicturePreview()
            pictureSliderTabLayout = this.findViewById(R.id.pictureSliderTabLayout)
            pictureSliderViewPager = this.findViewById(R.id.pictureSliderViewPager)

            pictureSliderViewPager.clipToPadding = false
            pictureSliderViewPager.setPadding(60, 0, 60, 0)
            pictureSliderViewPager.pageMargin = 30
            for (tabTitle in tabTitleList) {
                pictureSliderTabLayout.addTab(pictureSliderTabLayout.newTab().setText(tabTitle))
            }

            pictureSliderViewPager.offscreenPageLimit = tabTitleList.size

            pictureSliderViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(
                pictureSliderTabLayout))
            pictureSliderTabLayout.addOnTabSelectedListener(onTabSelectedListener)
            pictureSliderViewPager.adapter = ILEPagerAdapter(this, true)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
//            pictureSliderTabLayout.setScrollPosition(tab!!.position,-1f,true)
            var tabPosition = tab!!.position
            pictureSliderViewPager.currentItem = tabPosition
            footerPictureCounter.text = resources.getString(R.string.dynamicPicPreviewCounter,
                (tabPosition + 1), tabTitleList.size)

            if ((tabPosition + 1) == tabTitleList.size) {
                watchVideoButton.text = resources.getString(R.string.dynamicChatWith, "Akash")
                watchVideoButton.setBackgroundResource(R.drawable.blue_outer_transparent_body)
                watchVideoButton.setTextColor(ContextCompat.getColor(this@TourPictureActivity, R.color.colorBlueLinks))

                tour360Button.text = resources.getString(R.string.dynamicContact, "Akash")
                tour360Button.setBackgroundResource(R.drawable.blue_round_button)
                tour360Button.setTextColor(ContextCompat.getColor(this@TourPictureActivity, R.color.colorWhite))
                isAlreadyDefaultSet=false
            } else {
                if(!isAlreadyDefaultSet){
                    isAlreadyDefaultSet=true
                    watchVideoButton.text = resources.getString(R.string.watchVideo)
                    watchVideoButton.setBackgroundResource(R.drawable.white_stroke_transparent_solid_curve)
                    watchVideoButton.setTextColor(ContextCompat.getColor(this@TourPictureActivity, R.color.colorWhite))

                    tour360Button.text = resources.getString(R.string.tour360)
                    tour360Button.setBackgroundResource(R.drawable.white_stroke_transparent_solid_curve)
                    tour360Button.setTextColor(ContextCompat.getColor(this@TourPictureActivity, R.color.colorWhite))
                }
            }
        }
    }
}