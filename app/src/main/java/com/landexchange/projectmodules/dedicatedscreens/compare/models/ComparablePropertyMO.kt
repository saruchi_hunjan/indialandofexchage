package com.landexchange.projectmodules.dedicatedscreens.compare.models

/**
 * Created by Ashu Rajput on 6/20/2019.
 */
data class ComparablePropertyMO(val propertyName: String, val propertyPrice: String)