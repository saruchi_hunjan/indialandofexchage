package com.landexchange.projectmodules.dedicatedscreens.propertydetails.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.landexchange.R

/**
 * Created by Ashu Rajput on 6/18/2019.
 */
class PropertyDetailsAdapter(val context: Context) :
    RecyclerView.Adapter<PropertyDetailsAdapter.PropertyDetailsViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyDetailsViewHolder {
        val view = layoutInflater.inflate(R.layout.row_property_details, parent, false)
        return PropertyDetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: PropertyDetailsViewHolder, position: Int) {
        holder.bindPropertyDetailsItems()
    }

    inner class PropertyDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPropertyDetailsItems() {

        }
    }

}