package com.landexchange.projectmodules.dedicatedscreens.exploreneighbour

import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.adapters.ExploreNeighbourTabPagerAdapter
import kotlinx.android.synthetic.main.activity_explore_neighbourhood.*
import kotlinx.android.synthetic.main.bottom_sheet_explore_neighbour.*

/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class ExploreNeighbourhoodActivity : BaseActivity() {

    private lateinit var exploreNeighbourTabLayout: TabLayout
    private lateinit var exploreNeighbourViewPager: ViewPager

    override fun getLayoutId(): Int {
        return R.layout.activity_explore_neighbourhood
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun setUpUi() {
        setUpBottomNavTabAndViewPager()
        headerBlackBackButton.setOnClickListener { finish() }
    }

    private fun setUpBottomNavTabAndViewPager() {

        val exploreBottomNavSheet = BottomSheetBehavior.from(exploreNeighbourBottomNavSheet)
        exploreBottomNavSheet.isFitToContents = false

        exploreNeighbourTabLayout = this.findViewById(R.id.exploreNeighbourTabLayout)
        exploreNeighbourViewPager = this.findViewById(R.id.exploreNeighbourViewPager)

        exploreNeighbourTabLayout.addTab(exploreNeighbourTabLayout.newTab().setText(resources.getString(R.string.commute)))
        exploreNeighbourTabLayout.addTab(exploreNeighbourTabLayout.newTab().setText(resources.getString(R.string.essentials)))
        exploreNeighbourTabLayout.addTab(exploreNeighbourTabLayout.newTab().setText(resources.getString(R.string.lifestyle)))
        /*exploreNeighbourTabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.colorGreyLabels),
            ContextCompat.getColor(this, R.color.colorBlueLinks))*/

        exploreNeighbourViewPager.offscreenPageLimit = 3

        exploreNeighbourViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(
            exploreNeighbourTabLayout))
        exploreNeighbourTabLayout.addOnTabSelectedListener(onTabSelectedListener)
        exploreNeighbourViewPager.adapter = ExploreNeighbourTabPagerAdapter(supportFragmentManager)
    }

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            exploreNeighbourViewPager.currentItem = tab!!.position
        }
    }

}