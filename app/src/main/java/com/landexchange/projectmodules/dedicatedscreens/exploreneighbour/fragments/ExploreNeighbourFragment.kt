package com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.fragments

import androidx.recyclerview.widget.LinearLayoutManager
import com.landexchange.R
import com.landexchange.base.BaseFragment
import com.landexchange.projectmodules.dedicatedscreens.exploreneighbour.adapters.BottomNavExploreAdapter
import kotlinx.android.synthetic.main.fragment_explore_neighbour.*

/**
 * Created by Ashu Rajput on 6/19/2019.
 */
class ExploreNeighbourFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_explore_neighbour
    }

    override fun initializeDagger() {
    }

    override fun initializeViewModel() {
    }

    override fun fragmentOnCreate() {
    }

    override fun setUpUi() {
        try {
            exploreBottomNavRecyclerView.layoutManager = LinearLayoutManager(activity)
            exploreBottomNavRecyclerView.adapter = BottomNavExploreAdapter(activity!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}