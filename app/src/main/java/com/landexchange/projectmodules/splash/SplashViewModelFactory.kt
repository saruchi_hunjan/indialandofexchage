package com.sisindia.csat.projectmodules.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ashu Rajput on 12/21/2018.
 */
class SplashViewModelFactory
@Inject
constructor() : @JvmSuppressWildcards ViewModelProvider.Factory {

    @Inject
    lateinit var localRepository: LocalRepository

    @Inject
    lateinit var remoteRepository: RemoteRepository

    @Inject
    @field:Named("vm")
    lateinit var compositeDisposable: CompositeDisposable

    @Override
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            return SplashViewModel(localRepository, remoteRepository, compositeDisposable) as T
        }
        throw IllegalArgumentException("Wrong ViewModel class")
    }
}