package com.landexchange.projectmodules.splash

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivity
import com.landexchange.projectmodules.dashboard.DashboardActivity
import com.landexchange.projectmodules.iamlookingto.IAmLookingActivity
import com.landexchange.projectmodules.iamlookingto.models.CityLocalitiesMO
import com.landexchange.projectmodules.userentry.UserEntryActivity
import com.landexchange.util.AppPreferences
import com.sisindia.csat.projectmodules.splash.SplashInteractor.SplashView
import com.sisindia.csat.projectmodules.splash.SplashViewModel
import com.sisindia.csat.projectmodules.splash.SplashViewModelFactory
import javax.inject.Inject

/**
 * Created by Ashu Rajput on 4/23/2019.
 */

class SplashActivity : BaseActivity(), SplashView {

    @Inject
    lateinit var splashVMFactory: SplashViewModelFactory

    private lateinit var splashViewModel: SplashViewModel

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        splashViewModel = ViewModelProvider(this, splashVMFactory).get(SplashViewModel::class.java)
        splashViewModel.setView(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun setUpUi() {
        setFullModeScreenForStatusBar()
        splashViewModel.getAppVersionDetailFromAPI()
        //Removing key: to set default value in receiver chat ID
        appPreferences.removeKeyValueFromPref(AppPreferences.KEY_RECEIVER_CHAT_ID)
    }

    override fun onSuccess(response: Any) {
        /*if (appPreferences.signUpStatus || appPreferences.loginStatus) {
            if (appPreferences.cityLocalitiesJson != null)
                startActivity(Intent(this, DashboardActivity::class.java))
            else
                startActivity(Intent(this, IAmLookingActivity::class.java))
        } else
            startActivity(Intent(this, UserEntryActivity::class.java))*/
        tempMethodKindlyRemove()
    }

    private fun tempMethodKindlyRemove(){
        val cityLocalitiesMO=CityLocalitiesMO("Delhi",1,"New Delhi",1)
        appPreferences.saveCityLocalities(cityLocalitiesMO)
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()

    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }
}