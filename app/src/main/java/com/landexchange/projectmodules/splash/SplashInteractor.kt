package com.sisindia.csat.projectmodules.splash

import com.landexchange.base.listener.BaseView

/**
 * Created by Ashu Rajput on 12/21/2018.
 */
class SplashInteractor {
    interface SplashView : BaseView {
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}