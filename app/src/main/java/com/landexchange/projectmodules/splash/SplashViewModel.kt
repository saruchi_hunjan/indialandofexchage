package com.sisindia.csat.projectmodules.splash

import android.os.Handler
import com.landexchange.base.BaseViewModel
import com.landexchange.repository.RemoteRepository
import com.sisindia.csat.projectmodules.splash.SplashInteractor.SplashView
import com.sisindia.csat.repository.LocalRepository
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Ashu Rajput on 12/21/2018.
 */
class SplashViewModel
constructor(
    val localRepository: LocalRepository,
    val remoteRepository: RemoteRepository,
    val compositeDisposable: CompositeDisposable
) : BaseViewModel<SplashView>() {

    fun getAppVersionDetailFromAPI() {
        getView()?.setLoaderVisibility(true)
        Handler().postDelayed((Runnable {
            getView()?.setLoaderVisibility(false)
            getView()?.onSuccess("")
        }), 2000)
    }
}