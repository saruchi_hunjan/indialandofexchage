package com.landexchange.projectmodules.dashboard

import com.landexchange.base.listener.BaseView


/**
 * Created by Ashu Rajput on 4/30/2019.
 */
class DashboardContractor {
    interface DashboardView : BaseView {
        fun navigateToMainScreen()
        fun onSuccess(response: Any)
        fun onFailure(appErrorMessage: String)
        fun setLoaderVisibility(isLoaderVisible: Boolean)
    }
}