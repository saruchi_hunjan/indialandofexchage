package com.landexchange.projectmodules.dashboard

import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.base.BaseActivityWithRTP
import com.landexchange.projectmodules.dashboard.DashboardContractor.DashboardView
import com.landexchange.projectmodules.dashboardfragments.account.AccountFragment
import com.landexchange.projectmodules.dashboardfragments.chat.ChatFragment
import com.landexchange.projectmodules.dashboardfragments.favourites.FavouritesFragment
import com.landexchange.projectmodules.dashboardfragments.home.HomeFragment
import com.landexchange.projectmodules.dashboardfragments.supersearch.NavSearchFragment
import com.sisindia.csat.util.addFragment
import kotlinx.android.synthetic.main.activity_dashboard.*

/**
 * Created by Ashu Rajput on 4/30/2019.
 */
class DashboardActivity : BaseActivityWithRTP(), DashboardView, OnNavigationItemSelectedListener {

    /*@Inject
    lateinit var dashboardViewModelFactory: DashboardViewModelFactory

    private lateinit var dashboardViewModel: DashboardViewModel*/

    override fun initializeDagger() {
        IleApplication.getRoomComponent().inject(this)
    }

    override fun initializeViewModel() {
        /*dashboardViewModel = ViewModelProvider(this,
            dashboardViewModelFactory).get(DashboardViewModel::class.java)
        dashboardViewModel.setView(this)*/
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    override fun setUpUi() {
        gradientStatusBar()
        /*replaceContentFragment(R.id.fragmentsContainer,
            HomeFragment.newInstance(), mResources.getString(R.string.fragTagNameHome))*/

        /*footerHomeButton.setOnClickListener(onClickListener)
        footerFavouriteButton.setOnClickListener(onClickListener)
        footerChatButton.setOnClickListener(onClickListener)
        footerProfileButton.setOnClickListener(onClickListener)*/

        homeSuperSearch.setOnClickListener {
            replaceDashboardFragments(NavSearchFragment.newInstance(),
                mResources.getString(R.string.fragTagNameSuperSearch))
        }
        navigation.setOnNavigationItemSelectedListener(this)
        isGPSEnabled.observe(this, Observer {
            if (it) {
                addFragment(HomeFragment.newInstance(), R.id.fragmentsContainer,
                    mResources.getString(R.string.fragTagNameHome))
            }
        })
    }

    override fun navigateToMainScreen() {
    }

    override fun onSuccess(response: Any) {
    }

    override fun onFailure(appErrorMessage: String) {
    }

    override fun setLoaderVisibility(isLoaderVisible: Boolean) {
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.footerMenuHomeButton -> {
                replaceDashboardFragments(HomeFragment.newInstance(),
                    mResources.getString(R.string.fragTagNameHome))
            }
            R.id.footerMenuFavouriteButton -> {
                replaceDashboardFragments(FavouritesFragment.newInstance(),
                    mResources.getString(R.string.fragTagNameFavourite))
            }
            R.id.footerMenuChatButton -> {

                /* val mAuth = FirebaseAuth.getInstance()
                     mAuth.signInWithEmailAndPassword("ashurajput11@gmail.com", "ashu@123")
                         .addOnSuccessListener { authResult ->
                             if (authResult != null) {
                                 Log.e("ChatFragment", "Successfully signedIn")
                                startActivity(Intent(this@DashboardActivity,
                                    ChatMainActivity::class.java))
                            }
                        }.addOnFailureListener {
                            Log.e("ChatFragment", "Failure Exeception signedIn")
                            it.printStackTrace()
                        }*/

                replaceDashboardFragments(ChatFragment.newInstance(),
                    mResources.getString(R.string.fragTagNameChat))
                //                startActivity(Intent(this@DashboardActivity, ChatMainActivity::class.java))
            }
            R.id.footerMenuProfileButton -> {
                /*replaceDashboardFragments(ProfileFragment.newInstance(),
                        mResources.getString(R.string.fragTagNameProfile))*/
                replaceDashboardFragments(AccountFragment.newInstance(),
                    mResources.getString(R.string.fragTagNameAccount))
            }
        }
        return true
    }

    /*private val onClickListener = View.OnClickListener { v ->
        when {
            v.id == R.id.footerHomeButton -> {
                showSnakeBarMessages(v, "HomeFragment Clicked")
            }
            v.id == R.id.footerFavouriteButton -> {
                showSnakeBarMessages(v, "Fav Fragment Clicked")
            }
            v.id == R.id.footerChatButton -> {
                showSnakeBarMessages(v, "ChatFragment Clicked")
            }
            v.id == R.id.footerProfileButton -> {
                showSnakeBarMessages(v, "ProfileFragment Clicked")
            }
        }
    }*/

    /*override fun onBackPressed() {
        super.onBackPressed()
        popUpFragments()
    }*/

    private fun popUpFragments() {
        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    private fun replaceDashboardFragments(fragment: Fragment, fragmentTag: String) {
        val topFragment = supportFragmentManager.findFragmentByTag(fragmentTag)
        if (topFragment != null && topFragment.isVisible)
            return
        else {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragmentsContainer, fragment, fragmentTag).commit()
        }
    }
}