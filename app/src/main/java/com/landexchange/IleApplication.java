package com.landexchange;

import android.app.Application;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.landexchange.deps.AppComponent;
import com.landexchange.deps.AppModule;
import com.landexchange.deps.DaggerAppComponent;
import com.landexchange.deps.GsonModule;
import com.landexchange.deps.RoomComponent;
import com.landexchange.deps.RoomModule;
import com.landexchange.networking.NetworkModule;

import java.io.File;

public class IleApplication extends Application implements LifecycleObserver {
    private static AppComponent appComponent;
    private static IleApplication instance;
    private static RoomComponent roomComponent;
    private boolean isAppInBackground;

    public static IleApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Fabric.with(this, new Crashlytics());
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultUnCaughtExceptionHandlerDecorator(Thread.getDefaultUncaughtExceptionHandler()));
        instance = this;
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());
        initAppComponent();
        initRoomComponent();
        appComponent.inject(this);
        // syncAdapterInstance = new SyncAdapterInitialization(this);

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

//        onAppBackgrounded();
//        onAppForegrounded();
//        isAppInBackground();
    }

    public void initAppComponent() {
        File cacheFile = new File(getCacheDir(), "responses");
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule(cacheFile))
                .gsonModule(new GsonModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public void initRoomComponent() {
        roomComponent = appComponent.getRoomComponent(new RoomModule(this));
    }

    public static RoomComponent getRoomComponent() {
        return roomComponent;
    }

    /*public static LocalRepositoryImpl localRepoInstance() {
        return roomComponent.getLocalRepo();
    }

    public static RemoteRepositoryImpl remoteRepositoryInstance() {
        return roomComponent.getRemoteRepo();
    }

    public static CompositeDisposable getCompositeDisposableInstance() {
        return roomComponent.getVMCompositeDisposable();
    }*/

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
//        Log.e("ILEApplication", "onAppBackgrounded()");
        isAppInBackground = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
//        Log.e("ILEApplication", "onAppForegrounded()");
        isAppInBackground = false;
    }

    public boolean isAppInBackground() {
        return isAppInBackground;
    }

    /*public void isAppInBackground() {

        if (ProcessLifecycleOwner.get().getLifecycle().getCurrentState() == Lifecycle.State.CREATED)
            Log.e("ILEApplication", "isAppInBackground YESSS");
        else
            Log.e("ILEApplication", "isAppInBackground() NOOO");
//        return ProcessLifecycleOwner.get().getLifecycle().getCurrentState() == Lifecycle.State.CREATED;
    }*/

}