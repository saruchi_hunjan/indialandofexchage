package com.landexchange.database.models;

import androidx.room.ColumnInfo;

/**
 * Created by Ashu Rajput on 1/31/2019.
 */
public class AddNoteFileAttachment {

    /*@Embedded(prefix = "fileAttachments_")
    private FileAttachments fileAttachments;

    @Embedded(prefix = "addNotesDetails_")
    private AddNotesDetails addNotesDetails;*/

    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "csatId")
    public int csatid;

    @ColumnInfo(name = "documentPath")
    public String documentPath;

    @ColumnInfo(name = "fileType")
    public int fileType;

    @ColumnInfo(name = "isSynced")
    public int isSynced;

    @ColumnInfo(name = "isNew")
    public int isNew;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCsatid() {
        return csatid;
    }

    public void setCsatid(int csatid) {
        this.csatid = csatid;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public int getIsSynced() {
        return isSynced;
    }

    public void setIsSynced(int isSynced) {
        this.isSynced = isSynced;
    }

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }
}
