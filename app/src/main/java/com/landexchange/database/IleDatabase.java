package com.landexchange.database;

import com.landexchange.database.tables.LoginSessionEntityDummy;
import com.landexchange.util.Converters;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import com.sisindia.csat.database.IleDao;

@Database(entities = {
        LoginSessionEntityDummy.class
}, version = IleDatabase.VERSION)
@TypeConverters({Converters.class})  //convert ArrayList object to one of SQLite data type
public abstract class IleDatabase extends RoomDatabase {
    static final int VERSION = 1;

    public abstract IleDao getIleDao();
}