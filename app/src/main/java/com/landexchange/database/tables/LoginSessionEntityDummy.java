package com.landexchange.database.tables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity()
public class LoginSessionEntityDummy {

    // inserting the two record for the two logined users rather than one .
    @PrimaryKey
    @NonNull
    @SerializedName("SessionId")
    @Expose
    private Integer sessionId;

    @NonNull
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(@NonNull Integer sessionId) {
        this.sessionId = sessionId;
    }
}
