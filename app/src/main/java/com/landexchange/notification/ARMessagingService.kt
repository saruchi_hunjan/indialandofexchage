package com.landexchange.notification

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.landexchange.IleApplication
import com.landexchange.R
import com.landexchange.projectmodules.chat.OneToOneChatActivity
import com.landexchange.util.AppPreferences
import org.json.JSONObject

/**
 * Created by Ashu Rajput on 9/2/2019.
 */

@SuppressLint("LogConditional")
class ARMessagingService : FirebaseMessagingService() {

    private val channelID = "ILE_Channel_Id"
    private var appPreferences: AppPreferences? = null

    override fun onNewToken(token: String) {
        super.onNewToken(token)
//        Log.d("Notification", "ARMessagingService onNewToken: $token")
        if (appPreferences == null)
            appPreferences = AppPreferences(this)
        appPreferences!!.fcmId = token
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        try {
            if (appPreferences == null)
                appPreferences = AppPreferences(this)

//            val messageData = message.data
            val jsonBody = JSONObject(message.data["json_body"])

            if (IleApplication.getInstance().isAppInBackground) {
                Log.e("ARMessagingService", "App is in Background...")
                if (jsonBody["userId"] != appPreferences!!.fireBaseUniqueUserId) {
                    /*buildNotificationOnTray(messageData["title"]!!, messageData["body"]!!,
                        messageData["notificationId"]!!)*/
                    buildNotificationOnTray(jsonBody)
                }
            } else {
                Log.e("ARMessagingService", "App is in Foreground...")
                if (jsonBody["userId"] == appPreferences!!.fireBaseUniqueUserId ||
                    jsonBody["userId"] == appPreferences!!.receiverChatId
                ) {
                    Log.e("ARMessagingService", "No notification need to send")
                } else {
                    buildNotificationOnTray(jsonBody)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //    private fun buildNotificationOnTray(title: String, msgBody: String, notificationId: String) {
    private fun buildNotificationOnTray(msgJsonBody: JSONObject) {
        createNotificationChannel()

        val intent = Intent(this, OneToOneChatActivity::class.java).apply {
            putExtra("ChatWithUserId", msgJsonBody.getString("userId"))
            putExtra("ChatWithUserName", msgJsonBody.getString("userName"))
            putExtra("ChatWithFCMTokenID", msgJsonBody.getString("fcmTokenId"))
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT)

        //Builder for small text notification
//        val inboxStyle = NotificationCompat.InboxStyle()

        val notificationId = msgJsonBody.getString("notificationId")
        val builder = NotificationCompat.Builder(this, channelID)
            .setSmallIcon(R.drawable.ic_footer_chat_unselected)
            .setContentTitle(msgJsonBody.getString("userName"))
            .setContentText(msgJsonBody.getString("userMsg"))
            .setContentIntent(pendingIntent)
//            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
            .setStyle(NotificationCompat.InboxStyle().addLine(msgJsonBody.getString("userMsg")))
            .setGroupSummary(true)
            .setGroup(notificationId)

        with(NotificationManagerCompat.from(this)) {
            notify(notificationId.toInt() + 1, builder.build())
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}