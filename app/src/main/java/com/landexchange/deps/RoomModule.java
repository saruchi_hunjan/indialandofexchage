package com.landexchange.deps;

import android.app.Application;
import androidx.room.Room;
import com.landexchange.database.IleDatabase;

import com.landexchange.networking.NetworkService;
import com.landexchange.repository.RemoteRepository;
import com.landexchange.repository.RemoteRepositoryImpl;
import com.landexchange.util.AppConstants;
import com.sisindia.csat.database.IleDao;
import com.sisindia.csat.repository.LocalRepository;
import com.sisindia.csat.repository.LocalRepositoryImpl;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

import javax.inject.Named;
import java.util.concurrent.Executor;

@Module
public class RoomModule {

    private IleDatabase ileDatabase;

    public RoomModule(Application mApplication) {
        ileDatabase = Room.databaseBuilder(mApplication, IleDatabase.class, AppConstants.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    @RunTimeScope
    @Provides
    IleDatabase providesRoomDatabase() {
        return ileDatabase;
    }

    @RunTimeScope
    @Provides
    IleDao providesIleDao(IleDatabase ileDatabase) {
        return ileDatabase.getIleDao();
    }

    @RunTimeScope
    @Provides
    public RemoteRepository getRemoteRepo(NetworkService networkService) {
        return new RemoteRepositoryImpl(networkService);
    }

    @RunTimeScope
    @Provides
    public LocalRepository getLocalRepo(IleDao ileDao, Executor exec) {
        return new LocalRepositoryImpl(ileDao, exec);
    }

    @RunTimeScope
    @Provides
    @Named("activity")
    public CompositeDisposable getCompositeDisposable() {
        return new CompositeDisposable();
    }

    @RunTimeScope
    @Provides
    @Named("vm")
    public CompositeDisposable getVMCompositeDisposable() {
        return new CompositeDisposable();
    }

}