package com.landexchange.deps;

import com.landexchange.database.IleDatabase;
import com.landexchange.projectmodules.accountoptions.AccountOptionsActivity;
import com.landexchange.projectmodules.accountoptions.notifications.NotificationsFragment;
import com.landexchange.projectmodules.agentdetails.AgentDetailsActivity;
import com.landexchange.projectmodules.agentdetails.fragments.ActiveInactivePropertyFragment;
import com.landexchange.projectmodules.bookagent.BookAgentActivity;
import com.landexchange.projectmodules.bookagent.SearchAgentActivity;
import com.landexchange.projectmodules.createaccount.CreateAccountActivity;
import com.landexchange.projectmodules.dashboard.DashboardActivity;
import com.landexchange.projectmodules.dashboardfragments.account.AccountFragment;
import com.landexchange.projectmodules.dashboardfragments.favourites.FavouritesFragment;
import com.landexchange.projectmodules.dashboardfragments.home.HomeFragment;
import com.landexchange.projectmodules.dashboardfragments.profile.ProfileFragment;
import com.landexchange.projectmodules.dashboardfragments.supersearch.NavSearchFragment;
import com.landexchange.projectmodules.dedicatedscreens.compare.ComparePropertyAdapter;
import com.landexchange.projectmodules.dedicatedscreens.picturetour.TourPictureActivity;
import com.landexchange.projectmodules.dedicatedscreens.propertydetails.PropertyDetailsActivity;
import com.landexchange.projectmodules.filters.fragments.AgentsFiltersFragment;
import com.landexchange.projectmodules.filters.fragments.NavSearchFilterFragment;
import com.landexchange.projectmodules.filters.fragments.PropertyFiltersFragment;
import com.landexchange.projectmodules.iamlookingto.IAmLookingActivity;
import com.landexchange.projectmodules.login.LoginActivity;
import com.landexchange.projectmodules.popularlocalities.AreaLocalitiesActivity;
import com.landexchange.projectmodules.seeall.SeeAllAgentsActivity;
import com.landexchange.projectmodules.seeall.adapters.SeeAllPropAdapter;
import com.landexchange.projectmodules.splash.SplashActivity;
import com.landexchange.projectmodules.uploadlistings.UploadListingActivity;
import com.landexchange.projectmodules.userentry.UserEntryActivity;
import com.landexchange.repository.RemoteRepositoryImpl;
import com.landexchange.util.iledialog.DialogChooseCity;
import com.landexchange.util.iledialog.DialogSelectPriority;
import com.landexchange.util.iledialog.DialogYesNo;
import com.sisindia.csat.database.IleDao;
import com.sisindia.csat.repository.LocalRepositoryImpl;
import dagger.Subcomponent;
import io.reactivex.disposables.CompositeDisposable;

import javax.inject.Named;

@RunTimeScope
@Subcomponent(modules = RoomModule.class)
public interface RoomComponent {

    void inject(SplashActivity splashActivity);

    void inject(CreateAccountActivity createAccountActivity);

    void inject(UserEntryActivity userEntryActivity);

    void inject(DashboardActivity dashboardActivity);

    void inject(LoginActivity loginActivity);

    void inject(IAmLookingActivity iAmLookingActivity);

    void inject(UploadListingActivity uploadListingActivity);

    void inject(AreaLocalitiesActivity areaLocalitiesActivity);

    void inject(HomeFragment homeFragment);

    void inject(BookAgentActivity bookAgentActivity);

    void inject(ProfileFragment profileFragment);

    void inject(FavouritesFragment favouritesFragment);

    void inject(NotificationsFragment notificationsFragment);

    void inject(DialogYesNo dialogYesNo);

//    void inject(AccountFragment accountFragment);

    void inject(AccountOptionsActivity accountOptionsActivity);

//    void inject(DialogRateILE dialogRateILE);

    void inject(DialogChooseCity dialogChooseCity);

    void inject(DialogSelectPriority dialogSelectPriority);

    void inject(SearchAgentActivity searchAgentActivity);

    void inject(AgentsFiltersFragment agentsFiltersFragment);

    void inject(ActiveInactivePropertyFragment activeInactivePropertyFragment);

    void inject(AgentDetailsActivity agentDetailsActivity);

//    void inject(DialogUserRating dialogUserRating);

    void inject(NavSearchFragment navSearchFragment);

    void inject(NavSearchFilterFragment navSearchFilterFragment);

    void inject(PropertyDetailsActivity propertyDetailsActivity);

    void inject(TourPictureActivity tourPictureActivity);

    void inject(ComparePropertyAdapter comparePropertyAdapter);

    void inject(SeeAllPropAdapter seeAllPropAdapter);

    void inject(PropertyFiltersFragment propertyFiltersFragment);

    void inject(SeeAllAgentsActivity seeAllAgentsActivity);

    IleDao iledao();

    IleDatabase ileDatabase();

    RemoteRepositoryImpl getRemoteRepo();

    LocalRepositoryImpl getLocalRepo();

    @Named("activity")
    CompositeDisposable getCompositeDisposable();

    @Named("vm")
    CompositeDisposable getVMCompositeDisposable();
}
