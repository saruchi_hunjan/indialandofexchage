package com.landexchange.deps;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Scope
@Retention(value = RetentionPolicy.RUNTIME)
public @interface RunTimeScope {
}
