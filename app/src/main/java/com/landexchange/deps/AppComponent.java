
package com.landexchange.deps;

import android.app.Application;

import com.google.gson.Gson;
import com.landexchange.IleApplication;
import com.landexchange.networking.NetworkModule;

import dagger.Component;
import retrofit2.Retrofit;

import javax.inject.Singleton;

@Singleton
@Component(dependencies = {}, modules = {AppModule.class, NetworkModule.class, GsonModule.class})
public interface AppComponent {

    RoomComponent getRoomComponent(RoomModule roomModule);

    void inject(IleApplication applicationController);

    void inject(Gson gson);

    Application application();

    BaseUrlHolder provideBaseUrlHolder();

    Retrofit getRetrofit();

    Gson getGson();

}