package com.landexchange.deps;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ashu Rajput on 1/21/2020.
 */
@Module
public class GsonModule {
    private Gson gson;

    public GsonModule() {
        this.gson = new Gson();
    }

    @Provides
    @Singleton
    Gson providesGSON() {
        return gson;
    }
}
