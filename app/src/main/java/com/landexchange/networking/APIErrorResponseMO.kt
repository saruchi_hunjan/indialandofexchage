package com.landexchange.networking

import com.google.gson.annotations.SerializedName

/**
 * Created by Ashu Rajput on 1/21/2020.
 */
data class APIErrorResponseMO(
    @field:SerializedName("status")
    var statusCode: Int? = null,

    @field:SerializedName("StatusMessage")
    var statusMessage: String? = null,

    @field:SerializedName("data")
    var data: ErrorMessageMO? = null
)