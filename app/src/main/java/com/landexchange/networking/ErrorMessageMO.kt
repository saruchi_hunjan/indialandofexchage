package com.landexchange.networking

import com.google.gson.annotations.SerializedName


/**
 * Created by Ashu Rajput on 1/21/2020.
 */
data class ErrorMessageMO(
    @field:SerializedName("message")
    var errorMessage: String? = null
)