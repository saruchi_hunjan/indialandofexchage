package com.landexchange.networking;


import android.annotation.SuppressLint;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.landexchange.BuildConfig;
import com.landexchange.IleApplication;
import com.landexchange.deps.BaseUrlHolder;
import com.landexchange.util.AppPreferences;
import com.landexchange.util.Logger;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private File cacheFile;
    private static final int TIMEOUT_IN_MS = 60; // 1 min
    private AppPreferences preferences = null;

    public NetworkModule(File cacheFile) {
        this.cacheFile = cacheFile;
    }

    @Singleton
    @Provides
    Executor getExecutor() {
        return Executors.newFixedThreadPool(4);
    }

    @Provides
    @Singleton
    Retrofit provideCall(BaseUrlHolder baseUrlHolder) {

        if (preferences == null)
            preferences = new AppPreferences(IleApplication.getInstance());

        Cache cache = null;
        try {
            cache = new Cache(cacheFile, 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    /*@SuppressLint("DefaultLocale")
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .removeHeader("Pragma")
                            .header("Cache-Control", String.format("max-age=%d", BuildConfig.CACHETIME))
                            .build();*/

                    @SuppressLint("DefaultLocale")
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .removeHeader("Pragma")
                            .header("Cache-Control", String.format("max-age=%d", BuildConfig.CACHETIME));

                    if (!preferences.getAPITokenId().isEmpty())
                        requestBuilder.header("Authorization", preferences.getAPITokenId());

                    okhttp3.Response response = chain.proceed(requestBuilder.build());
                    response.cacheResponse();

                    return response;
                })
                .addInterceptor(logging)
                .cache(cache)
                .readTimeout(TIMEOUT_IN_MS, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_IN_MS, TimeUnit.SECONDS)
                .build();

//        Logger.d("Network Module", baseUrlHolder.getBaseUrl());

        return new Retrofit.Builder()
                .baseUrl(baseUrlHolder.getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    NetworkService providesNetworkService(Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }

    @Provides
    BaseUrlHolder provideBaseUrlHolder() {
        return new BaseUrlHolder(BuildConfig.BASEURL);
    }
}
