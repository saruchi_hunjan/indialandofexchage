package com.landexchange.networking;

import com.landexchange.projectmodules.accountoptions.notifications.models.NotificationAPIResponse;
import com.landexchange.projectmodules.bookagent.models.AgentsDetailAPIResponse;
import com.landexchange.projectmodules.createaccount.models.CreateAcRequestBodyMO;
import com.landexchange.projectmodules.dashboardfragments.home.models.CollectionsAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedLocalitiesAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.FeaturedProjectAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.PromotersAPIResponse;
import com.landexchange.projectmodules.dashboardfragments.home.models.TopAgentsAPIResponse;
import com.landexchange.projectmodules.iamlookingto.models.CitiesAPIResponse;
import com.landexchange.projectmodules.login.models.LoginAPIResponse;
import com.landexchange.projectmodules.login.models.LoginRequestBodyMO;
import com.landexchange.projectmodules.login.models.ResendApiMO;
import com.landexchange.projectmodules.popularlocalities.models.CityAreasAPIResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface NetworkService {

    /**
     * @deprecated explanation of why function was deprecated, if possible include what
     * should be used.
     */
    @Deprecated
    @GET("api/mobileLogin")
    Observable<LoginAPIResponse> loginAPICall(@Query("mobile") String mobileNo);

    @POST("users/login")
//    Observable<Response<LoginAPIResponse>> loginAPICallV2(@Body LoginRequestBodyMO loginRequestBodyMO);
    Observable<LoginAPIResponse> loginAPICallV2(@Body LoginRequestBodyMO loginRequestBodyMO);

    @POST("users/send_otp")
    Observable<ResendApiMO> resendOtpAPICallV2();

    @POST("users/signup")
    Observable<Response<LoginAPIResponse>> createAccountAPICall(@Body CreateAcRequestBodyMO userDetailsBody);

    @GET("api/getCityAreaData")
    Observable<CityAreasAPIResponse> cityAreaAPICall(@Query("citycode") String cityCode);

    @GET("api/getCityData")
    Observable<CitiesAPIResponse> citiesAPICall();

    @GET("api/getFeaturePropData")
    Observable<FeaturedProjectAPIResponse> featuredProjectAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getIlePropData")
    Observable<FeaturedProjectAPIResponse> ileRecommendationAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getTrendingPropData")
    Observable<FeaturedProjectAPIResponse> trendingProjectAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getCollectionPropData")
    Observable<CollectionsAPIResponse> collectionsAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getAgentData")
    Observable<TopAgentsAPIResponse> topAgentsAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getfLocation")
    Observable<FeaturedLocalitiesAPIResponse> featuredLocalitiesAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getAgentFilterLocation")
    Observable<AgentsDetailAPIResponse> agentsDetailsAPICall(@QueryMap Map<String, Integer> queryMap);

    @GET("api/getPramotionData")
    Observable<PromotersAPIResponse> promotersAPICall();

    @GET("api/getNotificationData")
    Observable<NotificationAPIResponse> notificationsAPICall(@Query("user_id") String user_id);
}